<?php

$loader = require __DIR__ . '/vendor/autoload.php';

$configurator = new \Flubber\Component\Application\Configurator($loader);

$configurator->setTempDirectory(__DIR__ . '/var/temp');
$configurator->setDebugMode(true);
$configurator->enableTracy(__DIR__ . '/var/log');

$configurator->setTimeZone('Europe/Prague');

$configurator->addExtensions([
    "flubber.extension" => \Flubber\Extension\DI\Extension::class,
    "flubber.diskBrowser" => \Flubber\Component\DiskBrowser\DI\Extension::class
]);

$configurator->addConfig(__DIR__ . '/config/config.json');
$configurator->addConfig(__DIR__ . '/config/config.local.json');

$container = $configurator->createContainer();

return $container;
