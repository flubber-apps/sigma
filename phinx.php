<?php
$config = json_decode(file_get_contents(__DIR__ . "/config/config.local.json"));
return [
    "paths" => [
        "migrations" => "%%PHINX_CONFIG_DIR%%/config/database"
    ],
    "environments" => [
        "default_migration_table" => "phinx_log",
        "default_database" => "develop",
        "develop" => [
            "adapter" => "mysql",
            "host" => $config->doctrine->host,
            "name" => $config->doctrine->dbname,
            "user" => $config->doctrine->user,
            "pass" => $config->doctrine->password,
            "port" => property_exists($config->doctrine, "port") ? $config->doctrine->port : 3306,
            "charset" => "utf8",
            "collation" => "utf8_unicode_ci"
        ]
    ]
];