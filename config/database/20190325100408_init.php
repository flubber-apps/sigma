<?php

class Init extends \Phinx\Migration\AbstractMigration
{
    protected function _init($facadeName)
    {
        /** @var \Nette\DI\Container $bootstrap */
        $container = require_once __DIR__ . "/../../bootstrap.php";
        $em = $container->getByType(\Doctrine\ORM\EntityManagerInterface::class);
        $storage = $container->getByType(\Nette\Caching\IStorage::class);

        return new $facadeName($em, $storage);
    }

    public function up()
    {
        $this->execute("-- Adminer 4.5.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `component_disk_browser_directory` (`id`, `name`, `is_root`) VALUES
    (1,     '/',    1),
(2,     '_images',      0),
(3,     '/',    1),
(4,     '_images',      0),
(5,     '/',    1),
(6,     '_images',      0),
(7,     '/',    1),
(8,     '_images',      0),
(9,     'ABC',  1),
(10,    'ABC 4',        1),
(11,    'DV 25',        1),
(12,    '/',    1),
(13,    '_images',      0),
(14,    '/',    1),
(15,    '_images',      0),
(16,    '/',    1),
(17,    '_images',      0),
(18,    '/',    1),
(19,    '_images',      0),
(20,    'Výměna ložiska',       1),
(21,    '/',    1),
(22,    '_images',      0),
(23,    'Výměna ložiska',       1),
(24,    '/',    1),
(25,    '_images',      0),
(26,    'Oprava ložiska',       1),
(27,    'ssssss',       1),
(28,    '/',    1),
(29,    '_images',      0),
(30,    '456',  1),
(31,    '/',    1),
(32,    '_images',      0),
(33,    'asd',  1),
(34,    '/',    1),
(35,    '_images',      0),
(36,    'aasdasd',      1),
(37,    '/',    1),
(38,    '_images',      0),
(39,    '/',    1),
(40,    '_images',      0),
(41,    '/',    1),
(42,    '_images',      0),
(43,    'a',    1),
(44,    '/',    1),
(45,    '_images',      0),
(46,    'test', 1),
(47,    '/',    1),
(48,    '_images',      0),
(49,    'xxx',  1),
(50,    '/',    1),
(51,    '_images',      0),
(52,    'ssss', 1),
(53,    '/',    1),
(54,    '_images',      0),
(55,    'ssss', 1),
(56,    '/',    1),
(57,    '_images',      0),
(58,    'ssss', 1),
(59,    '/',    1),
(60,    '_images',      0),
(61,    'asdad',        1),
(62,    '/',    1),
(63,    '_images',      0),
(64,    'asdad',        1),
(65,    'TeST', 1),
(66,    '/',    1),
(67,    '_images',      0),
(68,    'dddd', 1),
(69,    '/',    1),
(70,    '_images',      0),
(71,    'Kontrola teploty',     1);

INSERT INTO `component_disk_browser_directory_tree` (`id`, `ancestor_id`, `descendant_id`, `depth`) VALUES
    (1,     1,      1,      0),
(2,     2,      2,      0),
(3,     1,      2,      1),
(4,     3,      3,      0),
(5,     4,      4,      0),
(6,     3,      4,      1),
(7,     5,      5,      0),
(8,     6,      6,      0),
(9,     5,      6,      1),
(10,    7,      7,      0),
(11,    8,      8,      0),
(12,    7,      8,      1),
(13,    9,      9,      0),
(14,    10,     10,     0),
(15,    11,     11,     0),
(16,    12,     12,     0),
(17,    13,     13,     0),
(18,    12,     13,     1),
(19,    14,     14,     0),
(20,    15,     15,     0),
(21,    14,     15,     1),
(22,    16,     16,     0),
(23,    17,     17,     0),
(24,    16,     17,     1),
(25,    18,     18,     0),
(26,    19,     19,     0),
(27,    18,     19,     1),
(28,    20,     20,     0),
(29,    21,     21,     0),
(30,    22,     22,     0),
(31,    21,     22,     1),
(32,    23,     23,     0),
(33,    24,     24,     0),
(34,    25,     25,     0),
(35,    24,     25,     1),
(36,    26,     26,     0),
(37,    27,     27,     0),
(38,    28,     28,     0),
(39,    29,     29,     0),
(40,    28,     29,     1),
(41,    30,     30,     0),
(42,    31,     31,     0),
(43,    32,     32,     0),
(44,    31,     32,     1),
(45,    33,     33,     0),
(46,    34,     34,     0),
(47,    35,     35,     0),
(48,    34,     35,     1),
(49,    36,     36,     0),
(50,    37,     37,     0),
(51,    38,     38,     0),
(52,    37,     38,     1),
(53,    39,     39,     0),
(54,    40,     40,     0),
(55,    39,     40,     1),
(56,    41,     41,     0),
(57,    42,     42,     0),
(58,    41,     42,     1),
(59,    43,     43,     0),
(60,    44,     44,     0),
(61,    45,     45,     0),
(62,    44,     45,     1),
(63,    46,     46,     0),
(64,    47,     47,     0),
(65,    48,     48,     0),
(66,    47,     48,     1),
(67,    49,     49,     0),
(68,    50,     50,     0),
(69,    51,     51,     0),
(70,    50,     51,     1),
(71,    52,     52,     0),
(72,    53,     53,     0),
(73,    54,     54,     0),
(74,    53,     54,     1),
(75,    55,     55,     0),
(76,    56,     56,     0),
(77,    57,     57,     0),
(78,    56,     57,     1),
(79,    58,     58,     0),
(80,    59,     59,     0),
(81,    60,     60,     0),
(82,    59,     60,     1),
(83,    61,     61,     0),
(84,    62,     62,     0),
(85,    63,     63,     0),
(86,    62,     63,     1),
(87,    64,     64,     0),
(88,    65,     65,     0),
(89,    66,     66,     0),
(90,    67,     67,     0),
(91,    66,     67,     1),
(92,    68,     68,     0),
(93,    69,     69,     0),
(94,    70,     70,     0),
(95,    69,     70,     1),
(96,    71,     71,     0);

INSERT INTO `component_disk_browser_file` (`id`, `directory_id`, `name`, `unique_name`, `stored_name`, `mime_type`) VALUES
    (12,    32,     'protocol_2_14 (12).odt',       '0fomihur7w',   'b4e5e57c-6470f9ac-900bb1a7.odt',       'application/vnd.oasis.opendocument.text'),
(14,    25,     'SE-02_10 (1).odt',     'zrffb97kyb',   '5d491bf5-7f6c558d-ae147f4e.odt',       'application/vnd.oasis.opendocument.text'),
(15,    25,     'SE-02_18.odt', 'g0wv7um0cg',   '7d3efeb4-17f8ca02-d6f83e72.odt',       'application/vnd.oasis.opendocument.text'),
(17,    51,     'protocol_2_14.doc',    'iyrh0zivfs',   '00474431-f94aafff-30a6f021.doc',       'application/msword'),
(29,    42,     'SE-01_24.odt', '59as4l3a22',   '9c99da6c-4fa884ac-2aebe353.odt',       'application/vnd.oasis.opendocument.text'),
(43,    48,     'SE-01_28.docx',        'wn1ks9ad33',   '2297f0a2-cb1c2da2-096e0a84.docx',      'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
(51,    45,     'SE-01_26.odt', 'i75hn2zcba',   '6320ad5c-b3247b9d-6a1549d4.odt',       'application/vnd.oasis.opendocument.text'),
(52,    67,     'SE-02_41.docx',        '43oc05o0xf',   '63c5eac6-d0187ecc-31cbc180.docx',      'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
(53,    67,     'SE-02_42.odt', 'vr0ux4wnxs',   '62b52de2-b4367501-5e92a350.odt',       'application/vnd.oasis.opendocument.text'),
(54,    67,     'SE-02-plan-kontrol (17).docx', 'qxqfu1amxq',   'c530c65c-3551210c-464ec96e.docx',      'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
(55,    70,     'SE-02_44.odt', '3lk3x192uz',   '51298b04-bbd40dfb-a578e0da.odt',       'application/vnd.oasis.opendocument.text'),
(56,    70,     'SE-02_45.odt', 'hj39323vth',   '1f163168-c5f8b830-a7495e9d.odt',       'application/vnd.oasis.opendocument.text'),
(57,    70,     'SE-02-plan-kontrol.odt',       'kdaa3egwav',   'd8390ca2-dc6c7f83-4c542f4f.odt',       'application/vnd.oasis.opendocument.text');

INSERT INTO `sigma_archive_history` (`id`, `directory_id`, `plan_id`, `position_id`, `created_at`, `end_of_order`, `start_of_order`, `content`, `subject`, `number`, `number_offer`, `state`, `user_id`) VALUES
    (1,     26,     5,      9,      '2019-04-20 23:32:32',  NULL,   '2019-04-02',   'Doplnění maziva',      'Oprava ložiska',       '57862542',     'DV1801065789', 'f',    2),
(3,     36,     8,      9,      '2019-04-21 01:08:04',  NULL,   '2018-12-04',   'sssssssssasdasdas',    'aasdasd',      'asdadas',      'dsadsasd',     'c',    2),
(6,     49,     13,     10,     '2019-04-22 14:52:55',  NULL,   '2019-03-31',   NULL,   'xxx',  NULL,   'xxx',  'a',    2),
(7,     52,     14,     8,      '2019-04-22 14:53:32',  NULL,   '2019-04-01',   NULL,   'ssss', 'ss',   'xcyxcyxcyxcy', 'f',    2),
(8,     55,     15,     10,     '2019-04-22 14:54:43',  NULL,   '2019-04-21',   NULL,   'ssss', NULL,   'ssss', 'o',    2),
(9,     58,     16,     10,     '2019-04-22 14:55:55',  NULL,   '2019-04-08',   NULL,   'ssss', NULL,   'asddsad',      'o',    2),
(11,    64,     18,     10,     '2019-04-22 14:56:50',  NULL,   '2019-04-11',   NULL,   'asdad',        NULL,   'yssss',        'o',    2),
(12,    68,     19,     9,      '2019-04-23 22:14:49',  NULL,   '2019-04-05',   NULL,   'dddd', 'asdadasd',     'asdasda',      'f',    2),
(13,    71,     20,     9,      '2019-05-12 11:22:10',  '2019-05-16',   '2019-05-01',   NULL,   'Kontrola teploty',     'DV1904556584', 'OP152468',     'f',    2);

INSERT INTO `sigma_archive_kind` (`id`, `name`, `json_form`, `size_directories`, `position_directories`) VALUES
    (1,     'Armatura',     NULL,   '',     ''),
(2,     'Čerpadlo',     NULL,   '',     ''),
(3,     'Motory',       NULL,   '',     '');

INSERT INTO `sigma_archive_position` (`id`, `size_directory_id`, `workspace_id`, `directory_id`, `name`, `uid`, `serial_number`, `comment`) VALUES
    (8,     1,      1,      8,      'ABC',  '12.4.8.3',     NULL,   NULL),
(9,     2,      2,      9,      'ABC 4',        '6.1.1.4',      '9875FVS-9',    NULL),
(10,    1,      1,      10,     'DV 25',        '12.1.8.3',     NULL,   NULL),
(11,    10,     1,      65,     'TeST', '789.654.123.1',        '8797-CE-5456', 'sfsfsds');

INSERT INTO `sigma_archive_producer` (`id`, `name`) VALUES
    (2,     'SIGMA GROUP a.s.'),
(1,     'ŠKODA a.s.');

INSERT INTO `sigma_archive_size` (`id`, `name`) VALUES
    (1,     '100'),
(2,     '200'),
(3,     '400'),
(4,     '600');

INSERT INTO `sigma_archive_size_directory` (`id`, `type_id`, `size_id`) VALUES
    (1,     1,      1),
(3,     1,      2),
(5,     1,      3),
(7,     1,      4),
(2,     2,      1),
(4,     2,      2),
(8,     3,      2),
(10,    3,      4);

INSERT INTO `sigma_archive_type` (`id`, `sizes_directory_id`, `producer_id`, `kind_id`, `name`) VALUES
    (1,     1,      1,      1,      'CHN'),
(2,     2,      2,      2,      'AMG'),
(3,     13,     1,      2,      'asdadasd');

INSERT INTO `sigma_archive_workspace` (`id`, `name`) VALUES
    (4,     'EDU'),
(1,     'ETE'),
(3,     'Lutín'),
(2,     'Třebíč');

INSERT INTO `sigma_mocev_control_type` (`id`, `name`) VALUES
    ('P',   'Protokol'),
('V',   'Vizualní');

INSERT INTO `sigma_mocev_decree` (`id`, `name`) VALUES
    (2,     '358/16 Sb.'),
(1,     '408/16 Sb.');

INSERT INTO `sigma_mocev_plan` (`id`, `type_id`, `position_id`, `workspace_id`, `source_plan_id`, `directory_id`, `generate_plan`, `file_plan_id`) VALUES
    (1,     NULL,   NULL,   NULL,   NULL,   15,     1,      NULL),
(2,     2,      NULL,   NULL,   NULL,   17,     1,      NULL),
(3,     NULL,   NULL,   NULL,   2,      19,     1,      NULL),
(4,     NULL,   NULL,   NULL,   2,      22,     1,      NULL),
(5,     NULL,   NULL,   NULL,   2,      25,     0,      NULL),
(6,     NULL,   NULL,   NULL,   2,      29,     1,      NULL),
(8,     NULL,   NULL,   NULL,   2,      35,     0,      NULL),
(9,     1,      NULL,   NULL,   NULL,   38,     1,      NULL),
(13,    NULL,   NULL,   NULL,   9,      48,     1,      NULL),
(14,    NULL,   NULL,   NULL,   9,      51,     0,      NULL),
(15,    NULL,   NULL,   NULL,   9,      54,     1,      NULL),
(16,    NULL,   NULL,   NULL,   9,      57,     1,      NULL),
(17,    NULL,   NULL,   NULL,   9,      60,     1,      NULL),
(18,    NULL,   NULL,   NULL,   9,      63,     1,      NULL),
(19,    NULL,   NULL,   NULL,   2,      67,     1,      54),
(20,    NULL,   NULL,   NULL,   2,      70,     1,      57);

INSERT INTO `sigma_mocev_plan_item` (`id`, `plan_id`, `name`, `finished_at`, `generated`, `order_no`, `protocol_id`, `file_id`, `control_type_id`, `source_item_id`, `decree_id`, `watched`, `protocol_values`, `approved_by`, `u_name`) VALUES
    (1,     1,      'asddd',        NULL,   0,      1,      NULL,   NULL,   'V',    NULL,   2,      0,      '{}',   NULL,   NULL),
(2,     1,      'Teplotní zkouška 2',   NULL,   0,      2,      1,      NULL,   'P',    NULL,   2,      1,      '{}',   NULL,   NULL),
(3,     2,      '654',  NULL,   0,      1,      NULL,   NULL,   'V',    NULL,   1,      0,      '{}',   NULL,   NULL),
(4,     2,      'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    NULL,   NULL,   0,      '{}',   NULL,   NULL),
(5,     3,      '654',  NULL,   0,      1,      NULL,   NULL,   'V',    3,      1,      0,      '{}',   NULL,   NULL),
(6,     3,      'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    4,      NULL,   0,      '{}',   NULL,   NULL),
(7,     4,      '654',  NULL,   0,      1,      NULL,   NULL,   'V',    3,      1,      0,      '{}',   NULL,   NULL),
(8,     4,      'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    4,      NULL,   0,      '{}',   NULL,   NULL),
(9,     5,      '654',  '2019-04-21',   0,      1,      NULL,   NULL,   'V',    3,      1,      0,      '{}',   NULL,   NULL),
(10,    5,      'Teplotní zkouška',     '2019-04-22',   0,      2,      1,      14,     'P',    4,      NULL,   0,      '{}',   NULL,   NULL),
(11,    6,      '654',  NULL,   0,      1,      NULL,   NULL,   'V',    3,      1,      0,      '{}',   NULL,   NULL),
(12,    6,      'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    4,      NULL,   0,      '{}',   NULL,   NULL),
(15,    8,      '654',  NULL,   0,      1,      NULL,   NULL,   'V',    3,      1,      0,      '{}',   NULL,   NULL),
(16,    8,      'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    4,      NULL,   0,      '{}',   NULL,   NULL),
(17,    5,      '5555', '2019-04-22',   0,      3,      NULL,   NULL,   'V',    NULL,   NULL,   0,      '{}',   NULL,   NULL),
(18,    5,      'Teplotní zkouška',     '2019-04-22',   0,      4,      1,      15,     'P',    NULL,   2,      0,      '{}',   NULL,   NULL),
(19,    9,      'test', NULL,   0,      1,      NULL,   NULL,   'V',    NULL,   2,      0,      '{}',   NULL,   NULL),
(20,    9,      'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    NULL,   NULL,   0,      '{}',   NULL,   NULL),
(27,    13,     'test', '2019-04-22',   0,      1,      NULL,   NULL,   'V',    19,     2,      0,      '{}',   NULL,   NULL),
(28,    13,     'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    20,     NULL,   0,      '{}',   NULL,   NULL),
(29,    14,     'test', '2019-04-22',   0,      1,      NULL,   NULL,   'V',    19,     2,      0,      '{}',   NULL,   NULL),
(30,    14,     'Teplotní zkouška',     '2019-04-22',   0,      2,      1,      17,     'P',    20,     NULL,   0,      '{}',   NULL,   NULL),
(31,    15,     'test', NULL,   0,      1,      NULL,   NULL,   'V',    19,     2,      0,      '{}',   NULL,   NULL),
(32,    15,     'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    20,     NULL,   0,      '{}',   NULL,   NULL),
(33,    16,     'test', NULL,   0,      1,      NULL,   NULL,   'V',    19,     2,      0,      '{}',   NULL,   NULL),
(34,    16,     'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    20,     NULL,   0,      '{}',   NULL,   NULL),
(35,    17,     'test', NULL,   0,      1,      NULL,   NULL,   'V',    19,     2,      0,      '{}',   NULL,   NULL),
(36,    17,     'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    20,     NULL,   0,      '{}',   NULL,   NULL),
(37,    18,     'test', NULL,   0,      1,      NULL,   NULL,   'V',    19,     2,      0,      '{}',   NULL,   NULL),
(38,    18,     'Teplotní zkouška',     NULL,   0,      2,      1,      NULL,   'P',    20,     NULL,   0,      '{}',   NULL,   NULL),
(39,    2,      '1 Teplotní zkouška',   NULL,   0,      3,      1,      NULL,   'P',    NULL,   2,      1,      '{}',   NULL,   NULL),
(40,    19,     '654',  '2019-04-23',   0,      1,      NULL,   NULL,   'V',    3,      1,      0,      '{}',   2,      NULL),
(41,    19,     'Teplotní zkouška',     '2019-04-23',   0,      2,      1,      52,     'P',    4,      NULL,   0,      '{}',   2,      'SE-02_41'),
(42,    19,     '1 Teplotní zkouška',   '2019-04-23',   0,      3,      1,      53,     'P',    39,     2,      1,      '{\\\"control.name\\\":\\\"1 Teplotn\\\\u00ed zkou\\\\u0161ka\\\",\\\"default.criteria.01\\\":\\\"0,5mm\\\",\\\"default.criteria.02\\\":\\\"0,5mm\\\",\\\"default.criteria.03\\\":\\\"0,5mm\\\",\\\"default.criteria.04\\\":\\\"0,5mm\\\",\\\"default.page\\\":\\\"1\\\",\\\"default.pageCount\\\":\\\"1\\\",\\\"default.x1.01\\\":\\\"12\\\",\\\"default.x1.02\\\":\\\"9\\\",\\\"default.x2.01\\\":\\\"6\\\",\\\"default.x2.02\\\":\\\"3\\\",\\\"default.y1.01\\\":\\\"12\\\",\\\"default.y1.02\\\":\\\"9\\\",\\\"default.y2.01\\\":\\\"6\\\",\\\"default.y2.02\\\":\\\"3\\\",\\\"device.name\\\":\\\"ABC 4\\\",\\\"device.serialNumber\\\":\\\"9875FVS-9\\\",\\\"device.type\\\":\\\"AMG\\\",\\\"device.uid\\\":\\\"6.1.1.4\\\",\\\"history.number\\\":\\\"asdadasd\\\",\\\"input.date\\\":\\\"Zp\\\\u016fsob m\\\\u011b\\\\u0159en\\\\u00ed:\\\",\\\"input.delta.01\\\":\\\"\\\",\\\"input.delta.02\\\":\\\"\\\",\\\"input.delta.03\\\":\\\"\\\",\\\"input.delta.04\\\":\\\"\\\",\\\"input.delta.05\\\":\\\"\\\",\\\"input.delta.06\\\":\\\"\\\",\\\"input.delta.07\\\":\\\"\\\",\\\"input.delta.08\\\":\\\"\\\",\\\"input.footer.assign\\\":\\\"\\\",\\\"input.footer.name\\\":\\\"\\\",\\\"input.result.01\\\":\\\"\\\",\\\"input.result.02\\\":\\\"\\\",\\\"input.result.03\\\":\\\"\\\",\\\"input.result.04\\\":\\\"\\\",\\\"input.result.05\\\":\\\"\\\",\\\"input.result.06\\\":\\\"\\\",\\\"input.result.07\\\":\\\"\\\",\\\"input.result.08\\\":\\\"\\\",\\\"input.x1.01\\\":\\\"10.6\\\",\\\"input.x1.02\\\":\\\"10.5\\\",\\\"input.x1.03\\\":\\\"6.2\\\",\\\"input.x1.04\\\":\\\"6.1\\\",\\\"input.x2.01\\\":\\\"10.6\\\",\\\"input.x2.02\\\":\\\"10.3\\\",\\\"input.x2.03\\\":\\\"6.2\\\",\\\"input.x2.04\\\":\\\"6.2\\\",\\\"input.y1.01\\\":\\\"10.5\\\",\\\"input.y1.02\\\":\\\"10.1\\\",\\\"input.y1.03\\\":\\\"6.1\\\",\\\"input.y1.04\\\":\\\"10.2\\\",\\\"input.y2.01\\\":\\\"10.5\\\",\\\"input.y2.02\\\":\\\"10.2\\\",\\\"input.y2.03\\\":\\\"6.1\\\",\\\"input.y2.04\\\":\\\"10.2\\\"}',    2,      'SE-02_42'),
(43,    20,     '654',  '2019-05-12',   0,      1,      NULL,   NULL,   'V',    3,      1,      0,      '{}',   2,      NULL),
(44,    20,     'Teplotní zkouška',     '2019-05-12',   0,      2,      1,      55,     'P',    4,      NULL,   0,      '{}',   2,      'SE-02_44'),
(45,    20,     '1 Teplotní zkouška',   '2019-05-12',   0,      3,      1,      56,     'P',    39,     2,      1,      '{\\\"control.name\\\":\\\"\\\",\\\"default.criteria.01\\\":\\\"0,5mm\\\",\\\"default.criteria.02\\\":\\\"0,5mm\\\",\\\"default.criteria.03\\\":\\\"0,5mm\\\",\\\"default.criteria.04\\\":\\\"0,5mm\\\",\\\"default.page\\\":\\\"1\\\",\\\"default.pageCount\\\":\\\"1\\\",\\\"default.x1.01\\\":\\\"12\\\",\\\"default.x1.02\\\":\\\"9\\\",\\\"default.x2.01\\\":\\\"6\\\",\\\"default.x2.02\\\":\\\"3\\\",\\\"default.y1.01\\\":\\\"12\\\",\\\"default.y1.02\\\":\\\"9\\\",\\\"default.y2.01\\\":\\\"6\\\",\\\"default.y2.02\\\":\\\"3\\\",\\\"device.name\\\":\\\"ABC 4\\\",\\\"device.serialNumber\\\":\\\"9875FVS-9\\\",\\\"device.type\\\":\\\"AMG\\\",\\\"device.uid\\\":\\\"6.1.1.4\\\",\\\"history.number\\\":\\\"DV1904556584\\\",\\\"input.date\\\":\\\"Zp\\\\u016fsob m\\\\u011b\\\\u0159en\\\\u00ed:\\\",\\\"input.delta.01\\\":\\\"\\\",\\\"input.delta.02\\\":\\\"\\\",\\\"input.delta.03\\\":\\\"\\\",\\\"input.delta.04\\\":\\\"\\\",\\\"input.delta.05\\\":\\\"\\\",\\\"input.delta.06\\\":\\\"\\\",\\\"input.delta.07\\\":\\\"\\\",\\\"input.delta.08\\\":\\\"\\\",\\\"input.footer.assign\\\":\\\"\\\",\\\"input.footer.name\\\":\\\"\\\",\\\"input.result.01\\\":\\\"\\\",\\\"input.result.02\\\":\\\"\\\",\\\"input.result.03\\\":\\\"\\\",\\\"input.result.04\\\":\\\"\\\",\\\"input.result.05\\\":\\\"\\\",\\\"input.result.06\\\":\\\"\\\",\\\"input.result.07\\\":\\\"\\\",\\\"input.result.08\\\":\\\"\\\",\\\"input.x1.01\\\":\\\"12.1\\\",\\\"input.x1.02\\\":\\\"12.8\\\",\\\"input.x1.03\\\":\\\"5.5\\\",\\\"input.x1.04\\\":\\\"8.2\\\",\\\"input.x2.01\\\":\\\"12.2\\\",\\\"input.x2.02\\\":\\\"12.8\\\",\\\"input.x2.03\\\":\\\"5.2\\\",\\\"input.x2.04\\\":\\\"8\\\",\\\"input.y1.01\\\":\\\"12.5\\\",\\\"input.y1.02\\\":\\\"9.8\\\",\\\"input.y1.03\\\":\\\"9.6\\\",\\\"input.y1.04\\\":\\\"9.1\\\",\\\"input.y2.01\\\":\\\"12.2\\\",\\\"input.y2.02\\\":\\\"9.85\\\",\\\"input.y2.03\\\":\\\"9.5\\\",\\\"input.y2.04\\\":\\\"9.1\\\"}',   2,      'SE-02_45');

INSERT INTO `sigma_mocev_protocol_template` (`id`, `workspace_id`, `number`, `structure`, `structure_defaults`, `name`, `uid`, `directory_id`, `file_id`) VALUES
    (1,     NULL,   '200',  '{\\\"header\\\":{\\\"table\\\":{\\\"data\\\":[{\\\"data\\\":[{\\\"width\\\":2625,\\\"content\\\":{\\\"text\\\":\\\"SIGMA GROUP a.s.\\\\ndivize Energo\\\\nJana Sigmunda 313\\\\n783 49 Lut\\\\u00edn\\\\n \\\\u010cesk\\\\u00e1 Republika\\\"},\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"valign\\\":\\\"center\\\"}},{\\\"width\\\":4000,\\\"content\\\":{\\\"blockText\\\":{\\\"title\\\":\\\"PROTOKOL O KONTROLE\\\",\\\"title2\\\":\\\"(inspek\\\\u010dn\\\\u00ed certifik\\\\u00e1t dle \\\\u010cSN EN 10 204)\\\",\\\"text\\\":\\\"Evid. \\\\u010d.: %evid.no\\\"}},\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"valign\\\":\\\"center\\\"}},{\\\"width\\\":2625,\\\"content\\\":{\\\"text\\\":\\\"Z\\\\u00e1k\\\\u00e1zka (PP) \\\\u010d.: %history.number%\\\"},\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"valign\\\":\\\"center\\\"}}]},{\\\"data\\\":[{\\\"width\\\":null,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"width\\\":null,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"content\\\":{\\\"text\\\":\\\"P\\\\u0159\\\\u00edloha \\\\u010d.:\\\"},\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"valign\\\":\\\"center\\\"}}]},{\\\"data\\\":[{\\\"width\\\":null,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"width\\\":null,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"content\\\":{\\\"text\\\":\\\"Strana: %default.page%\\\\/%default.pageCount%\\\"},\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"valign\\\":\\\"center\\\"}}]}]}},\\\"body\\\":[{\\\"table\\\":{\\\"data\\\":[{\\\"data\\\":[{\\\"style\\\":[],\\\"content\\\":{\\\"text\\\":\\\"N\\\\u00e1zev za\\\\u0159\\\\u00edzen\\\\u00ed: %device.name%\\\"}},{\\\"style\\\":[],\\\"content\\\":{\\\"text\\\":\\\"Typ: %device.type%\\\"}}]},{\\\"data\\\":[{\\\"style\\\":[],\\\"content\\\":{\\\"text\\\":\\\"Projektov\\\\u00e9 \\\\u010d\\\\u00edslo: %device.uid%\\\"}},{\\\"style\\\":[],\\\"content\\\":{\\\"text\\\":\\\"Seriov\\\\u00e9 \\\\u010d\\\\u00edslo: %device.serialNumber%\\\"}}]},{\\\"data\\\":[{\\\"width\\\":5550,\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":\\\"Odb\\\\u011bratel:\\\"}},{\\\"width\\\":2500,\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"bgColor\\\":\\\"CCCCCC\\\"},\\\"content\\\":{\\\"blockText\\\":{\\\"text\\\":{\\\"data\\\":\\\"Vyhl\\\\u00e1\\\\u0161ka: 408\\\\/16 Sb.\\\\n358\\\\/16 Sb.\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"end\\\"}}}}},{\\\"width\\\":600,\\\"style\\\":{\\\"bgColor\\\":\\\"CCCCCC\\\"},\\\"content\\\":{\\\"decreeValue\\\":{\\\"id\\\":0,\\\"data\\\":\\\"ano\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":600,\\\"style\\\":{\\\"bgColor\\\":\\\"CCCCCC\\\"},\\\"content\\\":{\\\"decreeValue\\\":{\\\"id\\\":0,\\\"data\\\":\\\"ne\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":null,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"width\\\":null,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"width\\\":600,\\\"style\\\":{\\\"bgColor\\\":\\\"CCCCCC\\\"},\\\"content\\\":{\\\"decreeValue\\\":{\\\"id\\\":1,\\\"data\\\":\\\"ano\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":600,\\\"style\\\":{\\\"bgColor\\\":\\\"CCCCCC\\\"},\\\"content\\\":{\\\"decreeValue\\\":{\\\"id\\\":1,\\\"data\\\":\\\"ne\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"style\\\":[],\\\"content\\\":{\\\"text\\\":\\\"N\\\\u00e1zev kontroly: %control.name%\\\"}}]},{\\\"data\\\":[{\\\"style\\\":[],\\\"content\\\":{\\\"text\\\":\\\"Normy a p\\\\u0159edstavy pro hodnocen\\\\u00ed (IPZJ, V\\\\u00ddKRES \\\\u010d., \\\\u010d\\\\u00edslo operace TLP, MP, metodick\\\\u00fd list, odchylkov\\\\u00fd list aj.):\\\"}}]},{\\\"data\\\":[{\\\"style\\\":[],\\\"content\\\":{\\\"text\\\":\\\"Typ a rozm\\\\u011br pou\\\\u017eit\\\\u00e9ho t\\\\u011bsn\\\\u011bn\\\\u00ed:\\\\n\\\"}}],\\\"hidden\\\":true},{\\\"data\\\":[{\\\"style\\\":{\\\"vMerge\\\":\\\"restart\\\",\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":\\\"Za\\\\u0159\\\\u00edzen\\\\u00ed a prost\\\\u0159edky pro zaji\\\\u0161t\\\\u011bn\\\\u00ed kontroly: \\\\n\\\\n\\\"}}]},{\\\"data\\\":[{\\\"content\\\":{\\\"text\\\":\\\"Kontrolu provedl\\\"}}]},{\\\"data\\\":[{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Datum\\\",\\\"style\\\":{\\\"bold\\\":true}}},\\\"style\\\":{\\\"bgColor\\\":\\\"CCCCCC\\\"}},{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Jm\\\\u00e9no\\\",\\\"style\\\":{\\\"bold\\\":true}}},\\\"style\\\":{\\\"bgColor\\\":\\\"CCCCCC\\\"}},{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Podpis\\\",\\\"style\\\":{\\\"bold\\\":true}}},\\\"style\\\":{\\\"bgColor\\\":\\\"CCCCCC\\\"}}]},{\\\"data\\\":[{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.date%\\\",\\\"style\\\":{\\\"size\\\":22}}}},{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.name%\\\",\\\"style\\\":{\\\"size\\\":22}}}},{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\" \\\",\\\"style\\\":{\\\"size\\\":22}}}}]},{\\\"data\\\":[{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%date.02%\\\",\\\"style\\\":{\\\"size\\\":22}}}},{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%name.02%\\\",\\\"style\\\":{\\\"size\\\":22}}}},{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%signature.02%\\\",\\\"style\\\":{\\\"size\\\":22}}}}],\\\"hidden\\\":true},{\\\"data\\\":[{\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Zp\\\\u016fsob m\\\\u011b\\\\u0159en\\\\u00ed: \\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\\n\\\"}}}]}]}},{\\\"text\\\":{\\\"data\\\":\\\"V\\\\u00fdsledky m\\\\u011b\\\\u0159en\\\\u00ed:\\\"}},{\\\"table\\\":{\\\"data\\\":[{\\\"data\\\":[{\\\"width\\\":2500,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Vz\\\\u00e1jemn\\\\u00e9 ustanoven\\\\u00ed p\\\\u0159\\\\u00edrub\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Poloha odchylky\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Krit\\\\u00e9ria\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2300,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Sac\\\\u00ed p\\\\u0159\\\\u00edruba\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2300,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"V\\\\u00fdtla\\\\u010dn\\\\u00e1 p\\\\u0159\\\\u00edruba\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":2500,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"width\\\":1150,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"width\\\":1000,\\\"style\\\":{\\\"vMerge\\\":\\\"continue\\\"}},{\\\"width\\\":900,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"M\\\\u011b\\\\u0159en\\\\u00ed\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"&#916;,&#936;\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Hod.\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"M\\\\u011b\\\\u0159en\\\\u00ed\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"&#916;\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Hod.\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":1625,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Rovnob\\\\u011b\\\\u017enost\\\\n&#916; (mm)\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Y1 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.y1.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.criteria.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y1.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y1.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":null},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Y2 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.y2.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y2.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y2.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null}]},{\\\"data\\\":[{\\\"width\\\":null},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Y1 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.y1.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.criteria.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y1.03%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.03%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.03%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y1.04%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.04%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.04%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":null},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Y2 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.y2.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y2.03%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.y2.04%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null}]},{\\\"data\\\":[{\\\"width\\\":1625,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"P\\\\u0159esazen\\\\u00ed\\\\n&#936; (mm)\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"X1 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.x1.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.criteria.03%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x1.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.05%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.05%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x1.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.06%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.06%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":null},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"X2 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.x2.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x2.01%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x2.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null}]},{\\\"data\\\":[{\\\"width\\\":null},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"X1 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.x1.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.criteria.04%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x1.03%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.07%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.07%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x1.04%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.delta.08%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":700,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.result.08%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":null},{\\\"width\\\":875,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"X2 (mm)\\\",\\\"style\\\":{\\\"size\\\":8},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":1150,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%default.x2.02%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x2.03%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null},{\\\"width\\\":900,\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.x2.04%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":null},{\\\"width\\\":null}]}],\\\"style\\\":\\\"tableBorder\\\"}},{\\\"text\\\":{\\\"data\\\":\\\"Hodnocen\\\\u00ed: V - Vyhovuje; N - Nevyhovuje\\\"}},{\\\"text\\\":\\\"\\\"},{\\\"table\\\":{\\\"data\\\":[{\\\"data\\\":[{\\\"style\\\":{\\\"valign\\\":\\\"center\\\",\\\"bgColor\\\":\\\"CCCCCC\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Vystavil\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}]},{\\\"data\\\":[{\\\"width\\\":2000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Datum\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2800,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Jm\\\\u00e9no\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2450,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Funkce\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"Podpis\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}],\\\"style\\\":\\\"tableDefault\\\"},{\\\"data\\\":[{\\\"width\\\":2000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"2019-03-24\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2800,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.footer.assign%\\\",\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2450,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"%input.footer.name%\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}},{\\\"width\\\":2000,\\\"style\\\":{\\\"valign\\\":\\\"center\\\"},\\\"content\\\":{\\\"text\\\":{\\\"data\\\":\\\"\\\\n\\\",\\\"style\\\":{\\\"bold\\\":true},\\\"alignment\\\":{\\\"alignment\\\":\\\"center\\\"}}}}],\\\"style\\\":\\\"tableDefault\\\"}]}}],\\\"styles\\\":{\\\"tableDefault\\\":{\\\"borderSize\\\":6,\\\"borderColor\\\":\\\"000000\\\"},\\\"tableFirstRowDefault\\\":null,\\\"tableBorder\\\":{\\\"borderSize\\\":12,\\\"borderColor\\\":\\\"000000\\\"},\\\"tableFirstRowBorder\\\":null}}',        '{\\\"default.page\\\":1,\\\"default.pageCount\\\":1,\\\"default.y1.01\\\":12,\\\"default.y1.02\\\":9,\\\"default.y2.01\\\":6,\\\"default.y2.02\\\":3,\\\"default.x1.01\\\":12,\\\"default.x1.02\\\":9,\\\"default.x2.01\\\":6,\\\"default.x2.02\\\":3,\\\"default.criteria.01\\\":\\\"0,5mm\\\",\\\"default.criteria.02\\\":\\\"0,5mm\\\",\\\"default.criteria.03\\\":\\\"0,5mm\\\",\\\"default.criteria.04\\\":\\\"0,5mm\\\"}',       'Teplotní zkouška',     'priruby',      NULL,   NULL);


INSERT INTO `workspace_users` (`workspace_id`, `user_id`) VALUES
    (1,     2),
(1,     3),
(2,     3),
(3,     1),
(3,     3);

-- 2019-05-12 11:03:30
");
    }


    public function down()
    {
        $this->execute("DELETE FROM  sigma_archive_kind");
        $this->execute("DELETE FROM  sigma_archive_producer");
        $this->execute("DELETE FROM  sigma_archive_size");
        $this->execute("DELETE FROM  sigma_archive_workspace");
        $this->execute("DELETE FROM  sigma_archive_type");
        $this->execute("DELETE FROM  sigma_mocev_decree");
        $this->execute("DELETE FROM  sigma_mocev_control_type");
        $this->execute("DELETE FROM  sigma_mocev_protocol_template");
        $this->execute("DELETE FROM  component_disk_browser_directory");
    }
}
