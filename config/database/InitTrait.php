<?php

trait InitTrait
{
    public function initEm()
    {

        /** @var \Nette\DI\Container $bootstrap */
        $container = require_once __DIR__ . "/../../bootstrap.php";
        $em = $container->getByType(\Doctrine\ORM\EntityManagerInterface::class);

        var_dump($em);

        return $em;
    }
}