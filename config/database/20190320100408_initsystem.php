<?php

class InitSystem extends \Phinx\Migration\AbstractMigration
{
    public function up()
    {
        $id = 0;
        $this->table("system_role")->insert([
            ["id" => ++$id,  "parent_id" => null, "name" => "super_admin"],
            ["id" => ++$id,  "parent_id" => $id-1, "name" => "admin"],
            ["id" => ++$id,  "parent_id" => $id-1, "name" => "user"],
            ["id" => ++$id,  "parent_id" => $id-1, "name" => "guest"],
        ])->save();
        $id = 0;
        $this->table("system_user")->insert([
            ["id" => ++$id,  "name" => "Admin", "surname" => "Super", "email" => "superadmin@web.com", "password" => \Nette\Security\Passwords::hash("He32slo")],
            ["id" => ++$id,  "name" => null, "surname" => "Admin", "email" => "admin@web.com", "password" => \Nette\Security\Passwords::hash("He32slo")],
            ["id" => ++$id,  "name" => null, "surname" => "User", "email" => "user@web.com", "password" => \Nette\Security\Passwords::hash("He32slo")],
        ])->save();
        $this->table("system_user_role")->insert([
            ["user_id" => 1, "role_id" => 1],
            ["user_id" => 2, "role_id" => 2],
            ["user_id" => 3, "role_id" => 3],
        ])->save();
    }


    public function down()
    {
        $this->execute("DELETE FROM  system_user");
        $this->execute("DELETE FROM  system_role");
    }
}
