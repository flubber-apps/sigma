let Api = {
  getContent(lang = "en") {
    switch (lang) {
      case "cz":
      case "cs":
        return require("./cz-cs.js");
      default:
        return require("./cz-cs.js");
    }
  }
};

module.exports = Api;
