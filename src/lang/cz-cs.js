module.exports = {
  message: {
    success: "Úspěch"
  },
  signIn: {
    title: "Přihlášení",
    labelEmail: "Email",
    labelPassword: "Heslo",
    submit: "Příhlásit se",
    errorMessage: "Uživatel se zadaným emailem nebo heslem neexistuje"
  },
  dataTable: {
    body: {
      noMatch: "Omlouváme se, ale nenašli jsme žádné výsledky",
      toolTip: "Seřadit"
    },
    pagination: {
      next: "Další strana",
      previous: "Předchozí strana",
      rowsPerPage: "Řádky na stranu:",
      displayRows: "z"
    },
    toolbar: {
      search: "Vyhledávání",
      downloadCsv: "Stažení CSV",
      print: "Tisk",
      viewColumns: "Zobrazit sloupce",
      filterTable: "Filtrovat"
    },
    filter: {
      all: "Vše",
      title: "FILTRY",
      reset: "RESET"
    },
    viewColumns: {
      title: "Zobrazit sloupce",
      titleAria: "Zobrazit/skrýt sloupce tabulky"
    },
    selectedRows: {
      text: "Vybrané řádek(řádky)",
      delete: "Smazat",
      deleteAria: "Smazat vynrané řádky"
    }
  },
  title: {
    deviceSelect: "Vybírání zařízení"
  },
  text: {
    manageOfCategory: "Správa kategorie",
    kind: "Druh",
    kindAdd: "Přidat druh",
    kindName: "Název druhu",
    kindErrorMissing: "Druh nenalezen",
    kindErrorDuplicity: "Druh se stejným názvem již existuje",
    kindAddSuccess: "Druh úspěšně vytvořen",
    producer: "Výrobce",
    producerAdd: "Přidat výrobce",
    producerName: "Jméno výrobce",
    producerErrorMissing: "Výobce nenalezen",
    producerErrorDuplicity: "Výrobce se stejným názvem již existuje",
    producerAddSuccess: "Výrobce úspěšně vytvořen",
    type: "Typ",
    typeErrorDuplicity: "Typ již existuje",
    typeAssigned: "Přiřazené velikosti",
    typeAssignedAdd: "Přiřadit velikost",
    userAssigned: "Přiřazení uživatelé",
    userAssignedAdd: "Přiřadit uživatele",
    user: "Uživatel",
    typeAdd: "Přidat typ",
    size: "Velikost",
    sizeAdd: "Přidat velikost",
    sizeName: "Velikost",
    sizeErrorMissing: "Velikost nenalezena",
    sizeErrorDuplicity: "Velikost se stejným názvem již existuje",
    sizeAddSuccess: "Velikost úspěšně vytvořena",
    workspace: "Pracoviště",
    workspaceAdd: "Přidat pracoviště",
    workspaceName: "Pracoviště",
    workspaceErrorMissing: "Pracoviště nenalezeno",
    workspaceErrorDuplicity: "Pracoviště se stejným názvem již existuje",
    workspaceAddSuccess: "Pracoviště úspěšně vytvořeno",
    position: "Pozice",
    positionAdd: "Přidat pozici",
    positionName: "Název technologického systému",
    positionUid: "Projektové označení",
    positionSerialNumber: "Sériové číslo",
    positionComment: "Poznámka",
    history: "Zakázka",
    orderNew: "Nová zakázka",
    orderSubject: "Předmět",
    orderNumber: "Číslo zakázky",
    orderNumberOffer: "Číslo obchodního případu",
    orderStartAt: "Datum začátku",
    orderEndAt: "Datum konce",
    orderContent: "Poznámka",
    planConfiguratorAddRow: "Přidat kontrolu",
    planConfiguratorNextStep: "Další krok",
    planConfiguratorFilter: "Filter",
    planConfiguratorId: "Pořadí",
    planConfiguratorTypeOfControl: "Typ kontroly",
    planConfiguratorDecree: "Vyhláška",
    planConfiguratorWatch: "Sledovat",
    planConfiguratorName: "Název",
    planConfiguratorTitle: "Plán kontrol",
    searchBox: "Vyhledávání"
  },
  user: {
    name: "Jméno",
    surname: "Přijmení",
    email: "Email"
  },
  history: {
    new: "Nová zakázka",
    subject: "Předmět",
    number: "Číslo zakázky",
    numberOffer: "Číslo obchodního případu",
    startAt: "Datum začátku",
    endAt: "Datum konce",
    content: "Poznámka",
    state: "Stav zakázky",
    operation: "Operace se zakázkou"
  },
  planConfigurator: {
    create: "Vytvořit plán",
    addRow: "Přidat kontrolu",
    importType: "Importovat od typu",
    importPosition: "Import od pozice",
    order: "Pořadí",
    controlType: "Typ kontroly",
    decree: "Vyhláška",
    watch: "Sledovat",
    name: "Název",
    finishedAt: "Datum ukončení",
    selectType: "Výběr typu",
    generatePlan: "Vytvořit plán kontrol"
  },
  protocol: {
    title: "Protokol",
    titleSearch: "Přiřazení protokolu",
    name: "Název",
    number: "Číslo protokolu"
  },
  historyDetail: {
    partPlan: "Plán kontrol",
    partPlanConfigure: "Nastavení plánu kontrol",
  },
  deviceDetail: {
    partHistory: "Archív",
    partPlan: "Nastavení plánu kontrol",
    partState: "Stav zařízení",
    partParts: "Kusovník"
  },
  button: {
    save: "Uložit",
    delete: "Smazat",
    add: "Přidat",
    remove: "Odebrat",
    open: "Otevřít",
    close: "Zavřít",
    agree: "Ano",
    disagree: "Ne",
    edit: "Upravit",
    download: "Stáhnout",
    upload: "Nahrát",
    generate: "Vygenerovat",
    back: "Zpět"
  },
  state: {
    all: "Vše",
    approve: "Schválit",
    approved: "Schváleno",
    opened: "Otevřeno",
    closed: "Uzavřeno",
    cancel: "Zrušit",
    canceled: "Zrušeno",
    open: "Otevřit",
    close: "Uzavřít",
    finished: "Dokončeno",
    finish: "Dokončit",
    unfinished: "Nedkončeno",
    done: "Splněno",
    notDone: "Nesplněno",
    current: "Současné",
    history: "Historie"
  },
  menu: {
    tlp: {
      short: "TLP",
      title: "Technická dokumentace"
    },
    mocev: {
      short: "Zakázky",
      title: "Zakázky"
    }
  }
};
