<?php

namespace Flubber\Extension\Presenters;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Router\ApiRoute;
use Flubber\Extension\Entity\Decree;
use Nette\Http\Response;

/**
 * Class MainPresenter
 *
 * @package Flubber\Extension
 * @ApiRoute("/decree")
 */
class DecreePresenter extends BasePresenter
{
    /** @var EntityManagerInterface @inject */
    public $em;

    public function actionRead($id)
    {
        if ($id) {
            $entity = $this->em->getRepository(Decree::class)->find($id);
            if ($entity) {
                $this->sendJson($entity->toArray());
            } else {
                $this->sendError("Decree with ID: {$id} not found.", Response::S404_NOT_FOUND);
            }
        } else {
            $data = [];
            foreach ($this->em->getRepository(Decree::class)->findAll() as $entity) {
                $data[] = $entity->toArray();
            }
            $this->sendJson($data);
        }
    }
}