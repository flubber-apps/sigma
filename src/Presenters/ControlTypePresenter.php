<?php

namespace Flubber\Extension\Presenters;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Router\ApiRoute;
use Flubber\Extension\Entity\ControlType;
use Nette\Http\Response;

/**
 * @package Flubber\Extension
 * @ApiRoute("/controlType")
 */
class ControlTypePresenter extends BasePresenter
{
    /** @var EntityManagerInterface @inject */
    public $em;

    public function actionRead($id)
    {
        if ($id) {
            /** @var ControlType $entity */
            $entity = $this->em->getRepository(ControlType::class)->find($id);
            if ($entity) {
                $this->sendJson($entity->toArray());
            } else {
                $this->sendError("ControlType with ID: {$id} not found.", Response::S404_NOT_FOUND);
            }
        } else {
            /** @var ControlType $entity */
            $entities = $this->em->getRepository(ControlType::class)->findAll();
            $data = [];
            foreach ($entities as $entity) {
                $data[] = $entity->toArray();
            }
            $this->sendJson($data);
        }
    }
}