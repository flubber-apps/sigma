<?php

namespace Flubber\Extension\Presenters;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Router\ApiRoute;
use Flubber\Extension\Facade\Position;
use Flubber\Extension\utils\Protocol\ProtocolPriruby;
use Nette\Application\AbortException;
use Nette\Http\Response;
use Nette\InvalidStateException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * Class ProducerPresenter
 * @package Flubber\Extension
 * @ApiRoute("/position")
 */
class PositionPresenter extends BasePresenter
{
    /** @var Position @inject */
    public $facade;
    /** @var EntityManagerInterface @inject */
    public $em;

    public function actionCreate()
    {
        try {
            $request = Json::decode($this->getHttpRequest()->getRawBody());
            if (!property_exists($request, "name")
                || (!property_exists($request, "uid") && !property_exists($request, "serialNumber"))
                || !property_exists($request, "workspaceId")
                || !property_exists($request, "sizeOfType"))
                throw new InvalidStateException("Not valid input data");
            $entity = $this->facade->insert([
                "name" => $request->name,
                "uid" => property_exists($request, "uid") ? $request->uid : null,
                "serialNumber" => property_exists($request, "serialNumber") ? $request->serialNumber : null,
                "comment" => property_exists($request, "comment") ? $request->comment : null,
                "sizeDirectory" => $request->sizeOfType,
                "workspace" => $request->workspaceId
            ], true);
            $this->sendJson([
                "status" => "success",
                "id" => $entity->getId()
            ]);
        } catch (JsonException $e) {
            $this->sendError("Not valid JSON input", Response::S400_BAD_REQUEST);
        } catch (InvalidStateException $e) {
            $this->sendError("Position already exists.", Response::S400_BAD_REQUEST, [
                "name" => "string",
                "uid" => "string (optional)",
                "serialNumber" => "string (optional)",
                "comment" => "strin (optional)",
                "sizeOfType" => "numeric",
                "workspaceId" => "numeric"
            ]);
        }
    }

    public function actionRead($id)
    {
        if ($id) {
            $entity = $this->facade->getById($id, false);
            if ($entity) {
                $this->sendJson($entity);
            } else {
                $this->sendError("Workspace with ID: {$id} not found.", Response::S404_NOT_FOUND);
            }
        } else {
            $unitOfWork = $this->em->getUnitOfWork();
            $data = [];
            foreach ($this->facade->getAll() as $entity) {
                $data[] = [
                    "id" => $entity->getId(),
                    "name" => $entity->name,
                    "uid" => $entity->uid,
                    "serialNumber" => $entity->serialNumber,
                    "sizeOfTypeId" => $unitOfWork->getEntityIdentifier($entity->sizeDirectory)["id"],
                    "workspaceId" => $unitOfWork->getEntityIdentifier($entity->workspace)["id"]
                ];
            }
            $this->sendJson($data);
        }
    }

    public function actionUpdate($id)
    {

    }

    public function actionDelete($id)
    {
        if (!$id)
            $this->sendError("Missing ID of position", Response::S400_BAD_REQUEST);

        $response = [
            "status" => "success"
        ];
        try {
            $this->facade->deleteById($id, true);
        } catch (InvalidStateException $e) {
            $this->getHttpResponse()->setCode(Response::S404_NOT_FOUND);
            $response = [
                "status" => "error",
                "message" => "Position with ID: {$id} not found."
            ];
        } finally {
            $this->sendJson($response);
        }
    }

    /**
     * @ApiRoute("/position/state", method="OPTIONS")
     */
    public function actionStateOptions() {
        $this->actionOptions();
    }

    /**
     * @param integer $id Position Id
     * @throws AbortException
     * @ApiRoute("/position/state", method="GET")
     */
    public function actionReadState($id)
    {
        try {
            $position = $this->facade->getById($id, true);
            $source = [];
            foreach ($position->histories as $history) {
                /** @var \Flubber\Extension\Entity\History  $history */
                if ($history->getState() !== "finished")
                    continue;
                foreach ($history->plan->items as $control) {
                    if ($control->watched && $control->finishedAt !== null) {
                        $protocol = $control->protocol;
                        if (!array_key_exists($protocol->uid, $source))
                            $source[$protocol->uid] = [];
                        $source[$protocol->uid][$control->finishedAt->format("Y-m-d")] = $control;
                    }
                }
            }
            $result = [];
            foreach ($source as $uid => $controls) {
                ksort($controls);
                foreach ($controls as $control) {
                    switch ($control->protocol->uid) {
                        case "priruby":
                            $helper = new ProtocolPriruby($control->values, $control->finishedAt);
                            $_result = $helper->loadData($result);
                            $result["parallelism"] = $_result[0];
                            $result["offset"] = $_result[1];
                            break;
                        default:
                    }
                }
            }
            $this->sendJson($result);
        } catch (InvalidStateException $e) {
            $this->sendError("Postion with ID: {$id} not found.", Response::S404_NOT_FOUND);
        }
    }
}