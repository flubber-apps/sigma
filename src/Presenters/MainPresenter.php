<?php

namespace Flubber\Extension\Presenters;

use Flubber\Component\Router\ApiRoute;

/**
 * Class MainPresenter
 *
 * @package Flubber\Extension
 * @ApiRoute("/")
 */
class MainPresenter extends BasePresenter
{
    public function actionRead() {
        $this->sendJson([
            "support" => [
                "/api/kind" => "CRUD - Kind"
            ]
        ]);
    }
}