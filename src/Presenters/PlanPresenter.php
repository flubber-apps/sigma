<?php

namespace Flubber\Extension\Presenters;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Router\ApiRoute;
use Flubber\Extension\Entity\ControlType;
use Flubber\Extension\Entity\Decree;
use Flubber\Extension\Entity\PlanItem;
use Flubber\Extension\Entity\ProtocolTemplate;
use Flubber\Extension\Facade\History;
use Flubber\Extension\Facade\Plan;
use Nette\Http\Response;
use Nette\InvalidStateException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * @package Flubber\Extension
 * @ApiRoute("/plan")
 */
class PlanPresenter extends BasePresenter
{
    /** @var Plan @inject */
    public $facade;
    /** @var History @inject */
    public $facadeHistory;
    /** @var EntityManagerInterface @inject */
    public $em;

    public function actionCreate()
    {
        try {
            $request = Json::decode($this->getHttpRequest()->getRawBody());
            if (!property_exists($request, "typeId") || !property_exists($request, "items"))
                throw new InvalidStateException("Not valid input data");

            $entity = $this->facade->insert([
                "typeId" => $request->typeId,
                "positionId" => $request->positionId,
                "items" => (array)$request->items
            ], true);
            $this->sendJson([
                "status" => "success",
                "id" => $entity->getId()
            ]);
        } catch (JsonException $e) {
            $this->sendError("Not valid JSON input", Response::S400_BAD_REQUEST);
        } catch (InvalidStateException $e) {
            $this->sendError("Input data error...", Response::S400_BAD_REQUEST, [
                "typeId" => "integer",
                "positionId" => "integer (optional)",
                "items" => [
                    [
                        "name" => "string",
                        "order" => "integer",
                        "watched" => "boolean",
                        "controlTypeId" => "integer",
                        "protocolId" => "integer (optional)"
                    ]
                ]
            ]);
        }
    }

    /**
     * @param int $typeId
     * @param int|null $positionId
     * @throws \Nette\Application\AbortException
     */
    public function actionRead($typeId, $positionId)
    {
        $parameters = (object)[
            "type" => is_numeric($typeId) ? (int)$typeId : null,
            "position" => is_numeric($positionId) ? (int)$positionId : null,
        ];

        $entity = null;
        if ($parameters->position === null)
            $entity = $this->facade->getByType($parameters->type);
        else
            $entity = $this->facade->getByTypeAndPosition($parameters->type, $parameters->position);

        if ($entity) {
            $this->sendJson($entity->toArray());
        } else {
            $this->sendError("Plan with typeID: {$parameters->type} and positionID: {$parameters->position} not found.", Response::S404_NOT_FOUND);
        }

    }

    public function actionUpdate($id)
    {
        try {
            if (!$id)
                $this->sendError("Missing ID of plan", Response::S400_BAD_REQUEST);
            $data = JSON::decode($this->getHttpRequest()->getRawBody());
            /** @var \Flubber\Extension\Entity\Plan $entity */
            $entity = $this->facade->getById($id, false);
            if (!$entity)
                $this->sendError("Plan with ID: {$id} was not found.", Response::S404_NOT_FOUND);
            if (property_exists($data, "generatePlan"))
                $entity->generatePlan = $data->generatePlan;

            foreach ($data->items as $item) {
                if (property_exists($item, "id")) {
                    $itemEntity = $this->em->getRepository(PlanItem::class)->find($item->id);
                } else {
                    $itemEntity = new PlanItem();
                }
                $itemEntity->name = $item->name;
                $itemEntity->decree = $item->decreeId !== null ? $this->em->getPartialReference(Decree::class, $item->decreeId) : null;
                $itemEntity->order = $item->order;
                $itemEntity->watched = $item->watched;
                $itemEntity->controlType = $this->em->getPartialReference(ControlType::class, $item->controlTypeId);
                if (property_exists($item, "protocolId") && $item->protocolId !== null)
                    $itemEntity->protocol = $this->em->getPartialReference(ProtocolTemplate::class, $item->protocolId);
                $itemEntity->plan = $entity;
                $this->em->persist($itemEntity);
            }
            $this->em->persist($entity);
            $this->em->flush();
            $this->sendJson($this->facade->getById($id, false)->toArray());
        } catch (JsonException $e) {
            $this->sendError("Not valid input.", Response::S400_BAD_REQUEST, [
                "items" => [
                    [
                        "id" => "integer (optional)",
                        "name" => "string",
                        "watched" => "boolean",
                        "controlTypeId" => "string",
                        "protocolId" => "integer (optional)",
                    ]
                ]
            ]);
        } catch (InvalidStateException $e) {
        }

    }

    /**
     * @ApiRoute("/plan/assignToPosition", method="OPTIONS")
     */
    public function actionAssignToPositionOptions()
    {
        $this->actionOptions();
    }

    /**
     * @ApiRoute("/plan/assignToPosition", method="POST")
     */
    public function actionAssignToPosition($from, $to)
    {
        try {
            if (!$from || !$to)
                $this->sendError("Missing ID of plan or ID of type", Response::S400_BAD_REQUEST);
            /** @var \Flubber\Extension\Entity\Plan $entity */
            $entity = $this->facade->getByType($from, false);
            if (!$entity)
                $this->sendError("Plan with typeID: {$from} was not found.", Response::S404_NOT_FOUND);
            $data = $entity->toArray();
            $data["positionId"] = (int)$to;
            $data["sourcePlanId"] = $entity->getId();
            $items = [];
            foreach ($data["items"] as $item) {
                $item["sourceItemId"] = $item["id"];
                $items[] = $item;
            }
            $data["items"] = $items;
            $newPlan = $this->facade->insert($data);
            $this->sendJson([
                "status" => "success",
                "id" => $newPlan->getId()
            ]);
        } catch (InvalidStateException $e) {
        }
    }

    /**
     * @ApiRoute("/plan/assignToHistoryFromType", method="OPTIONS")
     */
    public function actionAssignToHistoryFromTypeOptions() {
        $this->actionOptions();
    }

    /**
     * @ApiRoute("/plan/assignToHistoryFromType", method="POST")
     */
    public function actionAssignToHistoryFromType($from, $to)
    {
        try {
            if (!$from || !$to)
                $this->sendError("Missing ID of plan or ID of type", Response::S400_BAD_REQUEST);
            /** @var \Flubber\Extension\Entity\Plan $entity */
            $entity = $this->facade->getByType($from, false);
            if (!$entity)
                $this->sendError("Plan with typeID: {$from} was not found.", Response::S404_NOT_FOUND);
            $history = $this->facadeHistory->getById($to, false);
            if (!$history)
                $this->sendError("History with ID: {$to} was not found.", Response::S404_NOT_FOUND);
            $data = $entity->toArray();
            $data["positionId"] = $history->position->getId();
            $data["sourcePlanId"] = (int)$entity->getId();
            $items = [];
            foreach ($data["items"] as $item) {
                $item["sourceItemId"] = $item["id"];
                $items[] = $item;
            }
            $data["items"] = $items;
            $newPlan = $this->facade->insert($data);
            $history->plan = $newPlan;
            $this->facade->update($history);
            $this->sendJson([
                "status" => "success",
                "id" => $newPlan->getId()
            ]);
        } catch (InvalidStateException $e) {
            $this->sendError($e->getMessage(), Response::S400_BAD_REQUEST);
        }
    }

    /**
     * @ApiRoute("/plan/assignToHistoryFromPosition", method="OPTIONS")
     */
    public function actionAssignToHistoryFromPositionOptions() {
        $this->actionOptions();
    }

    /**
     * @ApiRoute("/plan/assignToHistoryFromPosition", method="POST")
     */
    public function actionAssignToHistoryFromPosition($from, $to)
    {
        try {
            if (!$from || !$to)
                $this->sendError("Missing ID of plan or ID of type", Response::S400_BAD_REQUEST);
            /** @var \Flubber\Extension\Entity\Plan $entity */
            $entity = $this->facade->getByPosition($from, false);
            if (!$entity)
                $this->sendError("Plan with ID: {$from} was not found.", Response::S404_NOT_FOUND);
            $data = $entity->toArray();
            $data["positionId"] = (int)$from;
            $data["sourcePlanId"] = $entity->getId();
            $items = [];
            foreach ($data["items"] as $item) {
                $item["sourceItemId"] = $item["id"];
                $items[] = $item;
            }
            $data["items"] = $items;
            $newPlan = $this->facade->insert($data);
            $history = $this->facadeHistory->getById($to, false);
            if (!$history)
                $this->sendError("History with ID: {$to} was not found.", Response::S404_NOT_FOUND);
            $history->plan = $newPlan;
            $this->facade->update($history);
            $this->sendJson([
                "status" => "success",
                "id" => $newPlan->getId()
            ]);
        } catch (InvalidStateException $e) {
        }
    }
}