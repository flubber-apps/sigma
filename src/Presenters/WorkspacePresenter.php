<?php

namespace Flubber\Extension\Presenters;

use Flubber\Component\Router\ApiRoute;
use Flubber\Component\Security\Entity\User;
use Flubber\Extension\Facade\Workspace;
use Nette\Http\Response;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * Class ProducerPresenter
 * @package Flubber\Extension
 * @ApiRoute("/workspace")
 */
class WorkspacePresenter extends BasePresenter
{
    /** @var \Doctrine\ORM\EntityManagerInterface @inject */
    public $em;
    /** @var Workspace @inject */
    public $facade;

    public function actionCreate()
    {
        try {
            $request = Json::decode($this->getHttpRequest()->getRawBody());
            if (!property_exists($request, "name"))
                throw new InvalidStateException("Not valid input data");
            $entity = $this->facade->insert(["name" => $request->name], true);
            $this->sendJson([
                "status" => "success",
                "id" => $entity->getId()
            ]);
        } catch (JsonException $e) {
            $this->sendError("Not valid JSON input", Response::S400_BAD_REQUEST);
        } catch (InvalidStateException $e) {
            $this->sendError("Workspace with same name already exists.", Response::S400_BAD_REQUEST, [
                "name" => "NAME XY"
            ]);
        }
    }

    public function actionRead($id)
    {
        if ($id) {
            $entity = $this->facade->getById($id, false);
            if ($entity) {
                $this->sendJson($entity);
            } else {
                $this->sendError("Workspace with ID: {$id} not found.", Response::S404_NOT_FOUND);
            }
        } else {
            $data = array_map(function($entity) {
                return $entity->toArray();
            }, $this->facade->getAll());
            $this->sendJson($data);
        }
    }

    public function actionDelete($id)
    {
        if (!$id)
            $this->sendError("Missing ID of workspace", Response::S400_BAD_REQUEST);

        $response = [
            "status" => "success"
        ];
        try {
            $this->facade->deleteById($id, true);
        } catch (InvalidStateException $e) {
            $this->getHttpResponse()->setCode(Response::S404_NOT_FOUND);
            $response = [
                "status" => "error",
                "message" => "Workspace with ID: {$id} not found."
            ];
        } finally {
            $this->sendJson($response);
        }
    }

    /**
     * @ApiRoute("/workspace/user", method="OPTIONS")
     */
    public function actionUserOptions() {
        $this->actionOptions();
    }

    /**
     * @param integer $id
     * @param integer $userId Id of user
     * @ApiRoute("/workspace/user", method="POST")
     * @throws \Nette\Application\AbortException
     */
    public function actionAssign($id, $userId) {
        try {
            /** @var \Flubber\Extension\Entity\Workspace $workspace */
            $workspace = $this->facade->getById($id, true);
            $workspace->assignUser($this->em->getRepository(User::class)->find($userId));
            $this->em->persist($workspace);
            $this->em->flush();
            $this->sendJson([
                "status" => "success"
            ]);
        } catch (InvalidArgumentException $e) {
            $this->sendError("Bad workspace or user.", Response::S400_BAD_REQUEST);
        }
    }

    /**
     * @param integer $id
     * @param integer $userId Id of user
     * @ApiRoute("/workspace/user", method="DELETE")
     * @throws \Nette\Application\AbortException
     */
    public function actionRemoveAssigned($id, $userId) {
        try {
            /** @var \Flubber\Extension\Entity\Workspace $workspace */
            $workspace = $this->facade->getById($id);
            $workspace->removeUser($this->em->getRepository(User::class)->find($userId));
            $this->em->persist($workspace);
            $this->em->flush();
            $this->sendJson([
                "status" => "success"
            ]);
        } catch (InvalidArgumentException $e) {
            $this->sendError("Bad workspace or user.", Response::S400_BAD_REQUEST);
        }
    }
}