<?php

namespace Flubber\Extension\Presenters;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\DiskBrowser\Facade\Directory;
use Flubber\Component\DiskBrowser\Facade\File;
use Flubber\Component\DiskBrowser\Presenters\FilePresenter;
use Flubber\Component\Router\ApiRoute;
use Flubber\Component\Security\Entity\User;
use Flubber\Extension\Entity\Position;
use Flubber\Extension\Facade\History;
use Flubber\Extension\ProtocolUtils\ProtocolGenerator;
use Flubber\Extension\ProtocolUtils\ProtocolReader;
use Nette\Application\AbortException;
use Nette\Application\Responses\FileResponse;
use Nette\Http\Response;
use Nette\InvalidStateException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Style\Cell;
use Tracy\Debugger;

/**
 * @package Flubber\Extension
 * @ApiRoute("/history")
 */
class HistoryPresenter extends BasePresenter
{
    /** @var History @inject */
    public $facade;
    /** @var EntityManagerInterface @inject */
    public $em;
    /** @var Directory @inject */
    public $directoryFacade;
    /** @var File @inject */
    public $fileFacade;

    public function startup()
    {
        if (!$this->getHttpResponse()->isSent()) {
            header("Access-Control-Expose-Headers: Content-Disposition");
        }

        parent::startup();
    }

    public function actionCreate()
    {
        try {
            $request = Json::decode($this->getHttpRequest()->getRawBody());
            if (!property_exists($request, "positionId")
                || !property_exists($request, "subject")
                || !property_exists($request, "numberOffer")
                || !property_exists($request, "startAt"))
                throw new InvalidStateException("Not valid input data");
            $entity = $this->facade->insert([
                "content" => property_exists($request, "content") ? $request->content : null,
                "subject" => $request->subject,
                "number" => property_exists($request, "number") ? $request->number : null,
                "numberOffer" => $request->numberOffer,
                "startAt" => $request->startAt,
                "endAt" => property_exists($request, "endAt") ? $request->endAt : null,
                "positionId" => $request->positionId,
                "userId" => property_exists($request, "userId") ? $request->userId : $this->getUser() !== null ? $this->getUser()->getId() : null
            ], true);
            $this->sendJson([
                "status" => "success",
                "id" => $entity->getId()
            ]);
        } catch (JsonException $e) {
            $this->sendError("Not valid JSON input", Response::S400_BAD_REQUEST);
        } catch (InvalidStateException $e) {
            $this->sendError("Duplicity of history entry or bad input.", Response::S400_BAD_REQUEST, [
                "content" => "string (optional)",
                "subject" => "string",
                "number" => "string (optional)",
                "numberOffer" => "string",
                "startAt" => "datetime (d-m-Y)",
                "endAt" => "datetime (optional)",
                "positionId" => "numeric"
            ]);
        }
    }

    public function actionRead($id)
    {
        if ($id) {
            $entity = $this->facade->getById($id, false);
            if ($entity) {
                $this->sendJson($entity);
            } else {
                $this->sendError("History with ID: {$id} not found.", Response::S404_NOT_FOUND);
            }
        } else {
            $data = [];
            foreach ($this->facade->getAll() as $entity) {
                $data[] = $entity->toArray();
            }
            $this->sendJson($data);
        }
    }

    public function actionUpdate($id)
    {
        try {
            $entity = $this->facade->getById($id, true);

            $request = Json::decode($this->getHttpRequest()->getRawBody());

            if (property_exists($request, "content"))
                $entity->content = $request->content;
            if (property_exists($request, "state")) {
                $stateOld = $entity->getState();
                $stateNew = $request->state;
                if ($stateNew !== $stateOld && $stateOld !== "finished" && $stateOld !== "canceled") {
                    if ($stateOld === "opened" || ($stateOld === "approved" && $stateNew !== "opened"))
                        $entity->setState($stateNew);
                }
            }
            $this->facade->update($entity, true, true);
            $this->sendJson($entity->toArray());
        } catch (InvalidStateException $e) {
            $this->sendError("History not found", Response::S404_NOT_FOUND);
        }
    }

    public function actionDelete($id)
    {
        if (!$id)
            $this->sendError("Missing ID of position", Response::S400_BAD_REQUEST);

        $response = [
            "status" => "success"
        ];
        try {
            $this->facade->deleteById($id, true);
        } catch (InvalidStateException $e) {
            $this->getHttpResponse()->setCode(Response::S404_NOT_FOUND);
            $response = [
                "status" => "error",
                "message" => "Position with ID: {$id} not found."
            ];
        } finally {
            $this->sendJson($response);
        }
    }

    /**
     * @ApiRoute("/history/plan", method="OPTIONS")
     */
    public function actionPlanOptions()
    {
        $this->actionOptions();
    }

    /**
     * @ApiRoute("/history/plan", method="GET")
     */
    public function actionReadPlan($id)
    {
        try {
            $entity = null;
            $entity = $this->facade->getById((int)$id, true);
            $this->sendJson($entity->plan->toArray());
        } catch (InvalidStateException $e) {
            $this->sendJson("History with ID: {$id} not found.", Response::S404_NOT_FOUND);
        }
    }

    /**
     * @ApiRoute("/history/plan/finish", method="OPTIONS")
     */
    public function actionControlOptions()
    {
        $this->actionOptions();
    }

    /**
     * @param integer $id ID of history
     * @param integer $itemId ID of
     * @param integer $userId ID of user
     * @throws AbortException
     * @ApiRoute("/history/plan/finish", method="PUT")
     */
    public function actionFinishControl($id, $itemId, $userId)
    {
        try {
            $entity = null;
            $entity = $this->facade->getById((int)$id, true);
            foreach ($entity->plan->items as $item) {
                if ($item->getId() === (int)$itemId) {
                    $item->finishedAt = new \DateTime();
                    $item->approvedBy = $this->em->getPartialReference(User::class, $userId);
                    $this->facade->update($item);
                    $this->sendJson(["status" => "success", "finishedAt" => $item->finishedAt->format("d-m-Y")]);
                }
            }
            throw new InvalidStateException();
        } catch (InvalidStateException $e) {
            $this->sendJson("History with ID: {$id} or planItem with ID {$itemId} not found.", Response::S404_NOT_FOUND);
        }
    }


    /**
     * @ApiRoute("/history/plan/generate", method="OPTIONS")
     */
    public function actionGenerateOptions()
    {
        $this->actionOptions();
    }

    /**
     * @param integer $id ID of history
     * @param integer $itemId ID of item
     * @param string $type One of ("DOC", "DOCX", "ODT")
     * @throws AbortException
     * @throws \Nette\Application\BadRequestException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @ApiRoute("/history/plan/generate", method="GET")
     */
    public function actionGenerate($id, $itemId, $type)
    {
        try {
            $entity = $this->facade->getById((int)$id, true);
            foreach ($entity->plan->items as $item) {
                if ($item->getId() === (int)$itemId) {
                    $phpWord = new PhpWord();

                    $temp = $this->context->parameters["tempDir"];
                    $values = $item->protocol->getDefaultValues();

                    $values["control.name"] = $item->name;
                    $values["control.decree"] = $item->decree;
                    $values["history.number"] = $entity->number;

                    /** @var Position $position */
                    $position = $entity->position;
                    $name = $temp . "/SE-0{$position->workspace->getId()}_{$itemId}";
                    $values["device.name"] = $position->name;
                    $values["device.uid"] = $position->uid;
                    $values["device.serialNumber"] = $position->serialNumber;

                    $objType = $position->sizeDirectory->type;
                    $values["device.type"] = $objType->name;
                    $values["input.date"] = (new \Datetime)->format("Y-m-d");
                    $generator = new ProtocolGenerator($phpWord, $item->protocol->getStructure(), $values);
                    $generator->setEntityManager($this->em);
                    $generator->run();

                    $objWriter = IOFactory::createWriter($phpWord);
                    $objWriter->save($name . ".docx");
                    if ($type === "ODT") {
                        exec("export HOME=/home && soffice --headless --convert-to odt --outdir {$temp} {$name}.docx");
                        $this->sendResponse(new FileResponse($name . ".odt"));
                    } else if ($type === "DOC") {
                        exec("export HOME=/home && soffice --headless --convert-to doc --outdir {$temp} {$name}.docx");
                        $this->sendResponse(new FileResponse($name . ".doc"));
                    } else {
                        $this->sendResponse(new FileResponse($name . ".docx"));
                    }
                }
            }
            throw new InvalidStateException();
        } catch (InvalidStateException $e) {
            $this->sendError("Inter error", Response::S500_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @ApiRoute("/history/plan/plan-generate", method="HEAD")
     */
    public function actionGeneratePlanHead()
    {
        $this->actionOptions();
    }

    /**
     * @ApiRoute("/history/plan/plan-generate", method="OPTIONS")
     */
    public function actionGeneratePlanOptions()
    {
        $this->actionOptions();
    }

    /**
     * @param $id
     * @param $type
     * @ApiRoute("/history/plan/plan-generate", method="GET")
     */
    public function actionGeneratePlan($id, $type)
    {
        try {
            $entity = $this->facade->getById((int)$id, true);
            $phpWord = new PhpWord();
            $temp = $this->context->parameters["tempDir"];
            $structure = [
                "orientation" => "landscape",
                "header" => [
                    "table" => [
                        "data" => [
                            [
                                "data" => [
                                    [
                                        "width" => 2225,
                                        "content" => [
                                            "text" => "SIGMA GROUP a.s.\ndivize Energo\nJana Sigmunda 313\n783 49 Lutín\n Česká Republika"
                                        ],
                                        "style" => [
                                            'vMerge' => 'restart', 'valign' => 'center'
                                        ]
                                    ],
                                    [
                                        "width" => 3500,
                                        "content" => [
                                            "blockText" => [
                                                "title" => "PLÁN KONTROL A ZKOUŠEK",
                                                "text" => "Evid. č.: %evid.no"
                                            ]
                                        ],
                                        "style" => [
                                            'vMerge' => 'restart', 'valign' => 'center'
                                        ]
                                    ],
                                    [
                                        "width" => 3525,
                                        "content" => [
                                            "text" => "Zákázka (PP) č.: \n%history.number%\n"
                                        ],
                                        "style" => [
                                            'vMerge' => 'restart', 'valign' => 'center'
                                        ]
                                    ]
                                ],
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 2225,
                                        "style" => [
                                            "vMerge" => "continue"
                                        ]
                                    ],
                                    [
                                        "width" => 3500,
                                        "style" => [
                                            "vMerge" => "continue"
                                        ]
                                    ],
                                    [
                                        "width" => 1600,
                                        "content" => [
                                            "text" => "Příloha č.:"
                                        ],
                                        "style" => [
                                            'vMerge' => 'restart', 'valign' => 'center'
                                        ]
                                    ],
                                    [
                                        "width" => 1925,
                                        "content" => [
                                            "text" => "Strana:\t%default.page%/%default.pageCount%"
                                        ],
                                        "style" => [
                                            'vMerge' => 'restart', 'valign' => 'center'
                                        ]
                                    ]
                                ],
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 1800,
                                        "content" => [
                                            "text" => [
                                                "data" => "Typ zařízení",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 1250,
                                        "content" => [
                                            "text" => [
                                                "data" => "Výrobní číslo",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 1450,
                                        "content" => [
                                            "text" => [
                                                "data" => "Projektové číslo",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 3050,
                                        "content" => [
                                            "text" => [
                                                "data" => "Číslo TLP (MP)",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 1700,
                                        "content" => [
                                            "text" => [
                                                "data" => "Vyhláška:",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 1800,
                                        "content" => [
                                            "text" => ""
                                        ]
                                    ],
                                    [
                                        "width" => 1250,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "%device.serialNumber%",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 1450,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "%device.uid%",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 3050,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => ""
                                        ]
                                    ],
                                    [
                                        "width" => 1700,
                                        "content" => [
                                            "text" => [
                                                "data" => "358/261 Sb.",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 1800,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 1250,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 1450,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 3050,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 850,
                                        "content" => [
                                            "text" => [
                                                "data" => "ANO",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 850,
                                        "content" => [
                                            "text" => [
                                                "data" => "NE",
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "height" => 900,
                                "data" => [
                                    [
                                        "width" => 530,
                                        "style" => ["valign" => "center", "textDirection" => Cell::TEXT_DIR_BTLR],
                                        "content" => [
                                            "text" => [
                                                "data" => "Pořadové č./ Operace v (TLP, MP)",
                                                "style" => ["bold" => true, "size" => 8],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 1800,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Druh, název a způsob\nkontroly a zkoušky",
                                                "style" => ["bold" => true, "size" => 9]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 680,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Rozsah kontroly",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 700,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Norma předpis, kritérium přijat.",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 750,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Realizátor kontroly",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 470,
                                        "style" => ["valign" => "center", "textDirection" => Cell::TEXT_DIR_BTLR],
                                        "content" => [
                                            "text" => [
                                                "data" => "Forma dok.",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 2190,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Forma a následné potvrzení účasti odběratelů / nez. dohledu",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 2100,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Vyhodnocení realizátorem",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 530,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 1800,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 680,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 700,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 750,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 470,
                                        "style" => ["vMerge" => "continue"]
                                    ],
                                    [
                                        "width" => 730,
                                        "content" => [
                                            "text" => ""
                                        ]
                                    ],
                                    [
                                        "width" => 730,
                                        "content" => [
                                            "text" => ""
                                        ]
                                    ],
                                    [
                                        "width" => 730,
                                        "content" => [
                                            "text" => ""
                                        ]
                                    ],
                                    [
                                        "width" => 1100,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Výsledek + příp. č. protokolu",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ],
                                    [
                                        "width" => 1000,
                                        "style" => ["valign" => "center"],
                                        "content" => [
                                            "text" => [
                                                "data" => "Datum, jméno a podpis ",
                                                "style" => ["bold" => true, "size" => 9],
                                                "alignment" => ["alignment" => "center"]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                "body" => [
                    [
                        "table" => [
                            "data" => [
                                [
                                    "data" => [
                                        "content" => [
                                            "text" => ""
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "table" => [
                            "style" => "tableBorderAround",
                            "data" => []
                        ]
                    ],
                    [
                        "table" => [
                            "style" => "tableBorderAround",
                            "data" => [
                                [
                                    "height" => 300,
                                    "data" => [
                                        [
                                            "width" => 600,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2110,
                                            "style" => ["valign" => "center", "bgColor" => "CCCCCC"],
                                            "content" => [
                                                "text" => [
                                                    "data" => "PKZ Zpracoval",
                                                    "style" => ["bold" => true],
                                                    "alignment" => ["alignment" => "center"]
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "style" => ["valign" => "center", "bgColor" => "CCCCCC"],
                                            "content" => [
                                                "text" => [
                                                    "data" => "Schválil",
                                                    "style" => ["bold" => true],
                                                    "alignment" => ["alignment" => "center"]
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "style" => ["valign" => "center", "bgColor" => "CCCCCC"],
                                            "content" => [
                                                "text" => [
                                                    "data" => "Schválil",
                                                    "style" => ["bold" => true],
                                                    "alignment" => ["alignment" => "center"]
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "style" => ["valign" => "center", "bgColor" => "CCCCCC"],
                                            "content" => [
                                                "text" => [
                                                    "data" => "Schválil",
                                                    "style" => ["bold" => true],
                                                    "alignment" => ["alignment" => "center"]
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "height" => 400,
                                    "data" => [
                                        [
                                            "width" => 600,
                                            "style" => ["valign" => "center"],
                                            "content" => [
                                                "text" => [
                                                    "data" => "Jméno",
                                                    "style" => ["bold" => true],
                                                    "alignment" => ["alignment" => "center"]
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2110,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "height" => 400,
                                    "data" => [
                                        [
                                            "width" => 600,
                                            "style" => ["valign" => "center"],
                                            "content" => [
                                                "text" => [
                                                    "data" => "Datum",
                                                    "style" => ["bold" => true],
                                                    "alignment" => ["alignment" => "center"]
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2110,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ]
                                    ]
                                ],
                                [
                                    "height" => 400,
                                    "data" => [
                                        [
                                            "width" => 600,
                                            "style" => ["valign" => "center"],
                                            "content" => [
                                                "text" => [
                                                    "data" => "Podpis",
                                                    "style" => ["bold" => true],
                                                    "alignment" => ["alignment" => "center"]
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2110,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ],
                                        [
                                            "width" => 2180,
                                            "content" => [
                                                "text" => [
                                                    "data" => ""
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ],
                    [
                        "text" => [
                            "style" => ["size" => 8],
                            "data" => "Vysvětlivky: Forma dok.: O=Zápis,Z=Záznam do form.“Záznam o kontrole“,P=vystavení formuláře „Protokol o kontrole“,POS=Prohlášení o shodě, OK=Odběratelská kontrola, W-svědečný /ověřovací bod(Witness point) osoba ,která si tento bod v PKZ vyznačila musí být o termínu kontroly informována.Pokud se v uvedeném termínu kontroly nezúčastní ,pak pověřený pracovník zhotovitele provede kontrolu a při splnění stanovených kritérií vyhotoví předepsaný výstup z kontrolní operace-zhotovitel může pokračovat v další činnosti,zpětně již tato kontrola není potvrzována.H-zádržný bod (Hold Point) osoba,která si tento bod v PKZ vyznačila,musí být o termínu kontroly informována a bez její účasti nelze kontrolu provést a pokračovat v další činnostech,její účast je dokladována."
                        ]
                    ]
                ],
                "styles" => [
                    "tableDefault" => [
                        "borderSize" => 6,
                        "borderColor" => "000000"
                    ],
                    "tableFirstRowDefault" => null,
                    "tableBorder" => [
                        "borderSize" => 12,
                        "borderColor" => "000000"
                    ],
                    "tableBorderAround" => [
                        "borderSize" => 6,
                        "borderTopSize" => 12,
                        "borderBottomSize" => 12,
                        "borderLeftSize" => 12,
                        "borderRightSize" => 12,
                        "borderColor" => "000000"
                    ],
                    "tableFirstRowBorder" => null
                ]
            ];
            $i = 0;
            foreach ($entity->plan->items as $item) {
                $i++;
                $structure["body"][0]["table"]["data"][] = [
                    "height" => 380,
                    "data" => [
                        [
                            "width" => 530,
                            "style" => ["valign" => "center"],
                            "content" => [
                                "text" => [
                                    "data" => $i,
                                    "alignment" => ["alignment" => "center"]
                                ]
                            ]
                        ],
                        [
                            "width" => 1800,
                            "style" => ["valign" => "center"],
                            "content" => [
                                "text" => [
                                    "data" => $item->name,
                                    "alignment" => ["alignment" => "center"]
                                ]
                            ]
                        ],
                        [
                            "width" => 680,
                            "style" => ["valign" => "center"],
                            "content" => [
                                "text" => [
                                    "data" => $item->controlType->getId(),
                                    "alignment" => ["alignment" => "center"]
                                ]
                            ]
                        ],
                        [
                            "width" => 700,
                            "content" => [
                                "text" => ""
                            ]
                        ],
                        [
                            "width" => 750,
                            "style" => ["valign" => "center"],
                            "content" => [
                                "text" => [
                                    "data" => $item->approvedBy->surname . " " . $item->approvedBy->name,
                                    "alignment" => ["alignment" => "center"]
                                ]
                            ]
                        ],
                        [
                            "width" => 470,
                            "content" => [
                                "text" => ""
                            ]
                        ],
                        [
                            "width" => 730,
                            "content" => [
                                "text" => ""
                            ]
                        ],
                        [
                            "width" => 730,
                            "content" => [
                                "text" => ""
                            ]
                        ],
                        [
                            "width" => 730,
                            "content" => [
                                "text" => ""
                            ]
                        ],
                        [
                            "width" => 1100,
                            "style" => ["valign" => "center"],
                            "content" => [
                                "text" => [
                                    "data" => $item->uName,
                                    "alignment" => ["alignment" => "center"]
                                ]
                            ]
                        ],
                        [
                            "width" => 1000,
                            "style" => ["valign" => "center"],
                            "content" => [
                                "text" => [
                                    "data" => $item->finishedAt->format("d-m-Y"),
                                    "alignment" => ["alignment" => "center"]
                                ]
                            ]
                        ]
                    ]
                ];
            }
            // Insert empty row
            $structure["body"][1]["table"]["data"][] = [
                "height" => 350 * (9 - $i),
                "data" => [
                    [
                        "content" => [
                            "text" => ""
                        ]
                    ]
                ]
            ];
            $values = [];

            /** @var Position $position */
            $position = $entity->position;
            $name = $temp . "/SE-0{$position->workspace->getId()}-plan-kontrol";

            $generator = new ProtocolGenerator($phpWord, $structure, $values);
            $generator->setEntityManager($this->em);
            $generator->run();

            $objWriter = IOFactory::createWriter($phpWord);
            $objWriter->save($name . ".docx");
            $objReader = IOFactory::createReader();
            /** @var PhpWord $a */
            $a = $objReader->load($name . ".docx");
            if ($type === "ODT") {
                exec("export HOME=/home && soffice --headless --convert-to odt --outdir {$temp} {$name}.docx");
                $this->sendResponse(new FileResponse($name . ".odt"));
            } else if ($type === "DOC") {
                exec("export HOME=/home && soffice --headless --convert-to doc --outdir {$temp} {$name}.docx");
                $this->sendResponse(new FileResponse($name . ".doc"));
            } else {
                $this->sendResponse(new FileResponse($name . ".docx"));
            }
            throw new InvalidStateException();
        } catch (InvalidStateException $e) {
            $this->sendError("Inter error", Response::S500_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * @param $id
     * @param $itemId
     * @param $type
     * @throws AbortException
     * @ApiRoute("/history/plan/generate", method="HEAD")
     */
    public function actionGenerateHead($id, $itemId, $type)
    {
        $this->actionGenerate($id, $itemId, $type);
    }

    /**
     * @ApiRoute("/history/plan/upload", method="OPTIONS")
     */
    public function actionUploadOptions()
    {
        $this->actionOptions();
    }

    /**
     * @param integer $id ID of history
     * @param integer $itemId ID of item
     * @param integer $userId ID of user
     * @throws AbortException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @ApiRoute("/history/plan/upload", method="POST")
     */
    public function actionUpload($id, $itemId, $userId)
    {
        try {
            $request = $this->getHttpRequest();
            $www = $this->context->parameters["wwwDir"];
            $files = FilePresenter::createFile((object)[
                "uid" => $request->getPost("uid"),
                "name" => $request->getPost("name"),
                "mimeType" => $request->getPost("mime-type"),
                "part" => (int)$request->getPost("part"),
                "partCount" => (int)$request->getPost("countParts"),
                "path" => $www . "/upload/",
                "directoryId" => (int)$request->getPost("directoryId")
            ], $request->getFiles(), $this->fileFacade, $this->directoryFacade, $this->getHttpResponse());

            $entity = $this->facade->getById((int)$id, true);
            $temp = $this->context->parameters["tempDir"];
            if ($itemId === null) {
                foreach ($files["files"] as $fileName => $uid) {
                    $file = $this->fileFacade->getByUId($uid);
                    $entity->plan->generatedPlan = $file;
                    $this->facade->update($entity);
                    $this->sendJson(["status" => "success", "files" => [$fileName => $uid]]);
                }
            } else foreach ($entity->plan->items as $item) {
                if ($item->getId() === (int)$itemId) {
                    foreach ($files["files"] as $fileName => $uid) {
                        $position = $entity->position;
                        $item->uName = "SE-0{$position->workspace->getId()}_{$itemId}";
                        $file = $this->fileFacade->getByUId($uid);
                        $item->protocolFile = $file;
                        // TODO For testing
                        //$this->em->remove($file);
                        //$this->em->flush();
                        if ($item->watched) {
                            $type = $file->getExtension();
                            $parts = explode('.', $file->getStoredName());
                            $result = ['', ''];
                            if (count($parts) >= 1) {
                                $result[0] = array_shift($parts);
                                $result[1] = implode('.', $parts);
                            }

                            if ($type === "odt" || $type === "doc") {
                                exec("export HOME=/home && soffice --headless --convert-to docx --outdir {$temp} {$www}/upload/{$file->getStoredName()}");
                            } else {
                                exec("cp {$www}/upload/{$file->getStoredName()} {$temp}/{$file->getStoredName()}");
                            }

                            $objReader = IOFactory::createReader();
                            $reader = new ProtocolReader($objReader, $temp . "/" . $result[0] . ".docx", $item->protocol->getStructure(), $type);
                            $reader->setEntityManager($this->em);

                            $reader->run();
                            $item->setValues($reader->getValues());
                        }
                        $item->finishedAt = new \DateTime();
                        $item->approvedBy = $this->em->getPartialReference(User::class, $userId);

                        $this->facade->update($item, true, true);

                        $this->sendJson(["status" => "success", "files" => [$fileName => $uid], "finishedAt" => $item->finishedAt->format("d-m-Y")]);
                    }
                }
            }
            throw new InvalidStateException();
        } catch (InvalidStateException $e) {
            $this->sendError("Inter error: " . $e->getMessage(), Response::S500_INTERNAL_SERVER_ERROR);
        }
    }
}