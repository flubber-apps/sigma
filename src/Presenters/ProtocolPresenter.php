<?php

namespace Flubber\Extension\Presenters;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Router\ApiRoute;
use Flubber\Extension\Entity\ProtocolTemplate;


/**
 * @package Flubber\Extension\Presenters
 * @ApiRoute("/protocol")
 */
class ProtocolPresenter extends BasePresenter
{

    /** @var EntityManagerInterface @inject */
    public $em;

    public function actionRead($uid)
    {
        $result = [];
        if (!$uid) {
            $entities = $this->em->getRepository(ProtocolTemplate::class)->findAll();
            foreach ($entities as $entity)
                $result[] = $entity->toJson();

        }

        $this->sendJson($result);
    }

}