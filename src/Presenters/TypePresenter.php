<?php

namespace Flubber\Extension\Presenters;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Router\ApiRoute;
use Flubber\Extension\Entity\SizeDirectory;
use Flubber\Extension\Facade\Size;
use Flubber\Extension\Facade\Type;
use Nette\Http\Response;
use Nette\InvalidStateException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * Class MainPresenter
 *
 * @package Flubber\Extension
 * @ApiRoute("/type")
 */
class TypePresenter extends BasePresenter
{
    /** @var Type @inject */
    public $facade;
    /** @var Size @inject */
    public $facadeSize;
    /** @var EntityManagerInterface @inject */
    public $em;

    public function actionCreate()
    {
        try {
            $request = Json::decode($this->getHttpRequest()->getRawBody());
            if (!property_exists($request, "name"))
                throw new InvalidStateException("Not valid input data");
            $entity = $this->facade->insert([
                "kindId" => $request->kindId,
                "producerId" => $request->producerId,
                "name" => $request->name
            ], true);
            $this->sendJson([
                "status" => "success",
                "id" => $entity->getId()
            ]);
        } catch (JsonException $e) {
            $this->sendError("Not valid JSON input", Response::S400_BAD_REQUEST);
        } catch (InvalidStateException $e) {
            $this->sendError("Type with same name in this categories already exists.", Response::S400_BAD_REQUEST, [
                "kindId" => "numeric",
                "producerId" => "numeric",
                "name" => "string"
            ]);
        }
    }

    public function actionRead($id)
    {
        $unitOfWork = $this->em->getUnitOfWork();
        if ($id) {
            $entity = $this->facade->getById($id, false);
            if ($entity) {
                $this->sendJson($entity);
            } else {
                $this->sendError("Type with ID: {$id} not found.", Response::S404_NOT_FOUND);
            }
        } else {
            $data = [];
            foreach ($this->facade->getAll() as $entity) {
                $data[] = [
                    "id" => $entity->getId(),
                    "kindId" => $unitOfWork->getEntityIdentifier($entity->kind)["id"],
                    "producerId" => $unitOfWork->getEntityIdentifier($entity->producer)["id"],
                    "directoryId" => $unitOfWork->getEntityIdentifier($entity->root)["id"],
                    "name" => $entity->name
                ];
            }
            $this->sendJson($data);
        }
    }

    public function actionUpdate($id)
    {

    }

    public function actionDelete($id)
    {
        if (!$id)
            $this->sendError("Missing ID of kind", Response::S400_BAD_REQUEST);

        $response = [
            "status" => "success"
        ];
        try {
            $this->facade->deleteById($id, true);
        } catch (InvalidStateException $e) {
            $this->getHttpResponse()->setCode(Response::S404_NOT_FOUND);
            $response = [
                "status" => "error",
                "message" => "Kind with ID: {$id} not found."
            ];
        } finally {
            $this->sendJson($response);
        }
    }


    /**
     * @ApiRoute("/type/assignSize", method="OPTIONS")
     */
    public function actionAssignSizeOptions() {
        $this->actionOptions();
    }

    /**
     * @param integer $id Id of type
     * @ApiRoute("/type/assignSize", method="GET")
     */
    public function actionAssignSizeRead($id)
    {
        if ($id) {
            $type = $this->facade->getById($id, false);
            if (!$type)
                $this->sendError("Type with ID: {$id} not found", Response::S404_NOT_FOUND);
            $data = $type->sizes->toArray();
        } else {
            $data = $this->em->getRepository(SizeDirectory::class)->findAll();
        }
        $unitOfWork = $this->em->getUnitOfWork();
        $this->sendJson(array_map(function ($size) use ($unitOfWork) {
            return [
                "id" => $size->getId(),
                "sizeId" => $unitOfWork->getEntityIdentifier($size->size)["id"],
                "typeId" => $unitOfWork->getEntityIdentifier($size->type)["id"]
            ];
        }, $data));
    }

    /**
     * @param integer $id Id of type
     * @ApiRoute("/type/assignSize", method="POST")
     */
    public function actionAssignSizeCreate($id)
    {
        try {
            if (!$id)
                $this->sendError("Missing ID of type.", Response::S400_BAD_REQUEST);
            $type = $this->facade->getById($id, false);
            if (!$type)
                $this->sendError("Type with ID: {$id} not found", Response::S404_NOT_FOUND);

            $request = Json::decode($this->getHttpRequest()->getRawBody());
            if (!property_exists($request, "sizeId"))
                throw new InvalidStateException("Not valid input data");
            $size = $this->facadeSize->getById($request->sizeId);
            if (!$size)
                $this->sendError("Size with ID: {$request->sizeId} not found", Response::S404_NOT_FOUND);

            $entity = new SizeDirectory;
            $entity->type = $type;
            $entity->size = $size;
            $this->em->persist($entity);
            $this->em->flush();
            $this->sendJson([
                "status" => "success",
                "id" => $entity->getId()
            ]);
        } catch (JsonException $e) {
            $this->sendError("Not valid JSON input", Response::S400_BAD_REQUEST);
        } catch (UniqueConstraintViolationException $e) {
            $this->sendError("Size is already assigned.", Response::S409_CONFLICT);
        } catch (InvalidStateException $e) {
            $this->sendError("Type with same name in this categories already exists.", Response::S400_BAD_REQUEST, [
                "sizeId" => "numeric",
            ]);
        }
    }

    /**
     * @param integer $id Id of type
     * @ApiRoute("/type/assignSize", method="DELETE")
     */
    public function actionAssignSizeDelete($id)
    {
        try {
            if (!$id)
                $this->sendError("Missing ID of assigned size.", Response::S400_BAD_REQUEST);
            $entity = $this->em->getRepository(SizeDirectory::class)->find($id);
            if (!$entity)
                $this->sendError("Size with ID: {$id} not found", Response::S404_NOT_FOUND);
            $this->em->remove($entity);
            $this->em->flush();
            $unitOfWork = $this->em->getUnitOfWork();

            $this->sendJson([
                "status" => "success",
                "entity" => [
                    "id" => $entity->getId(),
                    "typeId" => $unitOfWork->getEntityIdentifier($entity->type)["id"],
                    "sizeId" => $unitOfWork->getEntityIdentifier($entity->size)["id"],
                ]
            ]);
        } catch (InvalidStateException $e) {
            $this->sendError("Type with same name in this categories already exists.", Response::S400_BAD_REQUEST, [
                "sizeId" => "numeric",
            ]);
        }
    }
}