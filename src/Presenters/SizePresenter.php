<?php

namespace Flubber\Extension\Presenters;

use Flubber\Component\Router\ApiRoute;
use Flubber\Extension\Facade\Size;
use Nette\Http\Response;
use Nette\InvalidStateException;
use Nette\Utils\Json;
use Nette\Utils\JsonException;

/**
 * Class ProducerPresenter
 * @package Flubber\Extension
 * @ApiRoute("/size")
 */
class SizePresenter extends BasePresenter
{
    /** @var Size @inject */
    public $facade;

    public function actionCreate()
    {
        try {
            $request = Json::decode($this->getHttpRequest()->getRawBody());
            if (!property_exists($request, "name"))
                throw new InvalidStateException("Not valid input data");
            $entity = $this->facade->insert(["name" => $request->name], true);
            $this->sendJson([
                "status" => "success",
                "id" => $entity->getId()
            ]);
        } catch (JsonException $e) {
            $this->sendError("Not valid JSON input", Response::S400_BAD_REQUEST);
        } catch (InvalidStateException $e) {
            $this->sendError("Size with same name already exists.", Response::S400_BAD_REQUEST, [
                "name" => "NAME XY"
            ]);
        }
    }

    public function actionRead($id)
    {
        if ($id) {
            $entity = $this->facade->getById($id, false);
            if ($entity) {
                $this->sendJson($entity);
            } else {
                $this->sendError("Size with ID: {$id} not found.", Response::S404_NOT_FOUND);
            }
        } else {
            $data = [];
            foreach ($this->facade->getAll() as $entity) {
                $data[] = [
                    "id" => $entity->getId(),
                    "name" => $entity->name
                ];
            }
            $this->sendJson($data);
        }
    }

    public function actionUpdate($id)
    {

    }

    public function actionDelete($id)
    {
        if (!$id)
            $this->sendError("Missing ID of size", Response::S400_BAD_REQUEST);

        $response = [
            "status" => "success"
        ];
        try {
            $this->facade->deleteById($id, true);
        } catch (InvalidStateException $e) {
            $this->getHttpResponse()->setCode(Response::S404_NOT_FOUND);
            $response = [
                "status" => "error",
                "message" => "Size with ID: {$id} not found."
            ];
        } finally {
            $this->sendJson($response);
        }
    }
}