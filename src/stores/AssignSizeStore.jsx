import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";
import Action from "../variables/sigma";
import SizeStore from "./SizeStore";
import TypeStore from "./TypeStore";

class AssignSizeStore extends AbstractStore {
  startedInit = false;
  initialized = false;
  source = [];
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case Action.TYPE_ASSIGN_ADD:
        this.add(
          action.payload.typeId,
          action.payload.sizeId,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      case Action.TYPE_ASSIGN_DELETE:
        if (action.payload.filter !== true)
          this.delete(
            action.payload.id,
            action.payload.onSuccess,
            action.payload.onError
          );
        break;
      case Action.TYPE_DELETE:
        this._filterByType(action.payload.typeId);
        break;
      case Action.SIZE_DELETE:
        this._filterBySize(action.payload.sizeId);
        break;
      default:
        break;
    }
  }

  init() {
    if (!this.initialized) {
      if (this.startedInit) return;
      this.startedInit = true;
      TypeStore.addTypeListener(this.actionInit, this.init.bind(this));
      SizeStore.addTypeListener(this.actionInit, this.init.bind(this));
      let _this = this;
      this.initRequest(
        this.getApiPath("/type/assignSize"),
        "GET",
        true,
        response => {
          _this.source = response;
          _this.initialized = true;
          _this.init();
        }
      );
    } else {
      let initialized = true;
      this.list = this.source.map(item => {
        let type = TypeStore.getById(item.typeId);
        item.type = type;
        item.kind = type.kind || {};
        item.producer = type.producer || {};
        item.size = SizeStore.getById(item.sizeId);
        initialized &= item.kind.id !== undefined && item.producer.id !== undefined && item.size.id !== undefined;
        return item;
      });
      if (initialized) {
        this.emit(this.actionChange);
        this.emit(this.actionInit);
      }
    }
  }

  getAllByType(typeId) {
    if (!this.initialized) this.init();
    return this.list.filter(item => item.type.id === typeId);
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }

  getById(id) {
    if (!this.initialized) {
      this.init();
      return {};
    }
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  getAllUnassignByType(typeId) {
    return SizeStore.getAll().filter(item => {
      return (
        this.list.filter(
          assign => item.id === assign.size.id && typeId === assign.type.id
        ).length === 0
      );
    });
  }

  add(typeId, sizeId, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/type/assignSize?id=" + typeId),
      "POST",
      true,
      response => {
        _this.source.push({
          id: response.id,
          sizeId,
          typeId
        });
        let type = TypeStore.getById(typeId);
        _this.list.push({
          id: response.id,
          type: type,
          size: SizeStore.getById(sizeId),
          kind: type.kind,
          producer: type.producer
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        sizeId: sizeId
      }
    );
  }

  delete(id, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/type/assignSize?id=" + id),
      "DELETE",
      true,
      response => {
        _this.list = _this.list.filter(item => item.id !== id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      }
    );
  }

  _filterByType = id => {
    this.source = this.source.filter(item => {
      if (item.typeId === id) {
        Dispatcher.dispatch({
          type: Action.TYPE_ASSIGN_DELETE,
          payload: {
            id: item.id,
            filter: true
          }
        });
      }
      return item.typeId !== id;
    });
    this.init();
  };
  _filterBySize = id => {
    this.source = this.source.filter(item => {
      if (item.sizeId === id) {
        Dispatcher.dispatch({
          type: Action.TYPE_ASSIGN_DELETE,
          payload: {
            id: item.id,
            filter: true
          }
        });
      }
      return item.sizeId !== id;
    });
  };
}

export default new AssignSizeStore();
