import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";

import Action from "../variables/sigma";
import AssignSizeStore from "./AssignSizeStore";
import PositionStore from "./PositionStore";
import TypeStore from "./TypeStore";

class ProtocolTemplateStore extends AbstractStore {
  startedInit = false;
  initialized = false;
  source = [];
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case Action.HISTORY_ADD:
      case Action.HISTORY_DELETE:
      case Action.HISTORY_FILTER_CHANGED:
        break;
      default:
        break;
    }
  }

  init() {
    if (!this.initialized) {
      if (this.startedInit) return;
      this.startedInit = true;
      TypeStore.addTypeListener(this.actionInit, this.init.bind(this));
      let _this = this;
      this.initRequest(this.getApiPath("/history"), "GET", true, response => {
        _this.source = response;
        _this.initialized = true;
        this.init();
      });
    } else {
      this.list = this.source.map(item => {
        let position = TypeStore.getById(item.typeId);
        return {
          id: item.id,
          content: item.content,
          subject: item.subject,
          number: item.number,
          numberOffer: item.numberOffer,
          startAt: item.startAt,
          endAt: item.endAt,
          kind: position.kind || {},
          producer: position.producer || {},
          type: position.type || {},
          size: position.size || {},
          position: position,
          workspace: position.workspace
        };
      });
      this.emit(this.actionChange);
      this.emit(this.actionInit);
    }
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }

  getById(id) {
    if (!this.initialized) {
      this.init();
      return {};
    }
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }
}

export default new ProtocolTemplateStore();
