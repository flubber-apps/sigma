import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";
import Configuration from "../variables/configuration";

class UrlStore extends AbstractStore {
  constructor() {
    super();

    this.init();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {}

  init() {
    this.list = [];
    /*let _this = this;
        request.get(this.getApiPath("/urls")).end((err, response) => {
          _this.list = response.body;
          _this.emit(ActionType.URL_INIT);
        });*/
    this.list = [
      {
        id: 0,
        link: "/in",
        layout: "Sign",
        view: "SignIn",
        lang: "en"
      },
      {
        id: 2,
        link: "/zakazka",
        layout: "Front",
        view: "Dashboard",
        lang: "en"
      },/*
      {
        id: 3,
        link: "/dashboard",
        layout: "Front",
        view: "Dashboard",
        lang: "cs"
      },*/
      {
        id: 3,
        link: "/zakazka",
        layout: "Front",
        view: "OrderList",
        lang: "cs"
      },
      {
        id: 4,
        link: "/zarizeni",
        layout: "Front",
        view: "DeviceList",
        lang: "cs"
      },
      {
        id: 5,
        link: "/device",
        layout: "Front",
        view: "DeviceList",
        lang: "en"
      },
      {
        id: 6,
        link: "/zakazka",
        layout: "Front",
        view: "OrderList",
        lang: "cs"
      },
      {
        id: 7,
        link: "/order",
        layout: "Front",
        view: "OrderList",
        lang: "en"
      },
      {
        id: 8,
        link: "/device/:id",
        layout: "Front",
        view: "DeviceDetail",
        lang: "en"
      },
      {
        id: 9,
        link: "/zarizeni/:id",
        layout: "Front",
        view: "DeviceDetail",
        lang: "cs"
      },/* TODO
      {
        id: 10,
        link: "/dashboard",
        layout: "Admin",
        view: "Dashboard",
        lang: "en"
      },*/
      {
        id: 10,
        link: "/manage-categories",
        layout: "Admin",
        view: "Dashboard",
        lang: "en"
      },
      {
        id: 11,
        link: "/manage-categories",
        layout: "Admin",
        view: "ManageCategories",
        lang: "en"
      },
      {
        id: 12,
        link: "/history/:id",
        layout: "Front",
        view: "OrderDetail",
        lang: "en"
      },
      {
        id: 13,
        link: "/zakazka/:id",
        layout: "Front",
        view: "OrderDetail",
        lang: "cs"
      }
    ];

    this.emit(this.actionChange);
  }

  getAll() {
    let lang = this._config.lang;
    return this.list.filter(url => url.lang === lang);
  }

  getLayoutPath(layout) {
    let lang = Configuration.lang;
    if (lang === "cs") {
      switch (layout) {
        case "Sign":
          return "/sign";
        case "Admin":
          return "/administrace";
        case "Front":
        default:
          return "/app";
      }
    } else {
      switch (layout) {
        case "Sign":
          return "/sign";
        case "Admin":
          return "/admin";
        case "Front":
        default:
          return "/app";
      }
    }
  }

  getPath(layout, view) {
    let _this = this;
    let urls = this.list.filter(url => {
      return (
        url.layout === layout &&
        url.view === view &&
        (url.lang === _this._config.lang ||
          url.lang === _this._config.defaultLang)
      );
    });
    if (urls.length !== 1) {
      urls = urls.filter(url => url.lang === _this._config.lang);
    }
    return this.getLayoutPath(layout) + urls[0].link;
  }

  generateUrl(layout, view, params) {
    let path = this.getPath(layout, view);
    params.id = Object.values(params).join("_");
    for (let id in params) {
      let reg = ":" + id;
      path = path.replace(reg, params[id]);
    }
    return path;
  }
}

export default new UrlStore();
