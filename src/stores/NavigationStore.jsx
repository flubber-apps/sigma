import Dispatcher from "../Dispatcher";
import SystemAct from "../variables/system";
import AbstractStore from "./AbstractStore";

import config from "../variables/configuration";

let request = require("superagent");

class NavigationStore extends AbstractStore {
  list = [];
  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }
  _register(action) {
    switch (action.type) {
      case SystemAct.NAVIGATION_REORDER:
        //this.signIn(action.payload.email, action.payload.password);
        break;
      default:
        break;
    }
  }
  init() {
    let _this = this;
    request.get(this.getApiPath("/navigations")).end((err, response) => {
      _this.list = response.body;
      _this.emit(this.actionChange);
    });
  }
  getAll() {
    let lang = config.lang;
    return this.list.filter(nav => nav.lang === lang);
  }
}

export default new NavigationStore();
