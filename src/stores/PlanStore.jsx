import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";
import DecreeStore from "./DecreeStore";
import ControlTypeStore from "./ControlTypeStore";
import FileSaver from "file-saver";
import UserStore from "./UserStore";

class PlanStore extends AbstractStore {
  list = [];
  inProcess = [];
  initialized = false;

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {}

  init() {
    if (!this.initialized) {
      this.initialized = true;
      DecreeStore.addListener(
        this.actionInit,
        this.validateData.bind(this, this.list)
      );
      ControlTypeStore.addListener(
        this.actionInit,
        this.validateData.bind(this, this.list)
      );
    }
  }

  validateData = data => {
    this.init();
    return data.map(item => {
      item.items = item.items.map(subItem => {
        subItem.decree = DecreeStore.getById(subItem.decreeId);
        subItem.controlType = ControlTypeStore.getById(subItem.controlTypeId);
        return subItem;
      });
      return item;
    });
  };

  getAll() {
    return this.list;
  }

  getById(id, onSuccess, onError) {
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    if (this.inProcess.filter(item => item.id === id).length) return {};
    this.inProcess.push({ id });
    let _this = this;
    this.initRequest(
      this.getApiPath("/plan?id=" + id),
      "GET",
      true,
      response => {
        _this.inProcess = _this.inProcess.filter(item => item.id === id);
        _this.list.push(response);
        _this.list = _this.validateData(_this.list);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        _this.inProcess = _this.inProcess.filter(item => item.id === id);
        if (onError !== undefined) onError(response);
      },
      false
    );

    return {};
  }

  getByFullId(typeId, positionId, historyId, onSuccess, onError) {
    if (historyId !== undefined)
      return this.getByHistoryId(historyId, onSuccess, onError);
    let result = this.list.filter(
      item =>
        item.typeId === typeId &&
        (item.positionId === positionId ||
          (positionId === undefined && item.positionId === null))
    );
    if (result.length) return result[0];
    if (
      this.inProcess.filter(
        item => item.typeId === typeId && item.positionId === positionId
      ).length
    )
      return {};
    this.inProcess.push({ typeId, positionId });
    let _this = this;
    this.initRequest(
      this.getApiPath("/plan?typeId=" + typeId + "&positionId=" + positionId),
      "GET",
      true,
      response => {
        _this.inProcess = _this.inProcess.filter(
          item =>
            item.typeId !== typeId ||
            item.positionId !== positionId ||
            item.historyId !== historyId
        );
        _this.list.push(response);
        _this.list = _this.validateData(_this.list);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        _this.inProcess = _this.inProcess.filter(
          item => item.typeId !== typeId || item.positionId !== positionId
        );
        if (onError !== undefined) onError(response);
      },
      false
    );

    return {};
  }

  getByHistoryId(historyId, onSuccess, onError) {
    let result = this.list.filter(item => item.historyId === historyId);
    if (result.length) return result[0];
    if (this.inProcess.filter(item => item.historyId === historyId).length) {
      return {};
    }

    this.inProcess.push({ historyId });
    let _this = this;
    this.initRequest(
      this.getApiPath("/history/plan?id=" + historyId),
      "GET",
      true,
      response => {
        response.historyId = historyId;
        _this.inProcess = _this.inProcess.filter(
          item => item.historyId !== historyId
        );
        _this.list.push(response);
        _this.list = _this.validateData(_this.list);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        _this.inProcess = _this.inProcess.filter(
          item => item.historyId !== historyId
        );
        if (onError !== undefined) onError(response);
      },
      false
    );

    return {};
  }

  add(typeId, positionId, historyId, items, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/plan"),
      "POST",
      true,
      response => {
        _this.list.push({
          id: response.id,
          typeId: typeId,
          positionId: positionId ? positionId : null,
          historyId: historyId ? historyId : null,
          items: items,
          generatedPlan: true
        });
        _this.list = _this.validateData(_this.list);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        typeId: typeId,
        positionId: positionId ? positionId : null,
        historyId: historyId ? historyId : null,
        items: items
      }
    );
  }

  update(id, generatePlan, items, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/plan?id=" + id),
      "PUT",
      true,
      response => {
        let item = _this.list.filter(item => item.id === id);
        _this.list = _this.list.filter(item => item.id !== id);
        item.items = items;
        _this.list.push(item);
        _this.list = _this.validateData(_this.list);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      { items, generatePlan }
    );
  }

  toPosition(id, to, onSuccess) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/plan/assignToPosition?from=" + id + "&to=" + to),
      "POST",
      true,
      response => {
        onSuccess();
        _this.emit(_this.actionChange);
      },
      response => {},
      false
    );
  }

  toHistoryFromType(id, to, onSuccess) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/plan/assignToHistoryFromType?from=" + id + "&to=" + to),
      "POST",
      true,
      response => {
        onSuccess();
        _this.emit(_this.actionChange);
      },
      response => {},
      false
    );
  }

  toHistoryFromPosition(id, to, onSuccess) {
    let _this = this;
    this.initRequest(
      this.getApiPath(
        "/plan/assignToHistoryFromPosition?from=" + id + "&to=" + to
      ),
      "POST",
      true,
      response => {
        onSuccess();
        _this.emit(_this.actionChange);
      },
      response => {},
      false
    );
  }

  finish(historyId, itemId) {
    let _this = this;
    this.initRequest(
      this.getApiPath(
        "/history/plan/finish?id=" + historyId + "&itemId=" + itemId + "&userId=" + UserStore.getUser().id
      ),
      "PUT",
      true,
      response => {
        _this.list = _this.list.map(plan => {
          if (plan.historyId === historyId) {
            plan.items = plan.items.map(item => {
              if (item.id === itemId) {
                item.finishedAt = response.finishedAt;
              }
              return item;
            });
          }
          return plan;
        });
        _this.emit(_this.actionChange);
      }
    );
  }

  generate(historyId, itemId, type) {
    let _this = this;
    this.initRequest(
      this.getApiPath(
        "/history/plan/generate?id=" +
          historyId +
          "&itemId=" +
          itemId +
          "&type=" +
          type
      ),
      "GET",
      true,
      (response, xhr) => {
        _this.list = _this.list.map(plan => {
          if (plan.historyId === historyId) {
            plan.items = plan.items.map(item => {
              if (item.id === itemId) {
                item.generated = true;
              }
              return item;
            });
          }
          return plan;
        });
        let data = this.getDownloaded(xhr);
        FileSaver.saveAs(
          this.getApiPath(
            "/history/plan/generate?id=" +
              historyId +
              "&itemId=" +
              itemId +
              "&type=" +
              type
          ),
          data.filename
        );
        _this.emit(_this.actionChange);
      }
    );
  }
  generatePlan(historyId, type) {
    let _this = this;
    this.initRequest(
      this.getApiPath(
        "/history/plan/plan-generate?id=" +
          historyId +
          "&type=" +
          type
      ),
      "GET",
      true,
      (response, xhr) => {
        let data = this.getDownloaded(xhr);
        FileSaver.saveAs(
          this.getApiPath(
            "/history/plan/plan-generate?id=" +
              historyId +
              "&type=" +
              type
          ),
          data.filename
        );
        _this.emit(_this.actionChange);
      }
    );
  }

  updateFile(planId, itemId, uid, finishedAt) {
    let plan = this.getById(planId);
    plan.items = plan.items.map(item => {
      if (item.id === itemId) {
        item.finishedAt = finishedAt;
        item.protocolUid = uid;
      }
      return item;
    });
    this.list = this.list.filter(item => item.id !== planId);
    this.list.push(plan);
    this.emit(this.actionChange);
  }

  updatePlanFile(planId, uid) {
    let plan = this.getById(planId);
    plan.generatedPlanFile = uid;
    this.list = this.list.filter(item => item.id !== planId);
    this.list.push(plan);
    this.emit(this.actionChange);
  }
}

export default new PlanStore();
