import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";
import KindStore from "./KindStore";
import CrudAction from "../variables/sigma";
import ProducerStore from "./ProducerStore";

class TypeStore extends AbstractStore {
  startedInit = false;
  initialized = false;
  source = [];
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case CrudAction.TYPE_ADD:
        this.add(
          action.payload.kindId,
          action.payload.producerId,
          action.payload.name,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      case CrudAction.TYPE_DELETE:
        if (action.payload.filter !== true)
          this.delete(
            action.payload.id,
            action.payload.onSuccess,
            action.payload.onError
          );
        break;
      case CrudAction.KIND_DELETE:
        this._filterByKind(action.payload.id);
        break;
      case CrudAction.PRODUCER_DELETE:
        this._filterByProducer(action.payload.id);
        break;
      default:
        break;
    }
  }

  init() {
    if (!this.initialized) {
      if (this.startedInit) return;
      this.startedInit = true;
      KindStore.addTypeListener(this.actionInit, this.init.bind(this));
      ProducerStore.addTypeListener(this.actionInit, this.init.bind(this));
      let _this = this;
      this.initRequest(this.getApiPath("/type"), "GET", true, response => {
        _this.source = response;
        _this.initialized = true;
        _this.init();
      });
    } else {
      let initialized = true;
      this.list = this.source.map(item => {
        let kind = KindStore.getById(item.kindId);
        let producer = ProducerStore.getById(item.producerId);
        initialized &= kind.id !== undefined && producer.id !== undefined;
        item.kind = kind;
        item.producer = producer;
        return item;
      });
      if (initialized) {
        this.emit(this.actionChange);
        this.emit(this.actionInit);
      }
    }
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }

  getById(id) {
    if (!this.initialized) {
      this.init();
      return {};
    }
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  getByKindAndProducer(kindId, producerId) {
    if (!this.initialized) this.init();
    return this.list.filter(
      item => item.kind.id === kindId && item.producer.id === producerId
    );
  }

  add(kindId, producerId, name, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/type"),
      "POST",
      true,
      response => {
        _this.source.push({
          id: response.id,
          kindId: kindId,
          producerId: producerId,
          name: name
        });
        _this.list.push({
          id: response.id,
          name: name,
          kind: KindStore.getById(kindId),
          producer: ProducerStore.getById(producerId)
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        kindId: kindId,
        producerId: producerId,
        name: name
      }
    );
  }

  delete(id, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/type?id=" + id),
      "DELETE",
      true,
      response => {
        _this.source = _this.source.filter(item => item.id !== id);
        _this.list = _this.list.filter(item => item.id !== id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      }
    );
  }

  _filterByKind = id => {
    this.source = this.source.filter(item => {
      if (item.kindId === id) {
        Dispatcher.dispatch({
          type: CrudAction.TYPE_DELETE,
          payload: {
            id: item.id,
            filter: true
          }
        });
      }
      return item.kindId !== id;
    });
    this.init();
  };

  _filterByProducer = id => {
    this.source = this.source.filter(item => {
      if (item.producerId === id) {
        Dispatcher.dispatch({
          type: CrudAction.TYPE_DELETE,
          payload: {
            id: item.id,
            filter: true
          }
        });
      }
      return item.producerId !== id;
    });
    this.init();
  };
}

export default new TypeStore();
