import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";

class DirectoryStore extends AbstractStore {
  directories = {};
  filesByDirectory = {};
  inProgress = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
  }

  init() {
    /*if (this.initialized) return;
    if (this.startedInit) return;
    this.startedInit = true;
    let _this = this;
    this.initRequest(
      this.getApiPath("/disk/directory"),
      "GET",
      true,
      response => {
        _this.list = response;
        _this.initialized = true;
        _this.emit(_this.actionChange);
        _this.emit(_this.actionInit);
      }
    );*/
  }

  getById(id) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/disk/directory?id=" + id),
      "GET",
      true,
      response => {
        _this.addDirectory(response);
        _this.emit(_this.actionChange);
      }
    );
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  getFilesById(id) {
    if (id in this.filesByDirectory) return this.filesByDirectory[id];
    if (id !== undefined && this.inProgress.indexOf(id) === -1) {
      this.inProgress.push(id);
      let _this = this;
      this.initRequest(
        this.getApiPath("/disk/file/byDirectory?id=" + id),
        "GET",
        true,
        response => {
          _this.filesByDirectory[id] = response;
          _this.inProgress = _this.inProgress.filter(value => value !== id);
          _this.emit(_this.actionChange);
        }
      );
    }
    return [];
  }

  addDirectory(directory) {
    if ("id" in directory) {
      this.directories[directory.id] = directory;
      directory.nodes.map(subDirectory => this.addDirectory(subDirectory));
    }
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }
}

export default new DirectoryStore();
