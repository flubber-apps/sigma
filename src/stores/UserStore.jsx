import Dispatcher from "../Dispatcher";
import SystemAct from "../variables/system";
import AbstractStore from "./AbstractStore";
import CookieUtil from "../utils/Cookie";

class UserStore extends AbstractStore {
  constructor() {
    super();
    this.users = [];
    this.initUsers = false;
    this.initializedUsers = false;
    this.signed = false;
    this.inProcess = false;
    this.user = {};
    this.initialized = false;
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case SystemAct.USER_SIGN_IN:
        this.signIn(action.payload.email, action.payload.password);
        break;
      case SystemAct.USER_SIGN_OUT:
        this.signOut(action.onSuccess);
        break;
      default:
        break;
    }
  }

  getAll() {
    if (this.initializedUsers === false && this.initUsers === false) {
      let _this = this;
      this.initializedUsers = true;
      this.initRequest(this.getApiPath("/user/all"), "GET", true, response => {
        _this.users = response;
        _this.initUsers = true;
        _this.emit(this.actionChange);
      });
    }
    return this.users;
  }
  getById(id) {
    let users = this.getAll().filter(user => user.id === id);

    if (users.length > 0) return users[0];
    return {};
  }

  signIn(email, password, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/user/signIn"),
      "POST",
      true,
      response => {
        _this.user = response.user;
        CookieUtil.set("PHPSESSID", response.token, 24 * 60 * 60);
        _this.signed = true;
        _this.initialized = true;
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        email,
        password
      }
    );
  }

  signOut(onSuccess) {
    this.initRequest(this.getApiPath("/user/signOut"), "POST", true, response => {
      this.user = {};
      this.initialized = this.inProcess = false;
      if (onSuccess !== undefined)
        onSuccess(response);
    });
  }

  isSigned() {
    return this.signed === true;
  }

  getUser() {
    let _this = this;
    if (this.initialized === false && this.inProcess === false) {
      this.inProcess = true;
      this.initRequest(this.getApiPath("/user"), "GET", true, response => {
        _this.user = response;
        _this.signed = true;
        _this.initialized = true;
        _this.emit(_this.actionChange);
      });
    }
    return this.user;
  }
}

export default new UserStore();
