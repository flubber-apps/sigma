import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";
import CrudAction from "../variables/sigma";
import AssignSizeStore from "./AssignSizeStore";
import WorkspaceStore from "./WorkspaceStore";

class PositionStore extends AbstractStore {
  startedInit = false;
  initialized = false;
  source = [];
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case CrudAction.POSITION_ADD:
        this.add(
          action.payload.sizeOfType,
          action.payload.workspaceId,
          action.payload.name,
          action.payload.uid,
          action.payload.serialNumber,
          action.payload.comment,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      case CrudAction.POSITION_DELETE:
        this.delete(
          action.payload.id,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      default:
        break;
    }
  }

  init() {
    if (!this.initialized) {
      if (this.startedInit) return;
      this.startedInit = true;
      AssignSizeStore.addTypeListener(this.actionInit, this.init.bind(this));
      AssignSizeStore.addChangeListener(this._filterByAssignSize);
      WorkspaceStore.addTypeListener(this.actionInit, this.init.bind(this));
      WorkspaceStore.addChangeListener(this._filterByWorkspace);
      let _this = this;
      this.initRequest(this.getApiPath("/position"), "GET", true, response => {
        _this.source = response;
        _this.initialized = true;
        this.init();
      });
    } else {
      this.list = this.source.map(item => {
        let assignSize = AssignSizeStore.getById(item.sizeOfTypeId);
        return {
          id: item.id,
          name: item.name,
          uid: item.uid,
          serialNumber: item.serialNumber,
          comment: item.comment,
          kind: assignSize.kind || {},
          producer: assignSize.producer || {},
          type: assignSize.type || {},
          size: assignSize.size || {},
          workspace: WorkspaceStore.getById(item.workspaceId)
        };
      });
      this.emit(this.actionChange);
      this.emit(this.actionInit);
    }
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }

  getById(id) {
    if (!this.initialized) {
      this.init();
      return {};
    }
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  add(
    sizeOfType,
    workspaceId,
    name,
    uid,
    serialNumber,
    comment,
    onSuccess,
    onError
  ) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/position"),
      "POST",
      true,
      response => {
        let assignSize = AssignSizeStore.getById(sizeOfType);
        _this.list.push({
          id: response.id,
          sizeOfType: sizeOfType,
          workspaceId: workspaceId,
          name: name,
          uid: uid,
          serialNumber: serialNumber,
          kind: assignSize.kind || {},
          producer: assignSize.producer || {},
          type: assignSize.type || {},
          size: assignSize.size || {},
          workspace: WorkspaceStore.getById(workspaceId)
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        sizeOfType: sizeOfType,
        workspaceId: workspaceId,
        name: name,
        uid: uid,
        serialNumber: serialNumber,
        comment: comment
      }
    );
  }

  delete(id, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/sizeOfType?id=" + id),
      "DELETE",
      true,
      response => {
        _this.list = _this.list.filter(item => item.id !== id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      }
    );
  }

  getState(id, onSuccess, onError) {
    this.initRequest(
      this.getApiPath("/position/state?id=" + id),
      "GET",
      true,
      response => {
        if (onSuccess !== undefined) onSuccess(response);
      },
      (response, xhr) => {
        if (onError !== undefined) onError(response, xhr);
      }
    );
  }

  _filterByAssignSize = () => {};
  _filterByWorkspace = () => {};
}

export default new PositionStore();
