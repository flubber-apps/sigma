import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";
import Action from "../variables/sigma";

class ProducerStore extends AbstractStore {
  startedInit = false;
  initialized = false;
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case Action.PRODUCER_ADD:
        this.add(
          action.payload.name,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      case Action.PRODUCER_DELETE:
        this.delete(
          action.payload.id,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      default:
        break;
    }
  }

  init() {
    if (this.initialized) return;
    if (this.startedInit) return;
    this.startedInit = true;
    let _this = this;
    this.initRequest(this.getApiPath("/producer"), "GET", true, response => {
      _this.list = response;
      _this.initialized = true;
      _this.emit(_this.actionChange);
      _this.emit(_this.actionInit);
    });
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }

  getById(id) {
    if (!this.initialized) {
      this.init();
      return {};
    }
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  add(name, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/producer"),
      "POST",
      true,
      response => {
        _this.list.push({
          id: response.id,
          name: name
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        name: name
      }
    );
  }

  delete(id, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/producer?id=" + id),
      "DELETE",
      true,
      response => {
        _this.list = _this.list.filter(item => item.id !== id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      }
    );
  }
}

export default new ProducerStore();
