import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";

import Action from "../variables/sigma";
import PositionStore from "./PositionStore";
import UserStore from "./UserStore";

class HistoryStore extends AbstractStore {
  startedInit = false;
  initialized = false;
  source = [];
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case Action.ORDER_ADD:
        this.add(
          action.payload.positionId,
          action.payload.subject,
          action.payload.number,
          action.payload.numberOffer,
          action.payload.startAt,
          action.payload.endAt,
          action.payload.content,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      case Action.HISTORY_FILTER_CHANGED:
        break;
      default:
        break;
    }
  }

  init() {
    if (!this.initialized) {
      if (this.startedInit) return;
      this.startedInit = true;
      PositionStore.addTypeListener(this.actionInit, this.init.bind(this));
      PositionStore.addChangeListener(this._filterByPosition);
      let _this = this;
      this.initRequest(this.getApiPath("/history"), "GET", true, response => {
        _this.source = response;
        _this.initialized = true;
        this.init();
      });
    } else {
      this.list = this.source.map(item => {
        let position = PositionStore.getById(item.positionId);
        return {
          id: item.id,
          content: item.content,
          subject: item.subject,
          number: item.number,
          numberOffer: item.numberOffer,
          startAt: item.startAt,
          endAt: item.endAt,
          state: item.state,
          kind: position.kind || {},
          producer: position.producer || {},
          type: position.type || {},
          size: position.size || {},
          position: position,
          workspace: position.workspace
        };
      });
      this.emit(this.actionChange);
      this.emit(this.actionInit);
    }
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }

  getById(id) {
    if (!this.initialized) {
      this.init();
      return {};
    }
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  add(
    positionId,
    subject,
    number,
    numberOffer,
    startAt,
    endAt,
    content,
    onSuccess,
    onError
  ) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/history"),
      "POST",
      true,
      response => {
        let position = PositionStore.getById(positionId);
        _this.list.push({
          id: response.id,
          content: content,
          subject: subject,
          number: number,
          numberOffer: numberOffer,
          startAt: startAt,
          endAt: endAt,
          kind: position.kind || {},
          producer: position.producer || {},
          type: position.type || {},
          size: position.size || {},
          position: position,
          workspace: position.workspace,
          state: "opened"
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        positionId,
        subject,
        number,
        numberOffer,
        startAt,
        endAt,
        content,
        userId: UserStore.getUser().id
      }
    );
  }

  update(id, history, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/history?id=" + id),
      "PUT",
      true,
      response => {
        let historyOld = this.getById(id);
        _this.list = _this.list.filter(item => item.id !== id);
        _this.list.push({
          id: id,
          content: history.content,
          subject: history.subject,
          number: history.number,
          numberOffer: history.numberOffer,
          startAt: history.startAt,
          endAt: history.endAt,
          state: history.state,
          kind: historyOld.kind,
          producer: historyOld.producer,
          type: historyOld.type,
          size: historyOld.size,
          position: historyOld.position,
          workspace: historyOld.workspace
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      history
    );
  }

  getByPositionId = id => {
    if (!this.initialized) {
      this.init();
      return [];
    }
    return this.list.filter(item => item.position.id === id);
  };

  _filterByPosition = () => {
    //return this.list.filter(item => item.position.id !== id);
  };
}

export default new HistoryStore();
