import config from "../variables/configuration";

import { EventEmitter } from "events";

export default class AbstractStore extends EventEmitter {
  _config = config;
  actionChange = "EVENT_CHANGE";
  actionInit = "STORE_INIT";

  constructor() {
    super();

    if (new.target === AbstractStore) {
      throw new TypeError(
        "Cannot construct " +
          AbstractStore.name +
          " class instances directly. Empty is abstract class."
      );
    }
  }

  /**
   * Load init data
   */
  init() {
  }

  /**
   * Get all items (by current language)
   * @return Array
   */
  getAll() {
    throw new TypeError("Not implemented");
  }

  /**
   * Get item by ID
   * @param id
   * @return Object
   */
  getById(id) {
    throw new TypeError("Not implemented");
  }

  getApiPath = (path = "") => {
    return this._config.apiUrl + path;
  };
  /**
   * Register callback to CHANGE event
   * @param callback Callback function
   */
  addChangeListener = callback => {
    this.on(this.actionChange, callback);
  };
  /**
   * Register callback to specific event
   * @param type Type of callback
   * @param callback Callback function
   */
  addTypeListener = (type, callback) => {
    this.on(type, callback);
  };
  /**
   * Remove callback form CHANGE event
   * @param callback Callback function
   */
  removeChangeListener = callback => {
    this.removeListener(this.actionChange, callback);
  };
  /**
   * Remove callback form specific event
   * @param type Type of callback
   * @param callback Callback function
   */
  removeTypeListener = (type, callback) => {
    this.removeListener(type, callback);
  };

  /**
   *
   * @param url
   * @param method
   * @param asynch
   * @param onSuccess
   * @param onError
   * @param ajax
   */
  initRequest(url, method, asynch, onSuccess, onError, ajax, data) {
    let xhr;
    if (window.XMLHttpRequest) {
      xhr = new XMLHttpRequest();
      xhr.withCredentials = true;
    } else {
      xhr = new window.ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.open(method, url, asynch);
    // Ajax request
    if (ajax === true)
      xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    else
      xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");

    xhr.onload = () => {
      let response = {};
      try {
        response = JSON.parse(xhr.responseText);
      } catch (syntaxError) {
        response = {};
      }
      if (xhr.status === 200) {
        if (onSuccess !== undefined) {
          onSuccess(response, xhr);
        }
      } else {
        if (onError !== undefined) {
          onError(response, xhr);
        }
      }
    };
    if (method === "POST" || method === "PUT") {
      xhr.send(JSON.stringify(data));
    } else {
      xhr.send();
    }
  }

  getDownloaded(xhr) {
    let filename = "";
    let disposition = xhr.getResponseHeader("Content-Disposition");
    if (disposition && disposition.indexOf("attachment") !== -1) {
      let filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
      let matches = filenameRegex.exec(disposition);
      if (matches != null && matches[1])
        filename = matches[1].replace(/['"]/g, "");
    }
    let type = xhr.getResponseHeader("Content-Type");
    return {
      blob: new Blob([this.response], { type: type }),
      type: type,
      filename: filename
    };
  }
}
