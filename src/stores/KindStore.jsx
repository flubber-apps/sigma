import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";
import Action from "../variables/sigma";

class KindStore extends AbstractStore {
  startedInit = false;
  initialized = false;
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {
    switch (action.type) {
      case Action.KIND_ADD:
        this.add(action.name, action.onSuccess, action.onError);
        break;
      case Action.KIND_DELETE:
        this.delete(
          action.payload.id,
          action.payload.onSuccess,
          action.payload.onError
        );
        break;
      default:
        break;
    }
  }

  init() {
    if (this.initialized) return;
    if (this.startedInit) return;
    this.startedInit = true;
    let _this = this;
    this.initRequest(this.getApiPath("/kind"), "GET", true, response => {
      _this.list = response;
      _this.initialized = true;
      _this.emit(_this.actionChange);
      _this.emit(_this.actionInit);
    });
  }

  getById(id) {
    if (!this.initialized) {
      this.init();
      return {};
    }
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }

  add(name, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/kind"),
      "POST",
      true,
      response => {
        _this.list.push({
          id: response.id,
          name: name
        });
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      },
      false,
      {
        name: name
      }
    );
  }

  delete(id, onSuccess, onError) {
    let _this = this;
    this.initRequest(
      this.getApiPath("/kind?id=" + id),
      "DELETE",
      true,
      response => {
        _this.list = _this.list.filter(item => item.id !== id);
        _this.emit(_this.actionChange);
        if (onSuccess !== undefined) onSuccess(response);
      },
      response => {
        if (onError !== undefined) onError(response);
      }
    );
  }
}

export default new KindStore();
