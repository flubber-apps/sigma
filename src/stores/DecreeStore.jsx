import Dispatcher from "../Dispatcher";
import AbstractStore from "./AbstractStore";

class DecreeStore extends AbstractStore {
  initialized = false;
  startedInit = false;
  list = [];

  constructor() {
    super();
    Dispatcher.register(this._register.bind(this));
  }

  _register(action) {}

  init() {
    if (this.initialized) return;
    if (this.startedInit) return;
    this.startedInit = true;
    let _this = this;
    this.initRequest(this.getApiPath("/decree"),
      "GET",
      true,
      response => {
        _this.list = response;
        _this.initialized = true;
        _this.emit(_this.actionChange);
        _this.emit(_this.actionInit);
      }
    );
  }

  getById(id) {
    if (!this.initialized) this.init();
    let result = this.list.filter(item => item.id === id);
    if (result.length) return result[0];
    else return {};
  }

  getAll() {
    if (!this.initialized) this.init();
    return this.list;
  }
}

export default new DecreeStore();
