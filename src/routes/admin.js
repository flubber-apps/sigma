import ManageCategory from "../presentation/views/ManageCategory";

import UrlStore from "../stores/UrlStore";

const routes = [
  {
    hidden: true,
    path: UrlStore.getPath("Admin", "Dashboard"),
    sidebarName: "Manage Categories",
    navbarName: "Manage Categories",
    icon: "content_paste",
    component: ManageCategory
  },
  {
    hidden: false,
    path: UrlStore.getPath("Admin", "ManageCategories"),
    sidebarName: "Manage Categories",
    navbarName: "Manage Categories",
    icon: "content_paste",
    component: ManageCategory
  }
];

export default routes;
