import DeviceDetailPage from "../presentation/views/Device/Detail";
import DeviceList from "../presentation/views/Device/List";
import OrderDetail from "../presentation/views/Order/Detail";
import OrderList from "../presentation/views/Order/List";
import Dashboard from "../presentation/views/Dashboard/Front";

import UrlStore from "../stores/UrlStore";
import config from "../variables/configuration";

let language = config.langMapper.getContent(config.lang);


const routes = [
  {
    hidden: true,
    path: UrlStore.getPath("Front", "DeviceDetail"),
    component: DeviceDetailPage
  },
  {
    hidden: false,
    path: UrlStore.getPath("Front", "DeviceList"),
    sidebarName: language.menu.tlp.short,
    navbarName: language.menu.tlp.title,
    icon: "content_paste",
    component: DeviceList,
    exact: true
  },
  {
    hidden: false,
    path: UrlStore.getPath("Front", "OrderList"),
    sidebarName: language.menu.mocev.short,
    navbarName: language.menu.mocev.title,
    icon: "content_paste",
    component: OrderList,
    exact: true,
    strict: true
  },
  {
    hidden: true,
    path: UrlStore.getPath("Front", "OrderDetail"),
    sidebarName: "Order",
    component: OrderDetail
  },
  {
    hidden: true,
    path: UrlStore.getPath("Front", "Dashboard"),
    sidebarName: "Dashboard",
    navbarName: "Dashboard",
    icon: "dashboard",
    component: Dashboard,
    exact: true,
    strict: true
  }
];

export default routes;
