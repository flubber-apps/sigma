import Dispatcher from "../Dispatcher";
import SystemConst from "../variables/system";

class UserActions {
  signIn(email, password) {
    let item = {
      email: email,
      password: password
    };
    Dispatcher.dispatch({
      type: SystemConst.USER_SIGN_IN,
      payload: item
    });
  }
  signOut(onSuccess) {
    Dispatcher.dispatch({type: SystemConst.USER_SIGN_OUT, onSuccess: onSuccess});
  }
}

export default new UserActions();