"use strict";

import Dispatcher from "../Dispatcher";
import SystemConst from "../variables/system";

class NavigationActions {
  reorder(list) {
    Dispatcher.dispatch({
      type: SystemConst.NAVIGATION_REORDER,
      payload: list
    });
  }
}

export default new NavigationActions();