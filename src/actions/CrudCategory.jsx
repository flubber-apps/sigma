import Dispatcher from "../Dispatcher";
import Const from "../variables/sigma";

class CrudCategory {
  addType(kindId, producerId, name, onSuccess, onError) {
    Dispatcher.dispatch({
      type: Const.TYPE_ADD,
      payload: {
        kindId: kindId,
        producerId: producerId,
        name: name,
        onSuccess: onSuccess,
        onError: onError
      }
    });
  }

  assignSize(typeId, sizeId, onSuccess, onError) {
    Dispatcher.dispatch({
      type: Const.TYPE_ASSIGN_ADD,
      payload: {
        typeId: typeId,
        sizeId: sizeId,
        onSuccess: onSuccess,
        onError: onError
      }
    });
  }
  addPosition(
    sizeOfType,
    workspaceId,
    name,
    uid,
    serialNumber,
    comment,
    onSuccess,
    onError
  ) {
    Dispatcher.dispatch({
      type: Const.POSITION_ADD,
      payload: {
        sizeOfType,
        workspaceId,
        name,
        uid,
        serialNumber,
        comment,
        onSuccess,
        onError
      }
    });
  }
  addOrder(
    positionId,
    subject,
    number,
    numberOffer,
    startAt,
    endAt,
    content,
    onSuccess,
    onError
  ) {
    Dispatcher.dispatch({
      type: Const.ORDER_ADD,
      payload: {
        positionId,
        subject,
        number,
        numberOffer,
        startAt,
        endAt,
        content,
        onSuccess,
        onError
      }
    });
  }

  deleteById(type, id, onSuccess, onError) {
    Dispatcher.dispatch({
      type: type,
      payload: {
        id: id,
        onSuccess: onSuccess,
        onError: onError
      }
    });
  }
  removeAssignedUser(workspaceId, userId, onSuccess, onError) {
    Dispatcher.dispatch({
      type: Const.USER_ASSIGN_DELETE,
      payload: {
        workspaceId,
        userId,
        onSuccess,
        onError
      }
    });
  }
  assignUser(workspaceId, userId, onSuccess, onError) {
    Dispatcher.dispatch({
      type: Const.USER_ASSIGN_DELETE,
      payload: {
        workspaceId,
        userId,
        onSuccess,
        onError
      }
    });
  }
}

export default new CrudCategory();
