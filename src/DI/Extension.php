<?php

namespace Flubber\Extension\DI;

use Flubber\Component\Database\IEntityProvider;
use Flubber\Component\DI\CompilerExtension;

class Extension extends CompilerExtension implements IEntityProvider
{
    /**
     * {@inheritdoc}
     */
    public static function getMappingEntities()
    {
        return [
            "Flubber\\Extension\\Entity\\" => __DIR__ . "/../Entity"
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function presenterMapping()
    {
        return [
            "Extension" => "Flubber\\Extension\\Presenters\\*Presenter",
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function loadConfiguration()
    {
        parent::loadConfiguration();

        $this->parseConfigFile(__DIR__ . '/config.json');
    }


}