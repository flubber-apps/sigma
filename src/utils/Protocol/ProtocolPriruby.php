<?php

namespace Flubber\Extension\utils\Protocol;

use Flubber\Extension\ProtocolUtils\ProtocolFunctions;

class ProtocolPriruby extends ProtocolFunctions
{
    private $finishedAt;
    private $values;

    public function __construct($values, $finishedAt) {
        $this->values = $values;
        $this->finishedAt = $finishedAt;
    }

    public function loadData($result) {
        if (!array_key_exists("parallelism", $result)) {
            $result["parallelism"] = $temp = [
                "name" => "Rovnoběžnost",
                "options" => [
                    "yaxis" => [
                        "title" => [
                            "text" => 'Odchylka [mm]'
                        ]
                    ],
                    "xaxis" => [
                        "categories" => [],
                    ]
                ],
                "series" => [
                    [
                        "name" => "Sací příruba",
                        "data" => []
                    ],
                    [
                        "name" => "Výtlačná příruba",
                        "data" => []
                    ],
                    [
                        "name" => "Maximum",
                        "data" => []
                    ]
                ]
            ];
            $result["offset"] = $temp2 = [
                "name" => "Přesazení",
                "options" => [
                    "yaxis" => [
                        "title" => [
                            "text" => 'Odchylka [mm]'
                        ]
                    ],
                    "xaxis" => [
                        "categories" => [],
                    ]
                ],
                "series" => [
                    [
                        "name" => "Sací příruba",
                        "data" => []
                    ],
                    [
                        "name" => "Výtlačná příruba",
                        "data" => []
                    ],
                    [
                        "name" => "Maximum",
                        "data" => []
                    ]
                ]
            ];
        } else {
            $temp = $result["parallelism"];
            $temp2 = $result["offset"];
        }
        $values = $this->values;
        $temp["options"]["xaxis"]["categories"][] = $this->finishedAt->format("Y-m-d");
        $temp["series"][0]["data"][] = ProtocolFunctions::avg(
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.y1.01"]),
                ProtocolFunctions::parseNumber($values["input.y2.01"])
            ),
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.y1.03"]),
                ProtocolFunctions::parseNumber($values["input.y2.03"])
            )
        );
        $temp["series"][1]["data"][] = ProtocolFunctions::avg(
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.y1.02"]),
                ProtocolFunctions::parseNumber($values["input.y2.02"])
            ),
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.y1.04"]),
                ProtocolFunctions::parseNumber($values["input.y2.04"])
            )
        );
        $temp["series"][2]["data"][] = ProtocolFunctions::avg(
            ProtocolFunctions::parseNumber($values["default.criteria.01"]),
            ProtocolFunctions::parseNumber($values["default.criteria.02"])
        );

        $temp2["options"]["xaxis"]["categories"][] = $this->finishedAt->format("Y-m-d");
        $temp2["series"][0]["data"][] = ProtocolFunctions::avg(
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.x1.01"]),
                ProtocolFunctions::parseNumber($values["input.x2.01"])
            ),
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.x1.03"]),
                ProtocolFunctions::parseNumber($values["input.x2.03"])
            )
        );
        $temp2["series"][1]["data"][] = ProtocolFunctions::avg(
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.x1.02"]),
                ProtocolFunctions::parseNumber($values["input.x2.02"])
            ),
            ProtocolFunctions::parallelism(
                ProtocolFunctions::parseNumber($values["input.x1.04"]),
                ProtocolFunctions::parseNumber($values["input.x2.04"])
            )
        );
        $temp2["series"][2]["data"][] = ProtocolFunctions::avg(
            ProtocolFunctions::parseNumber($values["default.criteria.03"]),
            ProtocolFunctions::parseNumber($values["default.criteria.04"])
        );
        return [$temp, $temp2];
    }

    public function isOk() {
        return true;
    }


}