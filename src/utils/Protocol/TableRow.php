<?php

namespace Flubber\Extension\ProtocolUtils;

use PhpOffice\PhpWord\Element\Row;
use Tracy\Debugger;

/**
 * Class TableRow
 * @package Flubber\Extension\Protocol
 * @property-write int $rowWidth
 */
class TableRow extends AbstractElement
{
    public $rowWidth = 9250;
    public $scale = 1.0;
    /** @var Row */
    private $row;
    /** @var array */
    private $source;
    /** @var \PhpOffice\PhpWord\Element\Table */
    private $table;

    /**
     * TableRow constructor.
     * @param Row $row
     * @param array $source
     */
    public function __construct(Row $row, array $source)
    {
        $this->row = $row;
        $this->source = $source;
        $this->table = $row->getParent();
    }

    public function generate()
    {
        foreach ($this->getColumns() as $cell) {
            if (array_key_exists("width", $cell))
                $width = $this->countWidth($cell["width"]);
            else
                $width = $this->getRowWidth() / count($this->getColumns());
            if (array_key_exists("style", $cell))
                $style = $cell["style"];
            else
                $style = [];
            if ($width === null)
                $style["vMerge"] = "continue";
            if ($style !== null && !array_key_exists("vMerge", $style))
                $style["vMerge"] = "restart";
            if (array_key_exists("content", $cell))
                $this->runPart($this->table->addCell($width, $style), $cell["content"]);
            else
                $this->table->addCell($width, $style);
        }
    }
    public function setWidthScale() {
        $this->scale = 1.518918919;
    }
    public function countWidth($width) {
        if ($width === null)
            return null;
        return (int) ($this->scale * $width);
    }
    public function getRowWidth() {
        return $this->countWidth($this->rowWidth);
    }

    public function read()
    {
        $cells = $this->getColumns();
        $objCells = $this->row->getCells();
        for ($cellId = 0; $cellId < count($cells) && $cellId < count($objCells); $cellId++) {
            $cell = $this->getCell($cellId);
            if (array_key_exists("width", $cell))
                $width = $this->countWidth($cell["width"]);
            else
                $width = $this->getRowWidth() / count($this->getColumns());
            $objCell = $objCells[$cellId];
            if ($width !== null && array_key_exists("content", $cell)) {
                $this->runPart($objCell, $cell["content"]);
            }
        }
    }

    public function getColumns()
    {
        if (array_key_exists("data", $this->source))
            return $this->source["data"];
        else return [];
    }

    public function getCell($cellId)
    {
        $cells = $this->getColumns();
        if (count($cells) > $cellId)
            return $cells[$cellId];
        else [];
    }

    /**
     * @param int $rowWidth
     */
    public function setRowWidth($rowWidth)
    {
        $this->rowWidth = $rowWidth;
    }



    public static function generateRowWithEmptyCell(\PhpOffice\PhpWord\Element\Table $table, $count)
    {
        for ($id = 0; $id < $count; $id++) {
            $table->addCell(null, ["vMerge" => "continue"]);
        }
    }
}