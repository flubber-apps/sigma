<?php

namespace Flubber\Extension\ProtocolUtils;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Extension\Entity\Decree;
use PhpOffice\PhpWord\Element\AbstractContainer;
use PhpOffice\PhpWord\Element\Row;
use PhpOffice\PhpWord\Style\Font;
use Tracy\Debugger;

/**
 * Class AbstractElement
 * @package Flubber\Extension\Protocol
 * @property-write bool $mode
 */
abstract class AbstractElement
{
    const VARIABLE = "/\%([a-zA-Z\.0-9]+)\%/";
    /**
     * @var array
     */
    private static $values;
    /**
     * @var bool True for generate and false for reading
     */
    protected $mode = true;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    protected $prefix = 0;
    public $orientation = "portrait";

    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setValues($values)
    {
        self::$values = $values;
    }

    public function getValues()
    {
        ksort(self::$values);
        return self::$values;
    }

    public function setMode($mode)
    {
        $this->mode = (bool)$mode;
    }

    public function run()
    {
        if ($this->mode === true)
            $this->generate();
        else
            $this->read();
    }

    /**
     * Generate structure.
     */
    abstract public function generate();

    /**
     * Read file by structure.
     */
    abstract public function read();

    public function issetVariable($value)
    {
        return preg_match(self::VARIABLE, $value) !== 0;
    }

    public function getVariableName($value)
    {
        $result = preg_match(self::VARIABLE, $value, $matches);
        if ($result !== 0) {
            return $matches[1];
        }
        return "";
    }

    private function parseValue($source, $result)
    {
        $validate = function ($input) {
            return str_replace(["(", ")", ".", "/", ":", " "], ["\\(", "\\)", "\\.", "\\/", "\\:", "\\ "], $input);
        };
        $return = preg_match_all(self::VARIABLE, $source, $matches);
        if ($return !== 0) {
            array_shift($matches);
            $regExpr = "/^(.*)\%" . implode("\%(.*)\%", array_map(function ($item) use ($validate) {
                    return $validate($item);
                }, $matches[0])) . "\%(.*)$/";
            preg_match($regExpr, $source, $staticText);
            array_shift($staticText);
            $regExpr = "/^" . implode("(.*)", array_map(function ($item) use ($validate) {
                    return $validate($item);
                }, $staticText)) . "$/";

            if (preg_match($regExpr, $result, $variables) !== 0) {
                array_shift($variables);
                for ($id = 0; $id < count($matches[0]); $id++) {
                    self::$values[$matches[0][$id]] = $variables[$id];
                }
            }
        }
    }

    private function generateValue($source)
    {
        $variable = $this->getVariableName($source);
        if ($variable === "")
            return "";
        $value = "";//$variable;
        if (array_key_exists($variable, self::$values))
            $value = self::$values[$variable];
        $result = str_replace("%{$variable}%", $value, $source);
        if ($this->issetVariable($result)) {
            return $this->generateValue($result);
        } else return $result;
    }

    public function runPart(AbstractContainer $parent, array $data, $id = 0)
    {
        if ($this->mode) { // GENERATE
            foreach ($data as $type => $value) {
                switch ($type) {
                    case "table":
                        $this->factoryTable($parent, $value)->run();
                        break;
                    case "decreeText": // TODO Make dynamic generation
                        $decrees = $this->em->getRepository(Decree::class)->findAll();
                        $data = array_map(function ($decree) {
                            return $decree->toArray();
                        }, $decrees);
                        usort($data, function ($a, $b) {
                            return $a["id"] <= $b["id"];
                        });
                        $text = $value["text"] + implode("\n", array_map(function ($item) {
                                return $item["name"];
                            }, $data));
                        foreach (explode("\n", $text) as $row)
                            $parent->addText($row, null, ["alignment" => "end"]);
                        $prefixCells = count($parent->getParent()->getCells());
                        /** @var \PhpOffice\PhpWord\Element\Table $table */
                        $table = $parent->getParent()->getParent();
                        $selectDecree = self::$values[$value["value"]];
                        $isFirst = true;
                        foreach ($decrees as $decree) {
                            if (!$isFirst)
                                TableRow::generateRowWithEmptyCell($table, $prefixCells);
                            $isFirst = false;
                            $this->runPart($table->addCell(600, ["bgColor" => "CCCCCC"]), [
                                "text" => [
                                    "data" => $selectDecree === $decree->id ? "ano" : "ne",
                                    "alignment" => ["alignment" => "center"]
                                ]
                            ]);
                        }
                        break;
                    case "decreeValue":
                        $selectDecree = self::$values["control.decree"];
                        if (!array_key_exists("style", $value))
                            $value["style"] = [];

                        if (strtolower($value["data"]) === "ano") {
                            if ($selectDecree === $value["id"]) {
                                $value["style"]["underline"] = Font::UNDERLINE_DOUBLE;
                            } else {
                                $value["style"]["doubleStrikethrough"] = true;
                            }
                        } else { // ne
                            if ($selectDecree !== $value["id"]) {
                                if ($selectDecree !== $value["id"]) {
                                    $value["style"]["underline"] = Font::UNDERLINE_DOUBLE;
                                } else {
                                    $value["style"]["doubleStrikethrough"] = true;
                                }
                            }
                        }
                    case "text":
                        $alignment = null;
                        $style = null;
                        $text = $value;
                        if (is_array($value)) {
                            $alignment = array_key_exists("alignment", $value) ? $value["alignment"] : null;
                            $style = array_key_exists("style", $value) ? $value["style"] : null;
                            $text = array_key_exists("data", $value) ? $value["data"] : $value["text"];
                        }
                        if ($this->issetVariable($text))
                            $text = $this->generateValue($text);
                        foreach (explode("\n", $text) as $item)
                            $parent->addText(trim($item), $style, $alignment);
                        break;
                    case "blockText":
                        foreach ($value as $key => $subValue) {
                            $this->runPart($parent, [$key => $subValue]);
                        }
                        break;
                    case "title":
                        Debugger::barDump($parent);
                        $parent->addTitle($value);
                        break;
                    case "title2":
                        $parent->addTitle($value, 2);
                        break;
                }
            }
        } else { // READ
            foreach ($data as $type => $value) {
                $element = $parent->getElement($id + $this->prefix);
                switch ($type) {
                    case "table":
                        if (!($element instanceof \PhpOffice\PhpWord\Element\Table)) {
                            $this->prefix++;
                            $element = $parent->getElement($id + $this->prefix);
                        }
                        $this->factoryTable($element, $value)->run();
                        break;
                    case "text":
                        $text = $value;
                        if (is_array($value)) {
                            $text = array_key_exists("data", $value) ? $value["data"] : $value["text"];
                        }
                        if ($this->issetVariable($text)) {
                            /** @var AbstractContainer $textRun */
                            if ($element !== null) {
                                $objText = $element->getElement(0);
                                if ($objText)
                                    $this->parseValue($text, $objText->getText());
                                else
                                    $this->parseValue($text, "");
                            } else  $this->parseValue($text, "");
                        }
                        // else <-- Is static text
                        break;
                    case "block":
                        break;
                    case "blockText":
                        break;
                }
                $id++;
            }
        }
    }

    /**
     * @param \PhpOffice\PhpWord\Element\Table|\PhpOffice\PhpWord\Element\AbstractElement $parent
     * @param array $structure
     * @return Table
     */
    public function factoryTable($parent, array $structure)
    {
        if (array_key_exists("style", $structure))
            $style = $structure["style"];
        else $style = null;
        $obj = new Table($parent, $structure, $style);
        $values = self::$values;
        $obj->setEntityManager($this->em);
        $obj->setValues($values);
        $obj->orientation = $this->orientation;
        $obj->mode = $this->mode;
        return $obj;
    }

    public function factoryTableRow(\PhpOffice\PhpWord\Element\Table $table, array $columns, Row $row = null, $height = null)
    {
        if ($this->mode)
            $row = $table->addRow($height);

        $obj = new TableRow($row, $columns);
        if ($this->orientation !== "portrait")
            $obj->setWidthScale();
        $values = self::$values;
        $obj->setEntityManager($this->em);
        $obj->setValues($values);
        $obj->orientation = $this->orientation;
        $obj->mode = $this->mode;
        return $obj;
    }
}