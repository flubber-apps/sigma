<?php

namespace Flubber\Extension\ProtocolUtils;

use PhpOffice\PhpWord\Element\AbstractContainer;
use Tracy\Debugger;

/**
 * Class Table
 * @package Flubber\Extension\Protocol
 * @property-write array $def
 */
class Table extends AbstractElement
{
    private $styleName = "tableDefault";
    /** @var AbstractContainer|\PhpOffice\PhpWord\Element\Table */
    private $parent;
    /** @var array */
    private $structure;
    /** @var \PhpOffice\PhpWord\Element\Table */
    private $table = null;

    public function __construct($parent, array $structure, $style = null)
    {
        if ($parent instanceof \PhpOffice\PhpWord\Element\Table) {
            $this->parent = $parent->getParent();
            $this->table = $parent;
        } else {
            $this->parent = $parent;
        }
        $this->structure = $structure;
        if ($style !== null)
            $this->setStyle($style);
    }

    /**
     * @param string $name
     */
    public function setStyle($name)
    {
        $this->styleName = $name;
    }

    public function generate()
    {
        $this->table = $this->parent->addTable($this->styleName);
        foreach ($this->getData() as $row) {
            if (array_keys($row, "hidden") && $row["hidden"])
                continue;
            $this->factoryTableRow($this->table, $row, null,array_key_exists("height", $row) ? $row["height"] : null)->run();
        }
    }

    public function read()
    {
        if ($this->table === null)
            return;
        $objRows = $this->table->getRows();
        $rows = $this->getData();
        for ($rowId = 0; $rowId < count($rows) && $rowId < count($objRows); $rowId++) {
            if (array_keys($this->getRow($rowId), "hidden") && $this->getRow($rowId)["hidden"])
                continue;
            $this->factoryTableRow($this->table, $this->getRow($rowId), $objRows[$rowId])->run();
        }
    }

    public function getData()
    {
        if (array_key_exists("data", $this->structure))
            return $this->structure["data"];
        else return [];
    }

    public function getRow($id)
    {
        $data = $this->getData();
        if (count($data) > $id)
            return $data[$id];
        else return [];
    }
}