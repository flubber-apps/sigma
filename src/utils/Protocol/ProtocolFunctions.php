<?php

namespace Flubber\Extension\ProtocolUtils;

abstract class ProtocolFunctions
{
    public static function validateNumber($input)
    {
        if (is_string($input)) {
            $input = str_replace(",", ".", $input);
            if (preg_match("/[0-9]+\.[0-9]+/", $input) !== 0) {
                return (double)$input;
            } else return (int)$input;
        } else return $input;
    }

    public static function parseNumber($input)
    {
        if (is_string($input)) {
            $input = str_replace(",", ".", $input);
            if (preg_match("/((\+|\-){0,1}[0-9]+(\.[0-9]+){0,1})/", $input, $matches) !== 0)
                return self::validateNumber($matches[0]);
        }
        return 0;
    }

    public static function avg($num, $num2)
    {
        return ($num + $num2) / 2;
    }

    public static function parallelism($x1, $x2)
    {
        return abs(self::validateNumber($x1) - self::validateNumber($x2));
    }

    public static function flangeOffset($x1, $x2)
    {
        $x1 = self::validateNumber($x1);
        $x2 = self::validateNumber($x2);

        $fi = ($x1 + $x2) / 2;
        return (abs($fi - $x1) + abs($fi - $x2)) / 2;
    }

    abstract public function isOk();
}