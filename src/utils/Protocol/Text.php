<?php

namespace Flubber\Extension\ProtocolUtils;

class Text extends AbstractElement
{
    public function generate()
    {
        if ($this->issetVariable($value))
            $text = $this->generateValue($value);
        else
            $text = $value;
        foreach(explode("\n", $text) as $item)
            $parent->addText(trim($item), null, ["alignment" => "center"]);
    }

    public function read()
    {
        // TODO: Implement read() method.
    }

}