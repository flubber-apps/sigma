<?php

namespace Flubber\Extension\ProtocolUtils;

use Nette\InvalidStateException;
use Nette\NotImplementedException;
use PhpOffice\PhpWord\Reader\ReaderInterface;
use Tracy\Debugger;

class ProtocolReader extends AbstractElement
{
    /** @var string */
    private $path;
    /** @var ReaderInterface */
    private $reader;
    /** @var array */
    private $structure;
    private $type = "docx";

    /**
     * ProtocolReader constructor.
     * @param ReaderInterface $reader
     * @param $path
     * @param array $structure
     */
    public function __construct(ReaderInterface $reader, $path, array $structure, $type)
    {
        $this->reader = $reader;
        $this->path = $path;
        $this->structure = $structure;
        $this->type = $type;
        if (!file_exists($path))
            throw new InvalidStateException("File with path: {$path} not found.");
        $this->mode = false; // Disable generating mode
        $this->setValues([]); // Clear
    }

    public function run()
    {
        $phpWord = $this->reader->load($this->path);
        $id = 0;
        if (array_key_exists("header", $this->structure)) {
            if ($this->type === "docx") {
                $this->runPart($phpWord->getSection($id++)->getHeaders()[1], $this->structure["header"]);
            } else if ($this->type === "odt") {
                $this->runPart($phpWord->getSection(0)->getHeaders()[1], $this->structure["header"]);
            }
        }
        if (array_key_exists("body", $this->structure)) {
            $body = $this->structure["body"];
            $keys = array_keys($body);
            $last = array_pop($keys);

            if ($this->type === "docx") {
                if (is_numeric($last)) {
                    $section = $phpWord->getSection($id++);
                    $subId = 0;
                    foreach ($body as $value) {
                        $this->runPart($section, $value, $subId++);
                    }

                } else $this->runPart($phpWord->getSection($id++), $this->structure["body"]);
            } else if ($this->type === "odt") {
                $this->prefix = 0;
                if (is_numeric($last)) {
                    $section = $phpWord->getSection(0);
                    $subId = 0;
                    foreach ($body as $value) {
                        $this->runPart($section, $value, $subId++);
                    }

                } else $this->runPart($phpWord->getSection($id++), $this->structure["body"]);
            }
        }
        if (array_key_exists("footer", $this->structure))
            $this->runPart($phpWord->getSection($id++)->getFooters()[1], $this->structure["footer"]);
    }

    public function generate()
    {
        throw new NotImplementedException();
    }

    public function read()
    {
        throw new NotImplementedException();
    }
}