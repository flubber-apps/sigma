<?php

namespace Flubber\Extension\ProtocolUtils;

use Nette\NotImplementedException;
use PhpOffice\PhpWord\Element\Section;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Style;
use Tracy\Debugger;

class ProtocolGenerator extends AbstractElement
{
    /** @var PhpWord */
    private $phpWord;
    private $structure = [];


    /**
     * ProtocolGenerator constructor.
     * @param PhpWord $phpWord
     * @param array $structure
     * @param $values
     */
    public function __construct(PhpWord $phpWord, array $structure, $values)
    {
        $this->setValues($values);
        $this->phpWord = $phpWord;
        $this->structure = $structure;
        $this->mode = true; // Enable generating mode
    }

    public function run()
    {
        $styleSection = null;
        if (array_key_exists("orientation", $this->structure)) {
            $styleSection = ["orientation" => $this->structure["orientation"]];
            $this->orientation = $this->structure["orientation"];
        }
        if (array_key_exists("styles", $this->structure)) {
            $styles = $this->structure["styles"];
            if (array_key_exists("tableDefault", $styles)) {
                $this->phpWord->addTableStyle("tableDefault", $styles["tableDefault"], $styles["tableFirstRowDefault"]);
            }
            if (array_key_exists("tableBorder", $styles)) {
                $this->phpWord->addTableStyle("tableBorder", $styles["tableBorder"], $styles["tableFirstRowBorder"]);
            }
            if (array_key_exists("tableBorderAround", $styles)) {
                $this->phpWord->addTableStyle("tableBorderAround", $styles["tableBorderAround"], null);
            }
        }
        $this->phpWord->addTitleStyle(1, ["font" => "Arial", "size" => 12, "bold" => true]);
        if (array_key_exists("header", $this->structure))
            $this->runPart($this->createSection($this->phpWord, $styleSection)->addHeader(), $this->structure["header"]);
        if (array_key_exists("body", $this->structure)) {
            $body = $this->structure["body"];
            $keys = array_keys($body);
            $last = array_pop($keys);
            if (is_numeric($last)) {
                $section = $this->createSection($this->phpWord, $styleSection);
                foreach ($body as $value) {
                    $this->runPart($section, $value);
                }

            } else $this->runPart($this->createSection($this->phpWord, $styleSection), $this->structure["body"]);
        }
        if (array_key_exists("footer", $this->structure))
            $this->runPart($this->createSection($this->phpWord, $styleSection)->addFooter(), $this->structure["footer"]);
    }

    public function generate()
    {
        throw new NotImplementedException();
    }

    public function read()
    {
        throw new NotImplementedException();
    }

    /**
     * @param $parent
     * @param $styles
     *
     * @return Section
     */
    private function createSection(PhpWord $parent, array $styles = null)
    {
        $style = [
            "font" => "Arial",
            "size" => 22
        ];
        Debugger::barDump($styles);
        $section = $parent->addSection($styles);
        $style = new Style();
        $font = new Style\Font();
        //Debugger::barDump($section->getPhpWord()->getDefaultFontName());
        //Debugger::barDump($section->getStyle());
        return $section;
    }

}