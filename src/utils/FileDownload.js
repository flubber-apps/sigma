export default class FileDownload {
  url = "";
  filename = "";
  type = "";
  _global =
    typeof window === "object" && window.window === window
      ? window
      : typeof global === "object" && global.global === global
        ? global
        : void 0;

  constructor(url, filename, type) {
    this.url = url;
    this.filename = filename;
    this.type = type;
  }
  _bom(blob, opts) {
    if (typeof opts === "undefined") opts = { autoBom: false };
    else if (typeof opts !== "object") {
      console.warn("Deprecated: Expected third argument to be a object");
      opts = { autoBom: !opts };
    }

    // prepend BOM for UTF-8 XML and text/* types (including HTML)
    // note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
    if (
      opts.autoBom &&
      /^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(
        blob.type
      )
    ) {
      return new Blob([String.fromCharCode(0xfeff), blob], { type: blob.type });
    }
    return blob;
  }
  _click(node) {
    try {
      node.dispatchEvent(new MouseEvent("click"));
    } catch (e) {
      var evt = document.createEvent("MouseEvents");
      evt.initMouseEvent(
        "click",
        true,
        true,
        window,
        0,
        0,
        0,
        80,
        20,
        false,
        false,
        false,
        false,
        0,
        null
      );
      node.dispatchEvent(evt);
    }
  }
  _corsEnabled(url) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);
    xhr.send();
    return xhr.status >= 200 && xhr.status <= 299;
  }
  _download(url, name, opts) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.responseType = "blob";
    xhr.onload = function() {
      this.saveAs(xhr.response, name, opts);
    };
    xhr.onerror = function() {
      console.error("could not download file");
    };
    xhr.send();
  }

  saveAs(blob, name, opts, popup) {
    // Use download attribute first if possible (#193 Lumia mobile)
    if ("download" in HTMLAnchorElement.prototype) {
      var URL = this._global.URL || this._global.webkitURL;
      var a = document.createElement("a");
      name = name || blob.name || "download";

      a.download = name;
      a.rel = "noopener"; // tabnabbing

      // TODO: detect chrome extensions & packaged apps
      // a.target = '_blank'

      if (typeof blob === "string") {
        // Support regular links
        a.href = blob;
        if (a.origin !== window.location.origin) {
          this._corsEnabled(a.href)
            ? this._download(blob, name, opts)
            : this._click(a, (a.target = "_blank"));
        } else {
          this._click(a);
        }
      } else {
        // Support blobs
        a.href = URL.createObjectURL(blob);
        setTimeout(function() {
          URL.revokeObjectURL(a.href);
        }, 4e4); // 40s
        setTimeout(function() {
          this._click(a);
        }, 0);
      }
    } else if ("msSaveOrOpenBlob" in navigator) {
      name = name || blob.name || "download";

      if (typeof blob === "string") {
        if (this._corsEnabled(blob)) {
          this._download(blob, name, opts);
        } else {
          var a = document.createElement("a");
          a.href = blob;
          a.target = "_blank";
          setTimeout(function() {
            this._click(a);
          });
        }
      } else {
        navigator.msSaveOrOpenBlob(this._bom(blob, opts), name);
      }
    } else {
      // Open a popup immediately do go around popup blocker
      // Mostly only available on user interaction and the fileReader is async so...
      popup = popup || window.open("", "_blank");
      if (popup) {
        popup.document.title = popup.document.body.innerText = "downloading...";
      }

      if (typeof blob === "string") return this._download(blob, name, opts);

      var force = blob.type === "application/octet-stream";
      var isSafari = /constructor/i.test(this._global.HTMLElement) || this._global.safari;
      var isChromeIOS = /CriOS\/[\d]+/.test(navigator.userAgent);

      if (
        (isChromeIOS || (force && isSafari)) &&
        typeof FileReader === "object"
      ) {
        // Safari doesn't allow downloading of blob URLs
        var reader = new FileReader();
        reader.onloadend = function() {
          var url = reader.result;
          url = isChromeIOS
            ? url
            : url.replace(/^data:[^;]*;/, "data:attachment/file;");
          if (popup) popup.location.href = url;
          else window.location = url;
          popup = null; // reverse-tabnabbing #460
        };
        reader.readAsDataURL(blob);
      } else {
        var URL = this._global.URL || this._global.webkitURL;
        var url = URL.createObjectURL(blob);
        if (popup) popup.location = url;
        else window.location.href = url;
        popup = null; // reverse-tabnabbing #460
        setTimeout(function() {
          URL.revokeObjectURL(url);
        }, 4e4); // 40s
      }
    }
  }
}
