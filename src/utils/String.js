export default class StringUtil {
  /**
   * Check input email if is valid email address.
   * @param email
   * @returns {boolean}
   */
  static isEmail(email) {
    let re = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
    return re.test(String(email).toLowerCase());
  }

  static toCamelCase(value) {
    return value.replace(/_(.)/g, function(match, chr) {
      return chr.toUpperCase();
    });
  }

  static toSnakeCase(value) {
    return value
      .replace(/([A-Z])/g, function($1) {
        return "_" + $1.toLowerCase();
      })
      .replace(/^_/, "");
  }
}
