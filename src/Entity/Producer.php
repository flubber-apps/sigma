<?php

namespace Flubber\Extension\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;

/**
 * @ORM\Table(name="sigma_archive_producer")
 * @ORM\Entity
 */
class Producer extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", precision=0, scale=0, nullable=false, unique=true)
     */
    protected $name;
    /**
     * @var Type[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Type", mappedBy="producer")
     */
    protected $types;

    public function __construct()
    {
        $this->types = new \Doctrine\Common\Collections\ArrayCollection();
    }
}