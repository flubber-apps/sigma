<?php

namespace Flubber\Extension\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\DiskBrowser\Entity\Directory;

/**
 * @ORM\Table(name="sigma_archive_position", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"uid", "serial_number"})})
 * @ORM\Entity
 */
class Position extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $name;
    /**
     * @var integer
     *
     * @ORM\Column(type="string", length=32, precision=0, scale=0, nullable=true, unique=false)
     */
    protected $uid = null;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, precision=0, scale=0, nullable=true, unique=false)
     */
    protected $serialNumber = null;
    /**
     * @var string
     *
     * @ORM\Column(type="text", precision=0, scale=0, nullable=true, unique=false)
     */
    protected $comment = null;
    /**
     * @var SizeDirectory
     *
     * @ORM\ManyToOne(targetEntity="SizeDirectory", inversedBy="positions", cascade={"persist"})
     * @ORM\JoinColumn(name="size_directory_id", referencedColumnName="id", onDelete="CASCADE",
     *                                           nullable=false)
     */
    protected $sizeDirectory;
    /**
     * @var Workspace
     *
     * @ORM\ManyToOne(targetEntity="Workspace", inversedBy="positions", cascade={"persist"})
     * @ORM\JoinColumn(name="workspace_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $workspace;
    /**
     * @var Directory
     *
     * @ORM\OneToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\Directory", cascade={"persist"})
     * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $directory;
    /**
     * @var History[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="History", mappedBy="position", cascade={"persist"})
     */
    protected $histories;

    public function __construct()
    {
        $this->histories = new \Doctrine\Common\Collections\ArrayCollection;
    }

}
