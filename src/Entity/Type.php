<?php

namespace Flubber\Extension\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\DiskBrowser\Entity\Directory;
use Flubber\Component\DiskBrowser\Entity\File;

/**
 * @ORM\Table(name="sigma_archive_type", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name", "kind_id", "producer_id"})})
 * @ORM\Entity
 * @method Kind getKind()
 * @method Producer getProducer()
 */
class Type extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $name;
    /**
     * @var string
     *
     * ORM\Column(name="json_form_values", type="text", nullable=true, unique=false)
     */
    private $formData;
    /**
     * Directory for data of all sizes
     *
     * @var Directory
     *
     * @ORM\OneToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\Directory", cascade={"persist"})
     * @ORM\JoinColumn(name="sizes_directory_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $root;
    /**
     * @var Producer
     *
     * @ORM\ManyToOne(targetEntity="Producer", inversedBy="types")
     * @ORM\JoinColumn(name="producer_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $producer;
    /**
     * @var Kind
     *
     * @ORM\ManyToOne(targetEntity="Kind", inversedBy="types")
     * @ORM\JoinColumn(name="kind_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $kind;
    /**
     * @var SizeDirectory[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="SizeDirectory", cascade={"persist"}, mappedBy="type")
     */
    protected $sizes;

    public function getFormData()
    {
        return (array) json_decode($this->formData, true);
    }

    public function setFormData($data)
    {
        $this->formData = json_encode((array) $data);
        return $this;
    }

}
