<?php

namespace Flubber\Extension\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;

/**
 * Type of control
 *
 * @ORM\Table(name="sigma_mocev_control_type")
 * @ORM\Entity
 */
class ControlType extends BaseEntity
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=10, unique=true, nullable=false)
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, unique=false, length=32)
     */
    protected $name;

    public function toArray() {
        return [
            "id" => $this->id,
            "name" => $this->name
        ];
    }
}