<?php

namespace Flubber\Extension\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\DiskBrowser\Entity\Directory;

/**
 * @ORM\Table(name="sigma_archive_size_directory", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"type_id", "size_id"})})
 * @ORM\Entity
 */
class SizeDirectory extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var Directory
     *
     * ORM\ManyToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\Directory", cascade={"persist"})
     * ORM\JoinColumn(name="directory_id", referencedColumnName="id", nullable=false, unique=false, onDelete="cascade")
     */
    protected $directory;
    /**
     * @var Type
     *
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="sizes", cascade={"persist"})
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=false, unique=false, onDelete="cascade")
     */
    protected $type;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|Position[]
     *
     * @ORM\OneToMany(targetEntity="Position", mappedBy="sizeDirectory")
     */
    protected $positions;
    /**
     * @var Size
     *
     * @ORM\ManyToOne(targetEntity="Size", inversedBy="directories", cascade={"persist"})
     * @ORM\JoinColumn(name="size_id", referencedColumnName="id", nullable=false, unique=false, onDelete="cascade")
     */
    protected $size;

    public function __construct()
    {
        $this->positions = new ArrayCollection();
    }

}