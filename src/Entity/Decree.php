<?php

namespace Flubber\Extension\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;

/**
 * @ORM\Table(name="sigma_mocev_decree")
 * @ORM\Entity
 */
class Decree extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, precision=0, scale=0, nullable=false, unique=true)
     */
    protected $name;

    public function toArray() {
        return [
            "id" => $this->getId(),
            "name" => $this->name
        ];
    }
}
