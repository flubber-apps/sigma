<?php

namespace Flubber\Extension\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\DiskBrowser\Entity\Directory;
use Flubber\Component\Security\Entity\User;

/**
 * @ORM\Table(name="sigma_archive_history")
 * @ORM\Entity
 */
class History extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $endOfOrder;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     */
    protected $startOfOrder;
    /**
     * @var string
     *
     * @ORM\Column(type="text", precision=0, scale=0, nullable=true)
     */
    protected $content;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=256, precision=0, scale=0, nullable=false, unique=false)
     */
    protected $subject;
    /**
     * @var string Number of offer
     *
     * @ORM\Column(type="string", length=64, precision=0, scale=0, nullable=true, unique=false)
     */
    protected $number;
    /**
     * @var string Business case number
     *
     * @ORM\Column(type="string", length=64, precision=0, scale=0, nullable=false, unique=true)
     */
    protected $numberOffer;
    /**
     * @var Directory
     *
     * @ORM\OneToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\Directory", cascade={"persist"})
     * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $directory;
    /**
     * @var Plan
     *
     * @ORM\OneToOne(targetEntity="Plan", cascade={"persist"})
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id", onDelete="CASCADE", nullable=true, unique=true)
     */
    protected $plan;

    /**
     * Author
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Flubber\Component\Security\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", unique=false, nullable=false, onDelete="CASCADE")
     */
    protected $user;
    /**
     * @var Position
     *
     * @ORM\ManyToOne(targetEntity="Position", inversedBy="histories")
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id", unique=false, nullable=false,
     *                                     onDelete="CASCADE")
     */
    protected $position;
    /**
     * @var string One of "open", "cancel", "approved", "finished"
     *
     * @ORM\Column(name="state", length=1, nullable=false)
     */
    protected $state = "o";

    private $stateMapping = [
            "o" => "opened",
            "c" => "canceled",
            "a" => "approved",
            "f" => "finished"
        ];

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->endOfOrder = null;
    }

    public function setEndOfOrder(\Datetime $date = null)
    {
        $this->endOfOrder = $date;
    }

    public function setStartOfOrder(\Datetime $date)
    {
        $this->startOfOrder = $date;
    }

    public function isOpened()
    {
        return $this->endOfOrder->format('Y-m-d H:i:s') == (new \DateTime())->format('Y-m-d H:i:s');
    }

    public function setState($state) {
        $this->state = array_search(strtolower($state), $this->stateMapping);
    }

    public function getState() {
        return $this->stateMapping[$this->state];
    }

    public function toArray() {
        return [
            "id" => $this->getId(),
            "content" => $this->content,
            "subject" => $this->subject,
            "number" => $this->number,
            "numberOffer" => $this->numberOffer,
            "startAt" => $this->startOfOrder->format("d-m-Y"),
            "endAt" => $this->endOfOrder ? $this->endOfOrder->format("d-m-Y") : null,
            "state" => $this->getState(),
            "positionId" => $this->position->getId(),
            "planId" => $this->plan->getId()
        ];
    }

}