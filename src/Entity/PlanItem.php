<?php

namespace Flubber\Extension\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\DiskBrowser\Entity\File;
use Flubber\Component\Security\Entity\User;

/**
 * @ORM\Table(name="sigma_mocev_plan_item")
 * @ORM\Entity
 */
class PlanItem extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(nullable=false, length=64)
     */
    protected $name;
    /**
     * @var bool
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $finishedAt = null;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $generated = false;
    /**
     * @var int
     *
     * @ORM\Column(name="order_no", type="integer", nullable=false)
     */
    protected $order = 0;
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Decree", cascade={"persist"})
     * @ORM\JoinColumn(name="decree_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    protected $decree = null;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $watched = false;
    /**
     * @var Plan
     *
     * @ORM\ManyToOne(targetEntity="Plan", inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(name="plan_id", referencedColumnName="id", nullable=false, unique=false, onDelete="CASCADE")
     */
    protected $plan;
    /**
     * @var ControlType
     *
     * @ORM\ManyToOne(targetEntity="ControlType")
     * @ORM\JoinColumn(name="control_type_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $controlType;
    /**
     * @var PlanItem
     *
     * @ORM\ManyToOne(targetEntity="PlanItem", cascade={"persist"})
     * @ORM\JoinColumn(name="source_item_id", referencedColumnName="id", nullable=true, unique=false, onDelete="CASCADE")
     */
    protected $sourceItem = null;
    /**
     * @var ProtocolTemplate
     *
     * @ORM\ManyToOne(targetEntity="ProtocolTemplate", cascade={"persist"})
     * @ORM\JoinColumn(name="protocol_id", referencedColumnName="id", nullable=true, unique=false, onDelete="CASCADE")
     */
    protected $protocol = null;
    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\File", cascade={"persist"})
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", nullable=true, unique=false, onDelete="SET NULL")
     */
    protected $protocolFile = null;
    /**
     * @var string
     *
     * @ORM\Column(name="protocol_values", type="text", nullable=true)
     */
    protected $values = "{}";
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="Flubber\Component\Security\Entity\User", cascade={"persist"})
     * @ORM\JoinColumn(name="approved_by", referencedColumnName="id", nullable=true, unique=false, onDelete="CASCADE")
     */
    protected $approvedBy = null;
    /**
     * @var null|string
     *
     * @ORM\Column(name="u_name", unique=false, nullable=true)
     */
    protected $uName = null;

    public function setValues($data)
    {
        $this->values = json_encode($data);
    }

    public function getValues()
    {
        return json_decode($this->values, true);
    }

    public function toArray()
    {
        return [
            "id" => $this->getId(),
            "name" => $this->name,
            "generated" => $this->generated,
            "finishedAt" => $this->finishedAt ? $this->finishedAt->format("d-m-Y") : null,
            "order" => $this->order,
            "decreeId" => $this->decree !== null ? $this->decree->getId() : null,
            "watched" => $this->watched,
            "controlTypeId" => $this->controlType->getId(),
            "protocolId" => $this->protocol !== null ? $this->protocol->getId() : null,
            "protocolUid" => $this->protocolFile !== null ? $this->protocolFile->getUName() : null,
            "approvedBy" => $this->approvedBy !== null ? $this->approvedBy->getId() : null
        ];
    }
}