<?php

namespace Flubber\Extension\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\Security\Entity\User;
use Tracy\Debugger;

/**
 * @ORM\Table(name="sigma_archive_workspace", uniqueConstraints={@ORM\UniqueConstraint(name="name", columns={"name"})})
 * @ORM\Entity
 */
class Workspace extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    protected $name;
    /**
     * @var Position[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Position", mappedBy="workspace")
     */
    protected $positions;
    /**
     * @var User[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Flubber\Component\Security\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="workspace_users",
     *     joinColumns={@ORM\JoinColumn(name="workspace_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")})
     */
    protected $users = [];

    public function __construct()
    {
        $this->positions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function removeUser(User $user) {
        $this->users->removeElement($user);
    }
    public function assignUser(User $user) {
        $this->users->add($user);
    }

    public function toArray() {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "users" => array_map(function($user) {
                return $user->getId();
            }, $this->users->toArray())
        ];
    }

}
