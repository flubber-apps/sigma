<?php

namespace Flubber\Extension\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\DiskBrowser\Entity\Directory;
use Flubber\Component\DiskBrowser\Entity\File;

/**
 * @ORM\Table(name="sigma_mocev_plan")
 * @ORM\Entity
 */
class Plan extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var PlanItem[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PlanItem", mappedBy="plan")
     */
    protected $items;
    /**
     * @var Type
     *
     * @ORM\ManyToOne(targetEntity="Type", cascade={"persist"})
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id", nullable=true, onDelete="CASCADE", unique=false)
     */
    protected $type;
    /**
     * @var Position
     *
     * @ORM\ManyToOne(targetEntity="Position", cascade={"persist"})
     * @ORM\JoinColumn(name="position_id", referencedColumnName="id", onDelete="CASCADE", nullable=true, unique=false)
     */
    protected $position = null;
    /**
     * @var Workspace
     *
     * @ORM\ManyToOne(targetEntity="Workspace", cascade={"persist"})
     * @ORM\JoinColumn(name="workspace_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $workspace = null;
    /**
     * @var Plan
     *
     * @ORM\ManyToOne(targetEntity="Plan", cascade={"persist"})
     * @ORM\JoinColumn(name="source_plan_id", referencedColumnName="id", onDelete="SET NULL", nullable=true, unique=false)
     */
    protected $sourcePlan = null;
    /**
     * Directory for protocols
     *
     * @var Directory
     *
     * @ORM\OneToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\Directory", cascade={"persist"})
     * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    protected $root;
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean"   )
     */
    protected $generatePlan = true;
    /**
     * @var null|File
     * @ORM\ManyToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\File", cascade={"persist"})
     * @ORM\JoinColumn(name="file_plan_id", referencedColumnName="id", onDelete="SET NULL", nullable=true, unique=false)
     */
    protected $generatedPlan = null;

    public function __construct()
    {
        $this->items = new ArrayCollection;
    }

    public function toArray()
    {
        return [
            "id" => $this->getId(),
            "typeId" => $this->type !== null ? $this->type->getId() : null,
            "positionId" => $this->position !== null ? (int)$this->position->getId() : null,
            "directoryId" => $this->root !== null ? $this->root->getId() : null,
            "items" => array_map(function ($item) {
                return $item->toArray();
            }, $this->items->toArray()),
            "generatedPlan" => $this->generatePlan,
            "generatedPlanFile" => $this->generatedPlan === null ? null : $this->generatedPlan->getUName()
        ];
    }
}