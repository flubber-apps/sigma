<?php

namespace Flubber\Extension\Entity;

use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\DiskBrowser\Entity\Directory;
use Flubber\Component\DiskBrowser\Entity\File;

/**
 * @ORM\Table(name="sigma_mocev_protocol_template")
 * @ORM\Entity
 */
class ProtocolTemplate extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, precision=0, scale=0, nullable=false, unique=true)
     */
    protected $number;
    /**
     * @var string
     *
     * @ORM\Column(name="structure", type="text", nullable=true, unique=false)
     */
    protected $structure;
    /**
     * @var string
     *
     * @ORM\Column(name="structure_defaults", type="text", nullable=true, unique=false)
     */
    protected $defaultValues;
    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=false, unique=true)
     */
    protected $uid;
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Workspace")
     * @ORM\JoinColumn(name="workspace_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $workspace;
    /**
     * Directory for protocols
     *
     * @var Directory
     *
     * @ORM\OneToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\Directory", cascade={"persist"})
     * @ORM\JoinColumn(name="directory_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $root;
    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="Flubber\Component\DiskBrowser\Entity\File", cascade={"persist"})
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id", nullable=true, unique=false, onDelete="SET NULL")
     */
    protected $file = null;

    /**
     * @return array
     */
    public function getStructure()
    {
        return json_decode($this->structure, true);
    }

    /**
     * @param string $structure
     */
    public function setStructure($structure)
    {
        $this->structure = json_encode($structure);
    }

    public function getDefaultValues()
    {
        return json_decode($this->defaultValues, true);
    }

    public function isGenerated() {
        return $this->file === null;
    }

    public function toJson()
    {
        return [
            "id" => $this->getId(),
            "name" => $this->name,
            "number" => $this->number,
            "isGenerated" => $this->isGenerated(),
            "workspaceId" => $this->workspace !== null ? $this->workspace->getId() : null
        ];
    }


}
