<?php

namespace Flubber\Extension\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;

/**
 * @ORM\Table(name="sigma_archive_size")
 * @ORM\Entity
 */
class Size extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", precision=0, scale=0, nullable=false, unique=true)
     */
    protected $name;
    /**
     * @var SizeDirectory[]|\Doctrine\Common\Collections\ArrayCollection()
     *
     * @ORM\OneToMany(targetEntity="SizeDirectory", mappedBy="size")
     */
    protected $directories;

}