<?php

namespace Flubber\Extension\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Flubber\Component\Database\Entity\BaseEntity;

/**
 * @ORM\Table(name="sigma_archive_kind")
 * @ORM\Entity
 */
class Kind extends BaseEntity
{
    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, precision=0, scale=0, nullable=false, unique=true)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(name="json_form", type="text", nullable=true, unique=false)
     */
    protected $form;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, unique=false)
     */
    protected $sizeDirectories = "";
    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false, unique=false)
     */
    protected $positionDirectories = "";
    /**
     * @var Type[]|\Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Type", mappedBy="kind")
     */
    protected $types;

    public function __construct()
    {
        $this->types = new ArrayCollection();
    }

    public function addInput(array $data)
    {
        $temp = $this->getForm();
        $temp[] = $data;
        $this->setForm($temp);
    }

    public function getForm()
    {
        return (array)json_decode($this->form, true);
    }

    public function setForm($data)
    {
        $this->form = json_encode((array)$data);
        return $this;
    }

    /**
     * @param $selector
     * @param $value
     * @return array|bool
     */
    public function getInput($selector, $value)
    {
        foreach ($this->getForm() as $input) {
            if (array_key_exists($selector, $input) && $input[$selector] === $value)
                return $input;
        }
        return [];
    }

    /**
     * @return string
     */
    public function getPositionDirectories()
    {
        return json_decode($this->positionDirectories);
    }

    /**
     * @param string $positionDirectories
     */
    public function setPositionDirectories($positionDirectories)
    {
        $this->positionDirectories = json_encode(array_unique(explode("\n", str_replace(';', "\n", rtrim($positionDirectories)))));
    }

    /**
     * @return string
     */
    public function getSizeDirectories()
    {
        return json_decode($this->sizeDirectories);
    }

    /**
     * @param string $sizeDirectories
     */
    public function setSizeDirectories($sizeDirectories)
    {
        $this->sizeDirectories = json_encode(array_unique(explode("\n", str_replace(';', "\n", rtrim($sizeDirectories)))));
    }


}
