<?php

namespace Flubber\Extension\Facade;

use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\Database\Facade\BaseFacade;
use Flubber\Extension\Entity;

class Size extends BaseFacade
{
    public function create(BaseEntity $entity, $throwException = false, $autoFlush = true)
    {
        return $this->update($entity, $throwException, $autoFlush);
    }

    public function insert(array $data, $throwExceptions = false)
    {
        $entity = new Entity\Size;
        $entity->setName($data["name"]);

        return $this->create($entity, $throwExceptions);
    }

    public function getEntityClass()
    {
        return Entity\Size::class;
    }

    public function findPairs($value, $orderBy = [], $key = null)
    {
        if (count($orderBy))
            $orderBy['name'] = 'ASC';
        return parent::findPairs($value, $orderBy, $key);
    }


    public function getSizesListByType($typeId)
    {
        $qb = $this->em->createQueryBuilder();
        $data = $qb->select('size')
            ->from(Entity\Size::class, 'size')
            ->join('size.directories', 'sidi')
            ->join('sidi.type', 'type')
            ->where('type.id = :type')
            ->orderBy('size.name')
            ->setParameter('type', $typeId)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($data as $size) {
            $result[$size->getId()] = $size->name;
        }

        return $result;
    }
}