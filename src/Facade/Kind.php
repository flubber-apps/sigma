<?php

namespace Flubber\Extension\Facade;

use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\Database\Facade\BaseFacade;
use Flubber\Extension\Entity;

class Kind extends BaseFacade
{
    public function create(BaseEntity $entity, $throwException = false, $autoFlush = true)
    {
        return $this->update($entity, $throwException, $autoFlush);
    }

    public function insert(array $data, $throwExceptions = false)
    {
        $entity = new Entity\Kind;
        $entity->setName($data["name"]);

        return $this->create($entity, $throwExceptions);
    }

    public function getEntityClass()
    {
        return Entity\Kind::class;
    }
    
    public function findPairs($value, $orderBy = [], $key = null)
    {
        if (count($orderBy) === 0)
            $orderBy["name"] = "ASC";
        return parent::findPairs($value, $orderBy, $key);
    }


}