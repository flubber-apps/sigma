<?php

namespace Flubber\Extension\Facade;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\Database\Facade\BaseFacade;
use Flubber\Component\DiskBrowser\Facade\Directory;
use Flubber\Extension\Entity;
use Nette\Caching\IStorage;

class Position extends BaseFacade
{
    /** @var Directory */
    private $directoryFacade;

    public function __construct(EntityManagerInterface $em, IStorage $storage = null)
    {
        parent::__construct($em, $storage);
        $this->directoryFacade = new Directory($em, $storage);
    }


    public function create(BaseEntity $entity, $throwException = false, $autoFlush = true)
    {
        return $this->update($entity, $throwException, $autoFlush);
    }

    public function insert(array $data, $throwExceptions = false)
    {
        $entity = new Entity\Position;
        $entity->name = $data["name"];
        if (array_key_exists("serialNumber", $data))
            $entity->serialNumber = $data["serialNumber"];
        if (array_key_exists("uid", $data))
            $entity->uid = $data["uid"];
        if (array_key_exists("comment", $data))
            $entity->comment = $data["comment"];
        $temp = $data["sizeDirectory"];
        if (is_numeric($temp))
            $entity->sizeDirectory = $this->em->getPartialReference(Entity\SizeDirectory::class, $temp);
        else
            $entity->sizeDirectory = $temp;
        $temp = $data["workspace"];
        if (is_numeric($temp))
            $entity->workspace = $this->em->getPartialReference(Entity\Workspace::class, $temp);
        else
            $entity->workspace = $temp;
        $entity-> directory = $this->directoryFacade->createRootDirectory($data["name"]);

        return $this->create($entity, $throwExceptions);
    }


    public function getEntityClass()
    {
        return Entity\Position::class;
    }

    /**
     * @param integer $typeId
     * @param integer $sizeId
     * @param integer $workspaceId
     * @return array
     */
    public function getListByTypeSizeWorkspace($typeId, $sizeId, $workspaceId)
    {
        $qb = $this->em->createQueryBuilder();
        $data = $qb->select('posi')
            ->from(Entity\Position::class, 'posi')
            ->join('posi.sizeDirectory', 'sidi')
            ->join('posi.workspace', 'work')
            ->join('sidi.size', 'size')
            ->join('sidi.type', 'type')
            ->where('type.id = :type')
            ->andWhere('size.id = :size')
            ->andWhere('work.id = :work')
            ->orderBy('posi.name')
            ->setParameter('type', $typeId)
            ->setParameter('size', $sizeId)
            ->setParameter('work', $workspaceId)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($data as $row) {
            $result[$row->id] = $row->uid;
        }

        return $result;
    }

    public function getListByTypeSize($typeId, $sizeId)
    {
        $qb = $this->em->createQueryBuilder();
        $data = $qb->select('posi')
            ->from(Entity\Position::class, 'posi')
            ->join('posi.sizeDirectory', 'sidi')
            ->join('sidi.size', 'size')
            ->join('sidi.type', 'type')
            ->where('type.id = :type')
            ->andWhere('size.id = :size')
            ->orderBy('posi.name')
            ->setParameter('type', $typeId)
            ->setParameter('size', $sizeId)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($data as $row) {
            $result[$row->id] = $row->uid;
        }

        return $result;
    }
}