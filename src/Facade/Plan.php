<?php

namespace Flubber\Extension\Facade;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\Database\Facade\BaseFacade;
use Flubber\Component\DiskBrowser\Facade\Directory;
use Flubber\Extension\Entity;
use Nette\Caching\IStorage;

class Plan extends BaseFacade
{
    private $directoryFacade;

    public function __construct(EntityManagerInterface $em, IStorage $storage = null)
    {
        parent::__construct($em, $storage);
        $this->directoryFacade = new Directory($em, $storage);
    }

    public function create(BaseEntity $entity, $throwException = false, $autoFlush = true)
    {
        if ($entity->root === null) {
            $directory = $this->directoryFacade->createRootDirectory("/");
            $entity->root = $this->directoryFacade->createDirectory($directory, (new \Flubber\Component\DiskBrowser\Entity\Directory())->setName("_images"));
        }
        return $this->update($entity, $throwException, $autoFlush);
    }

    public function insert(array $data, $throwExceptions = false)
    {
        $entity = new Entity\Plan;
        if ($data["typeId"] !== null)
            $entity->type = $this->em->getPartialReference(Entity\Type::class, $data["typeId"]);
        if (array_key_exists("sourcePlanId", $data))
            $entity->sourcePlan = $this->em->getPartialReference(Entity\Plan::class, $data["sourcePlanId"]);
        if (array_key_exists("positionId", $data) && $data["positionId"] !== null)
            $entity->position = $this->em->getPartialReference(Entity\Position::class, $data["positionId"]);
        $entity = $this->create($entity, $throwExceptions);
        $this->em->flush();
        foreach ($data["items"] as $item) {
            $item = (object)$item;
            $itemEntity = new Entity\PlanItem();
            $itemEntity->name = $item->name;
            $itemEntity->order = $item->order;
            if ($item->decreeId !== null)
                $itemEntity->decree = $this->em->getPartialReference(Entity\Decree::class, $item->decreeId);
            $itemEntity->watched = $item->watched;
            $itemEntity->controlType = $this->em->getPartialReference(Entity\ControlType::class, $item->controlTypeId);
            if (property_exists($item, "protocolId") && $item->protocolId !== null)
                $itemEntity->protocol = $this->em->getPartialReference(Entity\ProtocolTemplate::class, $item->protocolId);
            $itemEntity->plan = $entity;
            if (property_exists($item, "sourceItemId"))
                $itemEntity->sourceItem = $this->em->getPartialReference(Entity\PlanItem::class, $item->sourceItemId);
            $this->update($itemEntity, $throwExceptions, true);
        }

        $this->em->flush();

        return $entity;
    }

    public function getByType($typeId)
    {
        $entities = $this->getRepository()->findBy([
            "type" => $this->em->getPartialReference(Entity\Type::class, $typeId),
            "position" => null
        ]);
        if (count($entities) === 1)
            return $entities[0];
        return null;
    }

    public function getByTypeAndPosition($typeId, $positionId)
    {
        $entities = $this->getRepository()->findBy([
            "type" => $this->em->getPartialReference(Entity\Type::class, $typeId),
            "position" => $this->em->getPartialReference(Entity\Position::class, $positionId),
        ]);
        if (count($entities) === 1)
            return $entities[0];
        return null;
    }

    public function getByPosition($positionId)
    {
        $entities = $this->getRepository()->findBy([
            "position" => $this->em->getPartialReference(Entity\Position::class, $positionId),
        ]);
        if (count($entities) === 1)
            return $entities[0];
        return null;
    }

    public function getEntityClass()
    {
        return Entity\Plan::class;
    }

    public function findPairs($value, $orderBy = [], $key = null)
    {
        if (count($orderBy) === 0)
            $orderBy["name"] = "ASC";
        return parent::findPairs($value, $orderBy, $key);
    }

    public function copyPlan($id, $typeId, $positionId)
    {
        $source = $this->getById($id, true);
        $data = $source->toArray();
        $data["typeId"] = $typeId;
        $data["positionId"] = $positionId;
        $data["sourcePlanId"] = $source->getId();
        $items = [];
        foreach ($data["items"] as $item) {
            $item["sourceItemId"] = $item["id"];
            $items[] = $item;
        }
        $data["items"] = $items;
        return $this->insert($data);
    }
}