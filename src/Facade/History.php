<?php

namespace Flubber\Extension\Facade;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\Database\Facade\BaseFacade;
use Flubber\Component\DiskBrowser\Facade\Directory;
use Flubber\Component\Security\Entity\User;
use Flubber\Extension\Entity;
use Nette\Caching\IStorage;
use Nette\InvalidStateException;
use Tracy\Debugger;

class History extends BaseFacade
{
    /** @var Directory */
    private $directoryFacade;
    /** @var Plan */
    private $planFacade;

    public function __construct(EntityManagerInterface $em, IStorage $storage = null)
    {
        parent::__construct($em, $storage);
        $this->directoryFacade = new Directory($em, $storage);
        $this->planFacade = new Plan($em, $storage);
    }

    public function create(BaseEntity $entity, $throwException = false, $autoFlush = true)
    {
        return $this->update($entity, $throwException, $autoFlush);
    }

    public function insert(array $data, $throwExceptions = false)
    {
        $entity = new Entity\History;

        $date = \DateTime::createFromFormat("d-m-Y", $data["startAt"]);
        if (is_bool($date)) {
            $date = \DateTime::createFromFormat("Y-m-d", $data["startAt"]);
            if (is_bool($date))
                throw new InvalidStateException("Invalid date format");
        }
        $entity->setStartOfOrder($date);
        if (array_key_exists("endAt", $data) && $data["endAt"] !== null) {
            $date = \DateTime::createFromFormat("d-m-Y", $data["endAt"]);
            if (is_bool($date)) {
                $date = \DateTime::createFromFormat("Y-m-d", $data["endAt"]);
                if (is_bool($date))
                    throw new InvalidStateException("Invalid date format");
            }
            $entity->setEndOfOrder($date);
        }
        $entity->subject = $data["subject"];
        $entity->content = $data["content"];
        $entity->number = $data["number"];
        $entity->numberOffer = $data["numberOffer"];
        if ($data["userId"])
            $entity->user = $this->em->getPartialReference(User::class, $data["userId"]);
        $position = $this->em->getRepository(Entity\Position::class)->find($data["positionId"]);
        $entity->position = $position;
        $type =$position->sizeDirectory->type->getId();
        $plan = $this->planFacade->getByTypeAndPosition($type, $position->getId());
        if (!$plan)
            $plan = $this->planFacade->getByType($type);
        $entity->plan = $this->planFacade->copyPlan($plan->getId(), null, null);
        $entity->directory = $this->directoryFacade->createRootDirectory($entity->subject);
        return $this->create($entity, $throwExceptions);
    }


    public function getEntityClass()
    {
        return Entity\History::class;
    }

    private function getByPositionIdBuilder($id)
    {
        return $this->em->createQueryBuilder()
            ->from(Entity\History::class, 'hist', 'hist.id')->addSelect('hist')
            ->join('hist.position', 'posi')->addSelect('posi')
            ->join('hist.directory', 'dire')->addSelect('dire')
            ->where('posi.id = :position')
            ->setParameter('position', $id);
    }

    public function getByPositionId($id)
    {
        return $this->getByPositionIdBuilder($id)
            ->getQuery()
            ->getResult();
    }

    public function getByPositionIdFinished($id)
    {
        $qb = $this->getByPositionIdBuilder($id);
        return $qb->andWhere($qb->expr()->isNotNull('hist.endOfOrder'))
            ->orderBy('hist.endOfOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getByPositionIdNotFinished($id)
    {
        $qb = $this->getByPositionIdBuilder($id);
        return $qb->andWhere($qb->expr()->isNull('hist.endOfOrder'))
            ->orderBy('hist.startOfOrder', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getCountByPosition($id)
    {
        $count = $this->em->createQuery(<<<SQL
SELECT COUNT(hist.id) FROM Entity\History hist
INNER JOIN hist.position posi
WHERE posi.id = :position
SQL
        )
            ->setParameter('position', $id)
            ->getSingleResult();

        return (int)array_pop($count);
    }

    /**
     * @param integer $number
     * @return Entity\History[]
     */
    public function findByNumber($number)
    {
        return $this->em->createQueryBuilder()
            ->select('hist')
            ->from(Entity\History::class, 'hist')
            ->where("hist.number LIKE :number")
            ->setParameter('number', "%$number%")
            ->orderBy('hist.number')
            ->getQuery()
            ->getResult();
    }

    public function findByNumberOrOffer($value)
    {
        return $this->em->createQueryBuilder()
            ->select('hist')
            ->from(Entity\History::class, 'hist')
            ->where("hist.number LIKE :number")
            ->orWhere("hist.numberOffer LIKE :number")
            ->setParameter('number', "%$value%")
            ->orderBy('hist.number')
            ->getQuery()
            ->getResult();
    }
}