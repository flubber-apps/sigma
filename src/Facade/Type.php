<?php

namespace Flubber\Extension\Facade;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Component\Database\Entity\BaseEntity;
use Flubber\Component\Database\Facade\BaseFacade;
use Flubber\Component\DiskBrowser\Facade\Directory;
use Flubber\Extension\Entity;
use Nette\Caching\IStorage;

class Type extends BaseFacade
{
    private $directoryFacade;

    public function __construct(EntityManagerInterface $em, IStorage $storage = null)
    {
        parent::__construct($em, $storage);
        $this->directoryFacade = new Directory($em, $storage);
    }

    public function getEntityClass()
    {
        return Entity\Type::class;
    }

    /**
     * {@inheritdoc}
     * @param Entity\Type $entity
     */
    public function create(BaseEntity $entity, $throwException = false, $autoFlush = true)
    {
        if ($entity->root === null) {
            $directory = $this->directoryFacade->createRootDirectory("/");
            $entity->root = $this->directoryFacade->createDirectory($directory, (new \Flubber\Component\DiskBrowser\Entity\Directory())->setName("_images"));
        }
        return $this->update($entity, $throwException, $autoFlush);
    }

    public function insert(array $data, $throwExceptions = false)
    {
        $entity = new Entity\Type;
        $entity->name = $data["name"];
        $entity->kind = $this->em->getPartialReference(Entity\Kind::class, $data["kindId"]);
        $entity->producer = $this->em->getPartialReference(Entity\Producer::class, $data["producerId"]);

        return $this->create($entity, $throwExceptions);
    }


    public function getTypesListByKindProd($kindId, $prodId)
    {
        $qb = $this->em->createQueryBuilder();
        $data = $qb->addSelect('type')
            ->from(Entity\Type::class, 'type', 'type.id')
            ->join('type.kind', 'kind')
            ->join('type.producer', 'prod')
            ->where('kind.id = :kind')
            ->andWhere('prod.id = :prod')
            ->orderBy('type.name')
            ->setParameter('kind', $kindId)
            ->setParameter('prod', $prodId)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($data as $id => $type) {
            $result[$id] = $type->name;
        }

        return $result;
    }
}