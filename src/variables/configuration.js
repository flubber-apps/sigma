import EventEmitter from "events";

class Configuration extends EventEmitter {
  EVENT_REDIRECT = "EVENT_REDIRECT";
  defaultLang = "en";
  lang = "cs";
  apiUrl = "http://localhost:80/api";
  langMapper = require("../lang/mapper");
  redirectRoute = undefined;

  redirect(destination, source) {
    this.redirectRoute = {
      from: source,
      to: destination
    };
    this.emit("EVENT_REDIRECT");
  }

  getRedirect() {
    let data = this.redirectRoute;
    this.redirectRoute = undefined;
    return data;
  }
}

export default new Configuration();
