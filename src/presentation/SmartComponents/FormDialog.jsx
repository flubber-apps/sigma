import AbstractComponent from "./AbstractComponent";
import PropTypes from "prop-types";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputAdornment,
  TextField
} from "@material-ui/core";
import React from "react";
import Close from "@material-ui/icons/Close";
import Select from "./FormControls/Select";
import { SystemContext } from "../../variables/SystemContext";
import Button from "./Button";

let closeImg = { cursor: "pointer", float: "right", width: "20px" };

const styles = {
  formControl: {
    width: "100%"
  }
};

export default class FormDialog extends AbstractComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    controls: PropTypes.arrayOf(PropTypes.object).isRequired,
    buttons: PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    onChange: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func
  };
  static defaultProps = {
    controls: [],
    buttons: {},
    open: false
  };
  state = {
    open: false,
    controls: {}
  };

  constructor(props) {
    super(props);
    let _this = this;
    props.controls.map(control => {
      _this.state.controls[control.id] = control.value;
      return null;
    });
    this.onChangeCallback = props.onChange;
    this.onSubmitCallback = props.onSubmit;
    this.onCloseCallback = props.onClose;
    this.onOpenCallback = props.onOpen;
  }

  handleClose = () => {
    this.setState({ open: false });
    if (this.onCloseCallback) this.onCloseCallback();
  };
  handleOpen = () => {
    this.setState({ open: true });
    if (this.onOpenCallback) this.onOpenCallback();
  };
  handleSave = () => {
    if (this.onSubmitCallback !== undefined)
      this.onSubmitCallback(this.state.controls);
  };
  handleUpdate = (id, value) => {
    let data = Object.assign({}, this.state);
    Object.assign(data.controls, { [id]: value });
    this.setState(data);
    if (this.onChangeCallback !== undefined)
      this.onChangeCallback(this.state.controls);
  };

  generateFooter(language, buttons) {
    let result = [];
    if (buttons.save)
      result.push(
        <Button color="primary" onClick={this.handleSave} key="save">
          {language.button.save}
        </Button>
      );
    if (buttons.close)
      result.push(
        <Button color="transparent" onClick={this.handleClose} key="close">
          {language.button.close}
        </Button>
      );
    if (buttons.others)
      buttons.others.map(button => {
        result.push(
          <Button key={button.key}>{language.button[button.key]}</Button>
        );
        return null;
      });
    if (result.length) return <DialogActions>{result}</DialogActions>;
    else return null;
  }

  generateControl(control) {
    if (control.type === "select2") {

    } else if (control.type === "select") {
      return (
        <Select
          id={control.id}
          onChange={this.handleUpdate.bind(this, control.id)}
          classes={styles}
          options={control.options}
          label={control.label}
          required={control.required === true}
        />
      );
    } else if (control.type === "string-search") {
      return (
        <FormControl
          hidden={control.hidden === true}
          fullWidth
          key={control.id}
        >
          <TextField
            id={control.id}
            label={control.label}
            type="string"
            value={control.value}
            defaultValue={control.value}
            onChange={this.handleUpdate.bind(this, control.id)}
            shrink
            InputLabelProps={{
              shrink:
                (control.value !== undefined && control.value !== "") ||
                control.defaultValue !== undefined
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position={"end"}>
                  {control.render}
                </InputAdornment>
              )
            }}
          />
        </FormControl>
      );
    } else if (control.type === "date" || control.type === "time") {
      return (
        <FormControl
          hidden={control.hidden === true}
          fullWidth
          key={control.id}
        >
          <TextField
            id={control.id}
            type={control.type}
            label={control.label}
            InputLabelProps={{
              shrink: true
            }}
            required={control.required === true}
            defaultValue={control.value}
            onChange={(event) => this.handleUpdate(control.id, event.target.value)}
          />
        </FormControl>
      );
    } else if (
      control.type === "datetime-local" ||
      control.type === "datetime"
    ) {
      return (
        <FormControl
          hidden={control.hidden === true}
          fullWidth
          key={control.id}
        >
          <TextField
            id={control.id}
            type="datetime-local"
            label={control.label}
            InputLabelProps={{
              shrink: true
            }}
            required={control.required === true}
            defaultValue={control.value}
            onChange={(event) => this.handleUpdate(control.id, event.target.value)}
          />
        </FormControl>
      );
    } else if (control.type === "custom") {
      return control.render;
    } else {
      let style = undefined;
      if (control.hidden === true) style = { display: "none" };
      return (
        <FormControl
          style={style}
          hidden={control.hidden === true}
          fullWidth
          key={control.id}
        >
          <TextField
            error={control.error}
            id={control.id}
            required={control.required === true}
            label={control.label}
            defaultValue={control.value}
            onChange={event =>
              this.handleUpdate(control.id, event.target.value)
            }
            helperText={control.error ? control.errorMessage : null}
          />
        </FormControl>
      );
    }
  }

  render() {
    const { title, buttonValue, controls, buttons } = this.props;

    let value = buttonValue;
    if (!buttonValue) value = title;
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <>
              <Button color="primary" onClick={this.handleOpen}>
                {value}
              </Button>
              <Dialog open={this.state.open}>
                <DialogTitle>
                  {title}
                  <Close style={closeImg} onClick={this.handleClose}/>
                </DialogTitle>
                <DialogContent>
                  <form>
                    {controls.map(control => this.generateControl(control))}
                  </form>
                </DialogContent>
                {this.generateFooter(language, buttons)}
              </Dialog>
            </>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
