import React from "react";
// @material-ui/core
import AbstractComponent from "./AbstractComponent";
import Dialog from "./Dialog";
import { Button } from "@material-ui/core";
// core components
import PropTypes from "prop-types";
import { EventEmitter } from "events";

const styles = {
  progressWrapper: {
    height: "10px",
    marginTop: "10px",
    width: "400px",
    float: "left",
    overflow: "hidden",
    backgroundColor: "#f5f5f5",
    borderRadius: "4px",
    WebkitBoxShadow: "inset 0 1px 2px rgba(0,0,0,.1)",
    boxShadow: "inset 0 1px 2px rgba(0,0,0,.1)"
  },
  progressBar: {
    float: "left",
    width: "0",
    height: "100%",
    fontSize: "12px",
    lineHeight: "20px",
    color: "#fff",
    textAlign: "center",
    backgroundColor: "#337ab7",
    WebkitBoxShadow: "inset 0 -1px 0 rgba(0,0,0,.15)",
    boxShadow: "inset 0 -1px 0 rgba(0,0,0,.15)",
    WebkitTransition: "width .6s ease",
    transition: "width .6s ease"
  },
  cancelButton: {
    marginTop: "5px",
    WebkitAppearance: "none",
    padding: 0,
    cursor: "pointer",
    background: "0 0",
    border: 0,
    float: "left",
    fontSize: "21px",
    fontWeight: 700,
    lineHeight: 1,
    color: "#000",
    textShadow: "0 1px 0 #fff",
    filter: "alpha(opacity=20)",
    opacity: ".2"
  }
};

class FileUploadProgress extends React.Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    formGetter: PropTypes.func,
    formRenderer: PropTypes.func,
    progressRenderer: PropTypes.func,
    formCustomizer: PropTypes.func,
    beforeSend: PropTypes.func,
    onProgress: PropTypes.func,
    onLoad: PropTypes.func,
    onError: PropTypes.func,
    onAbort: PropTypes.func
  };
  static defaultProps = {
    progressRenderer: (progress, hasError, cancelHandler) => {
      if (hasError || progress > -1) {
        const barStyle = Object.assign({}, styles.progressBar);
        barStyle.width = `${progress}%`;

        let message = <span>Uploading ...</span>;
        if (hasError) {
          barStyle.backgroundColor = "#d9534f";
          message = (
            <span style={{ color: "#a94442" }}>Failed to upload ...</span>
          );
        }
        if (progress === 100) {
          message = <span>Successfully uploaded</span>;
        }

        return (
          <div className="_react_fileupload_progress_content">
            <div style={styles.progressWrapper}>
              <div
                className="_react_fileupload_progress_bar"
                style={barStyle}
              />
            </div>
            <button
              className="_react_fileupload_progress_cancel"
              style={styles.cancelButton}
              onClick={cancelHandler}
            >
              <span>&times;</span>
            </button>
            <div style={{ clear: "left" }}>{message}</div>
          </div>
        );
      }
      return "";
    },

    formCustomizer: form => form,
    beforeSend: request => request,
    onProgress: (e, request, progress) => {
    },
    onLoad: (e, request) => {
    },
    onError: (e, request) => {
    },
    onAbort: (e, request) => {
    }
  };

  constructor(props) {
    super(props);
    this.proxy = new EventEmitter();
    this.state = {
      progress: -1,
      hasError: false,
      file: undefined
    };
    this.refFrom = React.createRef();
  }

  cancelUpload() {
    this.proxy.emit("abort");
    this.setState({
      progress: -1,
      hasError: false
    });
  }

  handleUpload = event => {
    event.preventDefault();
    if (this.state.file === undefined) return;
    this.setState(
      {
        progress: 0,
        hasError: false,
        id: undefined
      },
      this._doUpload
    );
  };
  handleFile = event => {
    this.setState({ file: event.target.files[0] });
  };

  id() {
    let s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return s4() + s4() + "-" + s4() + s4() + "-" + s4() + s4();
  }

  render() {
    const progessElement = this.props.progressRenderer(
      this.state.progress,
      this.state.hasError,
      this.cancelUpload.bind(this)
    );

    return (
      <div>
        <div>
          <input type="file" name="file" onChange={this.handleFile}/>
        </div>
        <Button onClick={this.handleUpload}>Upload</Button>
        {progessElement}
      </div>
    );
  }

  _getFormData() {
    if (this.props.formGetter) {
      return this.props.formGetter();
    }
    return new FormData(this.refFrom.current);
  }

  _doUpload() {
    const form = this._getFormData();
    const req = new XMLHttpRequest();
    req.open("POST", this.props.url);

    req.addEventListener(
      "load",
      e => {
        this.proxy.removeAllListeners(["abort"]);
        const newState = { progress: 100 };
        if (req.status >= 200 && req.status <= 299) {
          this.setState(newState, () => {
            this.props.onLoad(e, req);
          });
        } else {
          newState.hasError = true;
          this.setState(newState, () => {
            this.props.onError(e, req);
          });
        }
      },
      false
    );

    req.addEventListener(
      "error",
      e => {
        this.setState(
          {
            hasError: true
          },
          () => {
            this.props.onError(e, req);
          }
        );
      },
      false
    );

    req.upload.addEventListener(
      "progress",
      e => {
        let progress = 0;
        if (e.total !== 0) {
          progress = parseInt((e.loaded / e.total) * 100, 10);
        }
        this.setState(
          {
            progress
          },
          () => {
            this.props.onProgress(e, req, progress);
          }
        );
      },
      false
    );

    req.addEventListener(
      "abort",
      e => {
        this.setState(
          {
            progress: -1
          },
          () => {
            this.props.onAbort(e, req);
          }
        );
      },
      false
    );

    this.proxy.once("abort", () => {
      req.abort();
    });

    form.append("name", this.state.file.name);
    form.append("size", this.state.file.size);
    form.append("mime-type", this.state.file.type);
    form.append("uid", this.id());
    form.append("countParts", 1);
    form.append("file", this.state.file);

    this.props.beforeSend(req).send(this.props.formCustomizer(form));
  }
}

export default class FileUpload extends AbstractComponent {
  static propTypes = {
    directoryId: PropTypes.number.isRequired,
    apiPath: PropTypes.string,
    onSuccess: PropTypes.func,
    onError: PropTypes.func,
    title: PropTypes.string,
    button: PropTypes.func
  };
  static defaultProps = {
    title: "Upload",
    button: props => {
      const { ...rest } = props;
      return <Button {...rest}>Upload</Button>;
    }
  };
  state = {
    open: false,
    files: []
  };

  constructor(props) {
    super(props);
    if (props.apiPath === undefined)
      this.state.apiPath = this._config.apiUrl + "/disk/file";
    else this.state.apiPath = props.apiPath;
    this.onSuccessCallback = props.onSuccess;
    this.onErrorCallback = props.onError;
    this.refDialog = React.createRef();
  }

  handleOpen = () => {
    this.refDialog.current.handleOpen();
  };
  handleClose = () => {
    this.refDialog.handleClose();
  };

  render() {
    const { directoryId, button, title } = this.props;
    /*const files = this.state.files.map(file => (
      <li key={file.name}>
        {file.name} - {file.size} bytes
      </li>
    ));*/
    return (
      <div>
        <Dialog
          innerRef={node => this.refDialog = node}
          fullScreen={false}
          button={button}
          title={title}

        >
          {/*<section>
            <Dropzone
              onDrop={this.onDrop.bind(this)}
              onFileDialogCancel={this.onCancel.bind(this)}
              onDragEnter={e => console.log(e)}
              onDragLeave={e => console.log(e)}
              onDragOver={e => console.log(e)}
            >
              {({ getRootProps, getInputProps }) => (
                <div {...getRootProps()}>
                  <input {...getInputProps()} />
                  <p>Vlož soubory</p>
                </div>
              )}
            </Dropzone>
            <aside>
              <h4>Files</h4>
              <ul>{files}</ul>
            </aside>
          </section>*/}
          <FileUploadProgress
            url={this.state.apiPath}
            onProgress={(e, request, progress) => {
              console.log("progress", e, request, progress);
            }}
            onLoad={(e, request) => {
              let response = JSON.parse(request.response);
              if (this.onSuccessCallback !== undefined)
                this.onSuccessCallback(response);
            }}
            onError={(e, request) => {
              let response = JSON.parse(request.response);
              if (this.onErrorCallback !== undefined)
                this.onErrorCallback(response);
            }}
            onAbort={(e, request) => {
              console.log("abort", e, request);
            }}
            formCustomizer={form => {
              form.append("directoryId", directoryId);
              return form;
            }}
          />
        </Dialog>
      </div>
    );
  }
}
