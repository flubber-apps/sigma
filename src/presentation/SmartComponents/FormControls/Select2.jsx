import React from "react";
import Select from "react-select";
import PropTypes from "prop-types";
import { emphasize } from "@material-ui/core/styles/colorManipulator";
import AbstractComponent from "../AbstractComponent";
import { MenuItem, Paper, Typography, TextField, withStyles } from "@material-ui/core";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  input: {
    display: "flex",
    padding: 0
  },
  valueContainer: {
    display: "flex",
    flexWrap: "wrap",
    flex: 1,
    alignItems: "center",
    overflow: "hidden"
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  chipFocused: {
    backgroundColor: emphasize(
      theme.palette.type === "light"
        ? theme.palette.grey[300]
        : theme.palette.grey[700],
      0.08
    )
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  singleValue: {
    fontSize: 16
  },
  placeholder: {
    position: "absolute",
    left: 2,
    fontSize: 16
  },
  paper: {
    position: "absolute",
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  divider: {
    height: theme.spacing.unit * 2
  }
});

class NoOptionsMessage extends AbstractComponent {
  render() {
    let props = this.props;
    return (
      <Typography
        color="textSecondary"
        className={props.selectProps.classes.noOptionsMessage}
        {...props.innerProps}
      >
        {props.children}
      </Typography>
    );
  }
}

function inputComponent({ inputRef, ...props }) {
  return <div ref={inputRef} {...props} />;
}

class Control extends AbstractComponent {
  render() {
    let props = this.props;
    return (
      <TextField
        fullWidth
        InputProps={{
          inputComponent,
          inputProps: {
            className: props.selectProps.classes.input,
            inputRef: props.innerRef,
            children: props.children,
            ...props.innerProps
          }
        }}
        {...props.selectProps.textFieldProps}
      />
    );
  }
}

class Option extends AbstractComponent {
  render() {
    let props = this.props;
    return (
      <MenuItem
        buttonRef={props.innerRef}
        selected={props.isFocused}
        style={{
          fontWeight: props.isSelected ? 500 : 400
        }}
        {...props.innerProps}
      >
        {props.children}
      </MenuItem>
    );
  }
}

class Placeholder extends AbstractComponent {
  render() {
    let props = this.props;
    return (
      <Typography
        color="textSecondary"
        className={props.selectProps.classes.placeholder}
        {...props.innerProps}
      >
        {props.children}
      </Typography>
    );
  }
}

class SingleValue extends AbstractComponent {
  render() {
    let props = this.props;
    return (
      <Typography
        className={props.selectProps.classes.singleValue}
        {...props.innerProps}
      >
        {props.children}
      </Typography>
    );
  }
}

class ValueContainer extends AbstractComponent {
  render() {
    let props = this.props;
    return (
      <div className={props.selectProps.classes.valueContainer}>
        {props.children}
      </div>
    );
  }
}

class Menu extends AbstractComponent {
  render() {
    let props = this.props;
    return (
      <Paper
        square
        className={props.selectProps.classes.paper}
        {...props.innerProps}
      >
        {props.children}
      </Paper>
    );
  }
}

class CustomSelect extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func,
    emptyValue: PropTypes.bool
  };

  constructor(props) {
    super(props);
    this.state.single = null;
    this.state.multi = null;
    this.onChangeCallback = props.onChange;
  }

  handleChangeSingle(value) {
    this.setState({ single: value });
    if (this.onChangeCallback !== undefined)
      this.onChangeCallback(value.value);
  }

  render() {
    const { classes, placeholder, options, emptyValue, ...rest } = this.props;
    let data = [];
    if (emptyValue === true)
      data.push({
        value: undefined,
        label: "-"
      });
    options.map(item => {
      data.push({
        value: item.id,
        label: item.name
      });
      return null;
    });

    return (
      <div className={classes.root}>
        <Select
          {...rest}
          classes={classes}
          options={data}
          components={{
            Control,
            Menu,
            NoOptionsMessage,
            Option,
            Placeholder,
            SingleValue,
            ValueContainer
          }}
          value={this.state.single}
          onChange={this.handleChangeSingle.bind(this)}
          placeholder={placeholder}
        />
      </div>
    );
  }
}

export default withStyles(styles)(CustomSelect);
