import React from "react";
import PropTypes from "prop-types";
import { Chip, withStyles } from "@material-ui/core";

import style from "../../../assets/jss/material-dashboard-react/components/chipStyle";

import AbstractComponent from "../AbstractComponent";
import Select from "./Select";

class SelectChip extends AbstractComponent {
  static defaultProps = {
    value: "",
    options: [],
    required: false,
    disabled: false,
    emptyValue: true,
    chipValues: [],
    error: false
  };
  static propTypes = {
    classes: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    error: PropTypes.bool,
    onChange: PropTypes.func,
    emptyValue: PropTypes.bool,
    options: PropTypes.arrayOf(PropTypes.object),
    chipValues: PropTypes.arrayOf(PropTypes.number),
    onSelect: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state.id = props.id;
    this.state.value = props.value;
    this.state.emptyValue = props.emptyValue;
    this.state.chipValues = props.chipValues;
    this.onCallbackChange = props.onChange;
    this.onCallbackSelect = props.onSelect;
  }

  handleChange = value => {
    let controlValues = this.state.chipValues;
    if (controlValues === undefined) controlValues = [];
    controlValues.push(value);
    this.setState({
      chipValues: controlValues
    });
    if (this.onCallbackChange) this.onCallbackChange(controlValues);
  };
  handleSelect = id => {
    if (this.onCallbackSelect)
      this.onCallbackSelect(
        this.props.options.filter(item => item.id === id)[0]
      );
  };
  handleDelete = id => {
    let values = this.state.chipValues.filter(item => item !== id);
    this.setState({
      chipValues: values
    });
    if (this.onCallbackChange) this.onCallbackChange(values);
  };

  generateOptions(classes, values) {
    return this.props.options
      .filter(item => values.indexOf(item.id) !== -1)
      .map(item => (
        <Chip
          key={item.id}
          className={classes.chip}
          onClick={this.handleSelect.bind(this, item.id)}
          onDelete={this.handleDelete.bind(this, item.id)}
          label={item.name}
          value=""
        />
      ));
  }

  render() {
    const { classes, options, chipValues, emptyValue, ...rest } = this.props;

    return (
      <div className={classes.root}>
        <Select
          {...rest}
          options={options.filter(
            item => this.state.chipValues.indexOf(item.id) === -1
          )}
          onChange={this.handleChange}
          alwaysEmpty
        />
        {this.generateOptions(classes, this.state.chipValues)}
      </div>
    );
  }
}

export default withStyles(style)(SelectChip);
