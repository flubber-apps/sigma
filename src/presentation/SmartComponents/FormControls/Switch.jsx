import React from "react";
import AbstractComponent from "../AbstractComponent";
import PropTypes from "prop-types";
import { FormControlLabel, Switch } from "@material-ui/core";

class CustomSwitch extends AbstractComponent {
  static defaultProps = {
    color: "primary"
  };
  static propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.string),
    color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
    onChange: PropTypes.func
  };
  constructor(props) {
    super(props);
    this.state = {
      value: false
    };
  }
  onChange() {
    this.setState({ value: !this.state.value });
    if (this.onChangeCallback !== undefined) {
      this.onChangeCallback({
        id: this.state.id,
        value: !this.state.value
      });
    }
  }
  render() {
    const { classes, id, label, options, color, onChange } = this.props;
    this.state.id = id;
    this.onChangeCallback = onChange;
    let getLabel = value => {
      return label + " (" + (value ? options[1] : options[0]) + ")";
    };
    return (
      <FormControlLabel
        className={classes.label}
        control={
          <Switch
            className={classes.control}
            defaultChecked={this.state.value}
            color={color}
            onChange={this.onChange.bind(this)}
          />
        }
        label={getLabel(this.state.value)}
      />
    );
  }
}

export default CustomSwitch;
