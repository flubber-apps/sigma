import React from "react";
import AbstractComponent from "../AbstractComponent";
import PropTypes from "prop-types";
import { FormControl, MenuItem, TextField } from "@material-ui/core";

class CustomSelect extends AbstractComponent {
  static defaultProps = {
    value: "",
    options: [],
    required: false,
    disabled: false
  };
  static propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    onChange: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state.id = props.id;
    this.state.value = props.value || false;
    this.onCallbackCange = props.onChange;
  }

  handleOnChange = event => {
    let key = event.target.value;
    this.setState({ value: key });
    if (this.onCallbackCange !== undefined) {
      this.onCallbackCange(key, this.state.id);
    }
  };

  render() {
    const {
      label,
      defaultValue,
      required,
      disabled,
      ...rest
    } = this.props;

    return (
      <FormControl style={{ width: "100%" }}>
        <TextField
          {...rest}
          select
          fullWidth
          id={this.state.id}
          label={label}
          required={required}
          value={this.state.value || ""}
          defaultValue={defaultValue}
          disabled={disabled === true}
          onChange={this.handleOnChange}
          InputLabelProps={{
            shrink: alwaysEmpty ? false : this.state.value !== ""
          }}
        >
          {this.generateOptions(options, emptyValue)}
        </TextField>
      </FormControl>
    );
  }
}

export default CustomSelect;
