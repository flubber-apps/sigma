import React from "react";

import config from "../../variables/configuration";

export default class AbstractComponent extends React.Component {
  state = {};
  _config = config;
  constructor(props, context) {
    super(props, context);
    if (new.target === AbstractComponent) {
      throw new TypeError(
        "Cannot construct " +
          AbstractComponent.name +
          " class instances directly. Class is abstract class."
      );
    }
  }
  getContent() {
    return this._config.langMapper.getContent(this._config.lang);
  }
}
