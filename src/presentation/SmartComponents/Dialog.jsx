import PropTypes from "prop-types";
import {
  AppBar,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Toolbar,
  Typography,
  withStyles
} from "@material-ui/core";
import React from "react";
import Close from "@material-ui/icons/Close";
import AbstractComponent from "./AbstractComponent";

import style from "../../assets/jss/material-dashboard-react/components/dialogStyle";

class CustomDialog extends AbstractComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    button: PropTypes.func.isRequired,
    displayTitle: PropTypes.bool,
    simpleTitle: PropTypes.bool,
    fullScreen: PropTypes.bool,
    actions: PropTypes.array,
    title: PropTypes.string,
    onClose: PropTypes.func,
    onOpen: PropTypes.func
  };
  static defaultProps = {
    displayTitle: true,
    simpleTitle: false,
    fullScreen: true,
    open: false,
    actions: []
  };

  constructor(props) {
    super(props);
    this.state.open = props.open;
    this.onCloseCallback = props.onClose;
    this.onOpenCallback = props.onOpen;
  }

  handleClose = () => {
    this.setState({ open: false });
    if (this.onCloseCallback) this.onCloseCallback();
  };
  handleOpen = () => {
    this.setState({ open: true });
    if (this.onOpenCallback) this.onOpenCallback();
  };

  render() {
    const {
      classes,
      title,
      button,
      simpleTitle,
      fullScreen,
      displayTitle,
      actions,
      children
    } = this.props;

    let titleContent = (display, simple) => {
      if (display)
        if (simple)
          return (
            <Toolbar className={classes.toolbar}>
              <Typography
                className={classes.simpleTitle}
                variant="title"
                style={{ flex: 1 }}
              >
                {title}
              </Typography>
              <IconButton onClick={this.handleClose}>
                <Close className={classes.simpleTitleIcon} />
              </IconButton>
            </Toolbar>
          );
        else
          return (
            <AppBar className={classes.appBar}>
              <Toolbar className={classes.toolbar}>
                <Typography
                  variant="title"
                  style={{ flex: 1 }}
                  className={classes.title}
                >
                  {title}
                </Typography>
                <IconButton
                  onClick={this.handleClose}
                  className={classes.titleIcon}
                >
                  <Close />
                </IconButton>
              </Toolbar>
            </AppBar>
          );
      else return null;
    };
    let dialogActions = buttons => {
      if (buttons.length) return <DialogActions>{buttons}</DialogActions>;
      return null;
    };

    return (
      <>
        {button({ onClick: this.handleOpen })}
        <Dialog open={this.state.open} fullScreen={fullScreen}>
          {titleContent(displayTitle, simpleTitle)}
          <DialogContent>{children}</DialogContent>
          {dialogActions(actions)}
        </Dialog>
      </>
    );
  }
}

export default withStyles(style)(CustomDialog);
