import React from "react";
import AbstractComponent from "./AbstractComponent";

import PropTypes from "prop-types";
import config from "../../variables/configuration";
import { withStyles } from "@material-ui/core/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  Tooltip
} from "@material-ui/core";
import styles from "../../assets/jss/material-dashboard-react/components/tableStyle";

class CustomTableHead extends AbstractComponent {
  static defaultProps = {
    data: [],
    optSortable: true,
    optDelay: 300,
    optSortBy: undefined,
    optSortDir: "asc",
    onChange: undefined,
    optLanguage: config.langMapper.getContent().dataTable
  };
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    optSortable: PropTypes.bool,
    optDelay: PropTypes.number,
    optSortBy: PropTypes.string,
    optSortDir: PropTypes.string,
    optLanguage: PropTypes.object,
    onChange: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      optSortable: props.optSortable,
      optDelay: props.optDelay,
      optSortBy: props.optSortBy,
      optSortDir: props.optSortDir,
      optLanguage: props.optLanguage
    };
    this.onChangeCallback = props.onChange;
  }

  generateHeaderCell(data) {
    let options = data.options || {};
    let generate = () => (
      <Tooltip
        title={this.state.optLanguage.body.toolTip}
        placement={options.placement || "bottom-start"}
        enterDelay={this.state.optDelay}
      >
        <TableSortLabel
          active={this.state.optSortBy === data.id}
          direction={this.state.optSortDir}
          onClick={this.handleOnClick(data.id)}
        >
          {data.name}
        </TableSortLabel>
      </Tooltip>
    );

    return (
      <TableCell
        key={data.id}
        sortDirection={
          this.state.optSortBy === data.id ? this.state.sortDir : null
        }
      >
        {this.state.optSortable && options.sortable !== false
          ? generate()
          : data.name}
      </TableCell>
    );
  }

  handleOnClick = id => () => {
    let dir = "asc";
    if (id === this.state.optSortBy) {
      dir = this.state.optSortDir === "asc" ? "desc" : "asc";
    }
    this.setState({ optSortBy: id, optSortDir: dir });
    if (this.onChangeCallback !== undefined) {
      this.onChangeCallback(id, dir);
    }
  };

  render() {
    const { data } = this.props;
    return (
      <TableHead>
        <TableRow>
          {data.map(col => {
            return this.generateHeaderCell(col);
          })}
        </TableRow>
      </TableHead>
    );
  }
}

class CustomTableBody extends AbstractComponent {
  static defaultProps = {
    onChange: undefined,
    onClick: undefined,
    page: 0,
    perPage: 10,
    optSortBy: undefined,
    optSortDir: "asc",
    pagination: true,
    data: []
  };
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    header: PropTypes.arrayOf(PropTypes.object).isRequired,
    keyRow: PropTypes.func.isRequired,
    pagination: PropTypes.bool,
    onChange: PropTypes.func,
    onSelectRow: PropTypes.func,
    page: PropTypes.number,
    perPage: PropTypes.number,
    optSortDir: PropTypes.string,
    optSortBy: PropTypes.string,
    selectedRow: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedRow: props.selectedRow,
      page: props.page,
      perPage: props.perPage,
      optSortBy: props.optSortBy,
      optSortDir: props.optSortDir,
      pagination: props.pagination
    };
    this.onChangeCallback = props.onChange;
    this.onSelectCallback = props.onSelectRow;
  }

  updatePagination = (page, perPage) => {
    this.setState({
      page,
      perPage
    });
  };
  updateSorting = (optSortBy, optSortDir) => {
    this.setState({
      optSortBy,
      optSortDir
    });
  };
  sort = (a, b) => {
    let filter = this.props.header.filter(
      item => item.id === this.state.optSortBy
    )[0];

    let _a = filter.access(a);
    let _b = filter.access(b);
    let ret = 1;
    switch (filter.type) {
      case "date":
        _a = filter.accessDate(a);
        _b = filter.accessDate(b);
        if (_a !== undefined && _b !== undefined && _a !== null && _b !== null) {
          if (this.state.optSortDir === "asc")
            return new Date(_a) > new Date(_b) ? 1 : -1;
          else return new Date(_a) > new Date(_b) ? -1 : 1;
        } else {
          if (_a === undefined || _a === null) ret *= -1;
          return this.state.optSortDir === "asc" ? ret : -ret;
        }
      case "number":
        if (_a === _b) return 0;
        if (_a > _b || _a === undefined) ret *= -1;
        return this.state.optSortDir === "asc" ? ret : -ret;
      case "string":
      default:
        if (_a !== undefined && _b !== undefined && _a !== null && _b !== null) {
          if (this.state.optSortDir === "asc")
            return _a.toString().localeCompare(_b.toString());
          else return _b.toString().localeCompare(_a.toString());
        } else {
          let ret = -1;
          if (_a === undefined || _a === null) ret *= -1;
          return this.state.optSortDir === "asc" ? ret : -ret;
        }
    }
  };
  sortData(data) {
    if (this.state.optSortBy === undefined) {
      return data;
    }
    return data.sort(this.sort);
  }

  handleSelectRow = id => {
    this.setState({ selectedRow: id });
    if (this.onSelectCallback !== undefined) this.onSelectCallback(id);
  };

  render() {
    const { data, header, keyRow } = this.props;
    let key = undefined;
    return (
      <TableBody>
        {this.sortData(data)
          .slice(
            this.state.page * this.state.perPage,
            (this.state.page + 1) * this.state.perPage
          )
          .map(row => {
            key = keyRow(row);
            const isSelected = key === this.state.selectedRow;
            return (
              <TableRow
                key={key}
                hover
                selected={isSelected}
                aria-checked={isSelected}
                onClick={this.handleSelectRow.bind(this, key)}
              >
                {header.map(col => {
                  return <TableCell key={col.id}>{col.access(row)}</TableCell>;
                })}
              </TableRow>
            );
          })}
      </TableBody>
    );
  }
}

class Datatable extends AbstractComponent {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    header: PropTypes.arrayOf(PropTypes.object),
    keyRow: PropTypes.func.isRequired,
    optSortable: PropTypes.bool,
    optSearch: PropTypes.bool,
    optSortBy: PropTypes.string,
    optSortDir: PropTypes.oneOf(["asc", "desc"]),
    optLanguage: PropTypes.object,
    page: PropTypes.number,
    perPage: PropTypes.oneOf([5, 10, 20, 25, 50, 100]),
    onSelectRow: PropTypes.func,
    onChange: PropTypes.func
  };
  static defaultProps = {
    header: [],
    optSortable: true,
    optSearch: false,
    optSortBy: undefined,
    optSortDir: "asc",
    optLanguage: {},
    perPage: 10,
    page: 0,
    onChange: undefined
  };

  constructor(props) {
    super(props);
    this.state = {
      optSortBy: props.optSortBy,
      optSortDir: props.optSortDir,
      optSearch: props.optSearch,
      optLanguage: props.optLanguage,
      page: props.page,
      perPage: props.perPage
    };
    this.onCallbackChange = props.onChange;
    this.bodyRef = React.createRef();
  }

  handleSort = (id, dir) => {
    this.setState({
      optSortBy: id,
      optSortDir: dir
    });
    this.bodyRef.current.updateSorting(id, dir);
  };
  handlePagination = (page, perPage) => {
    this.bodyRef.current.updatePagination(page, perPage);
  };
  handlePageChange = (event, page) => {
    this.setState({ page });
    this.handlePagination(page, this.state.perPage);
  };
  handlePerPageChange = event => {
    this.setState({ perPage: event.target.value });
    this.handlePagination(this.state.page, event.target.value);
  };

  render() {
    const { data, header, classes, keyRow, ...rest } = this.props;
    return (
      <div className={classes.root}>
        <div className={classes.wrapper}>
          <Table className={classes.table}>
            <CustomTableHead
              {...rest}
              classes
              data={header}
              onChange={this.handleSort}
            />
            <CustomTableBody
              {...rest}
              keyRow={keyRow}
              ref={this.bodyRef}
              data={data}
              header={header}
              optSortBy={this.state.optSortBy}
              optSortDir={this.state.optSortDir}
              page={this.state.page}
              perPage={this.state.perPage}
            />
          </Table>
        </div>
        <Table>
          <TableFooter>
            <TableRow>
              <TablePagination
                count={data.length}
                onChangePage={this.handlePageChange}
                onChangeRowsPerPage={this.handlePerPageChange}
                page={this.state.page}
                rowsPerPage={this.state.perPage}
                rowsPerPageOptions={[5, 10, 20, 25, 50, 100]}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </div>
    );
  }
}

export default withStyles(styles)(Datatable);
