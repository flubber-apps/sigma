import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import { withStyles, Snackbar, IconButton } from "@material-ui/core";
import Close from "@material-ui/icons/Close";

import styles from "../../assets/jss/material-dashboard-react/components/snackbarContentStyle";
import AbstractComponent from "./AbstractComponent";

class CustomSnackbar extends AbstractComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    message: PropTypes.node.isRequired,
    color: PropTypes.oneOf(["info", "success", "warning", "danger", "primary"]),
    close: PropTypes.bool,
    icon: PropTypes.func,
    place: PropTypes.oneOf(["tl", "tr", "tc", "br", "bl", "bc"]),
    open: PropTypes.bool,
    onClose: PropTypes.func,
    duration: PropTypes.number
  };
  static defaultProps = {
    place: "tl",
    onClose: () => {},
    duration: 6000
  };
  constructor(props) {
    super(props);
    this.timer = -1;
    this.state.open = props.open;
  }
  componentDidMount() {
    this.timer = setInterval(this.handleClose, this.props.duration);
  }
  componentWillUnmount() {
    if (this.timer !== -1)
      clearInterval(this.timer);
    this.timer = -1;
  }

  handleClose = () => {
    if (this.timer !== -1)
      clearInterval(this.timer);
    this.timer = -1;
    this.setState({open: false});
    this.props.onClose();
  };

  render() {
    const { classes, message, color, close, icon, place } = this.props;
    let action = [];
    const messageClasses = classNames({
      [classes.iconMessage]: icon !== undefined
    });
    if (close !== undefined) {
      action = [
        <IconButton
          className={classes.iconButton}
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={() => this.props.onClose()}
        >
          <Close className={classes.close} />
        </IconButton>
      ];
    }
    return (
      <Snackbar
        anchorOrigin={{
          vertical: place.indexOf("t") === -1 ? "bottom" : "top",
          horizontal:
            place.indexOf("l") !== -1
              ? "left"
              : place.indexOf("c") !== -1
                ? "center"
                : "right"
        }}
        open={this.state.open}
        message={
          <div>
            {icon !== undefined ? (
              <this.props.icon className={classes.icon} />
            ) : null}
            <span className={messageClasses}>{message}</span>
          </div>
        }
        action={action}
        ContentProps={{
          classes: {
            root: classes.root + " " + classes[color],
            message: classes.message
          }
        }}
      />
    );
  }
}

export default withStyles(styles)(CustomSnackbar);
