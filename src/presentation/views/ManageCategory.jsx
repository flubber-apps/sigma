import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import dashboardStyle from "../../assets/jss/material-dashboard-react/views/dashboardStyle";
import AbstractView from "./AbstractView";
import CardFooter from "../components/Card/CardFooter";
import Filter from "../SmartComponents/Filter";
import { SystemContext } from "../../variables/SystemContext";
import Card from "../components/Card/Card";
import CardBody from "../components/Card/CardBody";
import Datatable from "../SmartComponents/Datatable";
import KindStore from "../../stores/KindStore";
import ProducerStore from "../../stores/ProducerStore";
import TypeStore from "../../stores/TypeStore";
import SizeStore from "../../stores/SizeStore";
import WorkspaceStore from "../../stores/WorkspaceStore";
import PositionStore from "../../stores/PositionStore";
import Kind from "../Forms/Kind";
import Producer from "../Forms/Producer";
import Type from "../Forms/Type";
import Size from "../Forms/Size";
import Workspace from "../Forms/Workspace";
import Position from "../Forms/Position";
import { Button, Paper, withStyles } from "@material-ui/core";
import CrudCategory from "../../actions/CrudCategory";
import ActionCategory from "../../variables/sigma";
import AssignSize from "../components/AssignSize";
import AssignUser from "../components/AssignUser";
import CardHeader from "../components/Card/CardHeader";

class ManageCategory extends AbstractView {
  state = {
    selected: null,
    updated: false,
    data: [],
    columns: []
  };
  store = {
    kind: KindStore,
    producer: ProducerStore,
    type: TypeStore,
    size: SizeStore,
    workspace: WorkspaceStore,
    position: PositionStore
  };

  selectType = response => {
    if (this.state.selected !== null && this.state.selected !== undefined)
      this.store[this.state.selected].removeChangeListener(this.updateTable);
    if (response["select-type-of-category"] !== undefined)
      this.store[response["select-type-of-category"]].addChangeListener(
        this.updateTable
      );

    this.updateTable(response["select-type-of-category"], true);
  };
  updateTable = (category, afterSelectType) => {
    let type = category;
    if (type === undefined && afterSelectType !== true)
      type = this.state.selected;
    let data = [],
      columns = [];

    switch (type) {
      case "kind":
        data = KindStore.getAll();
        columns = [
          {
            id: "kind",
            access: row => row.name,
            name: this.getContent().text.kind
          },
          {
            id: "actions",
            access: row => (
              <Button
                color={"primary"}
                variant={"outlined"}
                onClick={this.deleteRow.bind(this, row.id)}
              >
                {this.getContent().button.delete}
              </Button>
            )
          }
        ];
        break;
      case "producer":
        data = ProducerStore.getAll();
        columns = [
          {
            id: "producer",
            access: row => row.name,
            name: this.getContent().text.producer
          },
          {
            id: "actions",
            access: row => {
              return (
                <div>
                  <Button
                    color={"primary"}
                    variant={"outlined"}
                    onClick={this.deleteRow.bind(this, row.id)}
                  >
                    {this.getContent().button.delete}
                  </Button>
                </div>
              );
            }
          }
        ];
        break;
      case "type":
        data = TypeStore.getAll();
        columns = [
          {
            id: "kind",
            access: row => row.kind.name,
            name: this.getContent().text.kind
          },
          {
            id: "producer",
            access: row => row.producer.name,
            name: this.getContent().text.producer
          },
          {
            id: "type",
            access: row => row.name,
            name: this.getContent().text.type
          },
          {
            id: "actions",
            access: row => {
              return (
                <div>
                  <Button
                    color={"primary"}
                    variant={"outlined"}
                    onClick={this.deleteRow.bind(this, row.id)}
                  >
                    {this.getContent().button.delete}
                  </Button>
                  <AssignSize type={row} />
                </div>
              );
            }
          }
        ];
        break;
      case "size":
        data = SizeStore.getAll();
        columns = [
          {
            id: "size",
            access: row => row.name,
            name: this.getContent().text.size
          },
          {
            id: "actions",
            access: row => {
              return (
                <div>
                  <Button
                    color={"primary"}
                    variant={"outlined"}
                    onClick={this.deleteRow.bind(this, row.id)}
                  >
                    {this.getContent().button.delete}
                  </Button>
                </div>
              );
            }
          }
        ];
        break;
      case "workspace":
        data = WorkspaceStore.getAll();
        columns = [
          {
            id: "workspace",
            access: row => row.name,
            name: this.getContent().text.workspace
          },
          {
            id: "actions",
            access: row => {
              return (
                <div>
                  <Button
                    color={"primary"}
                    variant={"outlined"}
                    onClick={this.deleteRow.bind(this, row.id)}
                  >
                    {this.getContent().button.delete}
                  </Button>
                  <AssignUser workspace={row} />
                </div>
              );
            }
          }
        ];
        break;
      case "position":
        data = PositionStore.getAll();

        columns = [
          {
            id: "size",
            access: row => row.size.name,
            name: this.getContent().text.sizeName
          },
          {
            id: "workspace",
            access: row => row.workspace.name,
            name: this.getContent().text.workspaceName
          },
          {
            id: "name",
            access: row => row.name,
            name: this.getContent().text.positionName
          },
          {
            id: "uid",
            access: row => row.uid,
            name: this.getContent().text.positionUid
          },
          {
            id: "serialNumber",
            access: row => row.serialNumber,
            name: this.getContent().text.positionSerialNumber
          },
          {
            id: "actions",
            access: row => {
              return (
                <div>
                  <Button
                    color={"primary"}
                    variant={"outlined"}
                    onClick={this.deleteRow.bind(this, row.id)}
                  >
                    {this.getContent().button.delete}
                  </Button>
                </div>
              );
            }
          }
        ];
        break;
      default:
        break;
    }
    this.setState({ selected: type, data: data, columns: columns });
  };
  deleteRow = id => {
    switch (this.state.selected) {
      case "kind":
        CrudCategory.deleteById(ActionCategory.KIND_DELETE, id);
        break;
      case "producer":
        CrudCategory.deleteById(ActionCategory.PRODUCER_DELETE, id);
        break;
      case "type":
        CrudCategory.deleteById(ActionCategory.TYPE_DELETE, id);
        break;
      case "size":
        CrudCategory.deleteById(ActionCategory.SIZE_DELETE, id);
        break;
      case "workspace":
        CrudCategory.deleteById(ActionCategory.WORKSPACE_DELETE, id);
        break;
      case "position":
        CrudCategory.deleteById(ActionCategory.POSITION_DELETE, id);
        break;
      default:
        break;
    }
  };

  generateTable(classes, translate) {
    if (this.state.selected) {
      return (
        <Paper className={classes.root}>
          <Datatable
            data={this.state.data}
            header={this.state.columns}
            optSortable={true}
            optLanguage={translate.dataTable}
            keyRow={row => row.id}
          />
        </Paper>
      );
    } else return null;
  }

  generateFooter() {
    switch (this.state.selected) {
      case "kind":
        return <Kind onChange={this.updateTable} />;
      case "producer":
        return <Producer onChange={this.updateTable} />;
      case "type":
        return <Type onChange={this.updateTable} />;
      case "size":
        return <Size onChange={this.updateTable} />;
      case "workspace":
        return <Workspace onChange={this.updateTable} />;
      case "position":
        return <Position onChange={this.updateTable} />;
      default:
        return null;
    }
  }

  render() {
    const { classes } = this.props;

    let filters = translate => [
      {
        id: "select-type-of-category",
        name: translate.text.manageOfCategory,
        options: {
          filter: "select",
          options: [
            "kind",
            "producer",
            "type",
            "size",
            "workspace",
            "position"
          ].map(item => {
            return {
              id: item,
              name: translate.text[item]
            };
          })
        }
      }
    ];
    return (
      <>
        {super.render()}
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <Card plain>
              <CardHeader>
                <Filter
                  filters={filters(language)}
                  onChange={this.selectType}
                />
              </CardHeader>
              <CardBody>{this.generateTable(classes, language)}</CardBody>
              <CardFooter>{this.generateFooter(classes, language)}</CardFooter>
            </Card>
          );
        }}
      </SystemContext.Consumer>
        </>
    );
  }
}

ManageCategory.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(ManageCategory);
