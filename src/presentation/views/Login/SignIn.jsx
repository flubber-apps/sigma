import React from "react";

import SignInComponent from "../../components/LoginPaper";
import AbstractView from "../AbstractView";

class SignIn extends AbstractView {
  render() {
    return (
      <>
        {super.render()}
        <SignInComponent />
      </>
    );
  }
}

export default SignIn;
