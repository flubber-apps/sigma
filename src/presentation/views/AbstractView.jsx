import React from "react";
import { Redirect } from "react-router-dom";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import Configuration from "../../variables/configuration";

export default class AbstractView extends AbstractComponent {
  constructor(props, context) {
    super(props, context);
    this.state.redirect = false;
    this.state.redirectRoute = undefined;
  }

  componentDidMount() {
    Configuration.addListener(Configuration.EVENT_REDIRECT, this.handleRedirect);
  }

  componentWillUnmount() {
    Configuration.removeListener(Configuration.EVENT_REDIRECT, this.handleRedirect);
  }

  handleRedirect = () => {
    let route = Configuration.getRedirect();
    if (route !== undefined) this.setState({redirect: true, redirectRoute: route });
  };

  needRedirect() {
    return this.state.redirect === true && this.state.redirectRoute !== undefined && this.state.redirectRoute.to !== this.props.location.pathname;
  }
  render() {
    if (this.needRedirect()) {
      return (
        <Redirect
          to={this.state.redirectRoute.to}
          from={this.state.redirectRoute.from}
          push={true}
        />
      );
    }
  }
}
