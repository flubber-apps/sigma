import React from "react";
import PropTypes from "prop-types";
// core components
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";

import dashboardStyle from "../../../assets/jss/material-dashboard-react/views/dashboardStyle";
import Button from "../../SmartComponents/Button";
import Select from "../../SmartComponents/FormControls/Select";
import Textarea from "@material-ui/core/es/InputBase/Textarea";
import {
  Grid,
  InputLabel,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
  withStyles
} from "@material-ui/core";
import AbstractView from "../AbstractView";
import { SystemContext } from "../../../variables/SystemContext";
import Configuration from "../../../variables/configuration";

import FileUpload from "../../SmartComponents/FileUpload";
import Image from "material-ui-image/lib/index";
import DirectoryStore from "../../../stores/DirectoryStore";
import HistoryStore from "../../../stores/HistoryStore";
import CardFooter from "../../components/Card/CardFooter";
import UrlStore from "../../../stores/UrlStore";
import Dialog from "../../SmartComponents/Dialog";
import OrderPlan from "../../components/OrderPlan";
import UserStore from "../../../stores/UserStore";

class Detail extends AbstractView {
  state = {
    type: {},
    size: {},
    positionId: undefined,
    redirect: false,
    destination: undefined
  };

  constructor(props, context) {
    super(props, context);
    this.state.edit = false;
    this.state.user = {};
    this.state.historyId = parseInt(props.match.params.id);
    this.state.history = {};
    this.state.position = {};
    this.state.images = [];
  }

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateUser);
    HistoryStore.addChangeListener(this.updateHistory);
    DirectoryStore.addChangeListener(this.updateImages);
    this.updateUser();
    this.updateHistory();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateUser);
    HistoryStore.removeChangeListener(this.updateHistory);
    DirectoryStore.removeChangeListener(this.updateImages);
  }

  updateUser = () => {
    this.setState({
      user: UserStore.getUser()
    });
  };
  updateHistory = () => {
    let history = HistoryStore.getById(parseInt(this.state.historyId));
    let type = (history.position || {}).type || {};
    this.setState({
      positionId: (history.position || {}).id,
      history: history,
      position: history.position,
      images: DirectoryStore.getFilesById(type.directoryId)
    });
  };
  updateImages = () => {
    let type = (this.state.position || {}).type || {};
    if (type.id !== undefined)
      this.setState({
        images: DirectoryStore.getFilesById(type.directoryId)
      });
  };

  handleDisplayDevice = () => {
    Configuration.redirect(
      UrlStore.generateUrl("Front", "DeviceDetail", {
        type: this.state.position.type.id,
        size: this.state.position.size.id,
        position: this.state.position.id
      })
    );
  };
  handleChangeContent = event => {
    let history = this.state.history;
    history.content = event.target.value;
    HistoryStore.update(this.state.historyId, history);
    this.setState({ history: history });
  };
  handleChangeState = value => {
    let history = this.state.history;
    history.state = value;
    HistoryStore.update(this.state.historyId, history);
    this.setState({ history: history });
  };
  handleSaveContent = () => {
    HistoryStore.update(this.state.historyId, this.state.history);
  };
  handleEdit = () => {
    this.setState({ edit: true });
  };
  handleSave = () => {
    this.setState({ edit: false });
  };

  generateHeaderData = (classes, language) => {
    let position = this.state.position || {
      uid: "-",
      serialNumber: "-",
      name: "-"
    };
    let type = position.type || {};
    let kind = position.kind || {};
    let producer = position.producer || {};
    let size = position.size || { name: "-" };
    let workspace = position.workspace || { name: "-" };
    let content = ({ ...props }) => (
      <Textarea
        rowsMax={27}
        value={this.state.history.content || ""}
        style={{
          width: "100%",
          height: 50,
          borderStyle: "solid",
          borderRadius: 5,
          borderColor: "gray"
        }}
        {...props}
      />
    );
    let state = language => {
      if (
        (this.state.history.state === "opened" ||
          this.state.history.state === "approved") &&
        (this.state.user.roles || []).filter(
          item => item.name === "admin" || item.name === "superadmin"
        ).length > 0
      ) {
        return (
          <TableRow>
            <TableCell>{language.history.operation}</TableCell>
            <TableCell>
              <Select
                label=""
                id="operation"
                options={[
                  { id: "approved", name: language.state.approve },
                  { id: "canceled", name: language.state.cancel },
                  { id: "finished", name: language.state.finish }
                ]}
                onChange={this.handleChangeState}
              />
            </TableCell>
          </TableRow>
        );
      } else return null;
    };
    let generateState = language => {
      switch (this.state.history.state) {
        case "canceled":
        case "approved":
        case "finished":
        case "opened":
          return language.state[this.state.history.state];
        default:
          return "";
      }
    };
    return (
      <Grid item>
        <Card plain>
          <CardBody>
            <Grid container justify="center" spacing={16} alignContent="center">
              <Grid item key="specifications">
                <Paper className={classes.paper}>
                  <Grid container spacing={8}>
                    <Grid item>
                      <Table className={classes.table}>
                        <TableBody>
                          <TableRow>
                            <TableCell>{language.text.kind}</TableCell>
                            <TableCell>{kind.name}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.text.producer}</TableCell>
                            <TableCell>{producer.name}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.text.type}</TableCell>
                            <TableCell>{type.name}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.text.size}</TableCell>
                            <TableCell>{size.name}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.text.positionUid}</TableCell>
                            <TableCell>{position.uid}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.text.positionName}</TableCell>
                            <TableCell>{position.name}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>
                              {language.text.positionSerialNumber}
                            </TableCell>
                            <TableCell>{position.serialNumber}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.text.workspace}</TableCell>
                            <TableCell>{workspace.name}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.history.state}</TableCell>
                            <TableCell>{generateState(language)}</TableCell>
                          </TableRow>
                          {state(language)}
                        </TableBody>
                      </Table>
                    </Grid>
                    <Grid item>
                      <Table className={classes.table}>
                        <TableBody>
                          <TableRow>
                            <TableCell>{language.history.number}</TableCell>
                            <TableCell>{this.state.history.number}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.history.startAt}</TableCell>
                            <TableCell>{this.state.history.startAt}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>{language.history.endAt}</TableCell>
                            <TableCell>
                              {this.state.history.endAt || "-"}
                            </TableCell>
                          </TableRow>
                          <TableRow />
                          <TableRow>
                            <TableCell colSpan={2}>
                              <Button
                                block={true}
                                color={"primary"}
                                onClick={this.handleDisplayDevice}
                              >
                                {language.menu.tlp.short}
                              </Button>
                            </TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell colSpan={2}>
                              <Button block={true} color={"primary"}>
                                Kusovník
                              </Button>
                            </TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell colSpan={2}>
                              <Button block={true} color={"primary"}>
                                Výkres
                              </Button>
                            </TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </Grid>
                  </Grid>
                  {this.state.images.length === 0 && type.directoryId !== undefined ? (
                    <FileUpload
                      directoryId={type.directoryId}
                      onUpload={this.updateImages}
                    />
                  ) : null}
                </Paper>
              </Grid>
              <Grid item key="image">
                {this.state.images
                  .filter(file => /^image/.test(file.mimeType))
                  .map(image => {
                    return (
                      <Image
                        key={image.uid}
                        animationDuration={100}
                        src={
                          this._config.apiUrl + "/disk/file?uid=" + image.uid
                        }
                        style={{ width: "400px" }}
                        alt="Logo"
                      />
                    );
                  })}
              </Grid>
            </Grid>
          </CardBody>
          <CardFooter>
            <Paper
              style={{
                width: "100%",
                padding: 10
              }}
            >
              <InputLabel>{language.history.content}</InputLabel>
              <Dialog
                fullScreen
                title={language.history.content}
                button={content}
                onClose={this.handleSaveContent}
              >
                <Textarea
                  value={this.state.history.content}
                  onChange={this.handleChangeContent}
                  style={{
                    width: "inherit",
                    height: 750,
                    borderStyle: "solid",
                    borderRadius: 5,
                    borderColor: "gray"
                  }}
                />
              </Dialog>
            </Paper>
          </CardFooter>
        </Card>
      </Grid>
    );
  };
  generatePlanHeader = language => {
    if (this.state.edit) return language.historyDetail.partPlanConfigure;
    else return language.historyDetail.partPlan;
  };

  render() {
    let type = (this.state.position || {}).type || {};
    const { classes } = this.props;
    return (
      <>
        {super.render()}
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            return (
              <Grid container direction={"column"}>
                {this.generateHeaderData(classes, language)}
                <Grid item>
                  <Card plain>
                    <CardHeader color="primary">
                      {this.generatePlanHeader(language)}
                    </CardHeader>
                    <CardBody>
                      {this.state.positionId !== undefined ? (
                        <OrderPlan
                          type={type.id}
                          editable={this.state.history.state === "opened"}
                          closed={
                            this.state.history.state === "finished" ||
                            this.state.history.state === "canceled"
                          }
                          position={this.state.position.id}
                          history={this.state.historyId}
                          onEdit={this.handleEdit}
                          onSave={this.handleSave}
                        />
                      ) : null}
                    </CardBody>
                  </Card>
                </Grid>
                {/*<GridItem xs={12} sm={12} md={12}>
                  <Card plain>
                    <CardHeader color="primary">
                      <h4 className={classes.cardTitleWhite}>Plán kontrol</h4>
                    </CardHeader>
                    <CardBody>
                      <Paper>
                        <Table>
                          <TableHead>
                            <TableRow>
                              <TableCell>ID</TableCell>
                              <TableCell>Název</TableCell>
                              <TableCell>Vytvořil/Provedl</TableCell>
                              <TableCell>Datum</TableCell>
                              <TableCell/>
                            </TableRow>
                          </TableHead>
                          <TableBody>
                            <TableRow>
                              <TableCell>1</TableCell>
                              <TableCell>kontrola pracoviště</TableCell>
                              <TableCell>Břetislav Novotný</TableCell>
                              <TableCell>2018-06-02</TableCell>
                              <TableCell/>
                            </TableRow>
                            <TableRow>
                              <TableCell>2</TableCell>
                              <TableCell>Měření teplot</TableCell>
                              <TableCell>-</TableCell>
                              <TableCell>-</TableCell>
                              <TableCell>
                                <Button>Vygenerovat</Button>
                                <Button>Odevzdat</Button>
                              </TableCell>
                            </TableRow>
                          </TableBody>
                        </Table>
                      </Paper>
                    </CardBody>
                  </Card>
                </GridItem>*/}
              </Grid>
            );
          }}
        </SystemContext.Consumer>
        </>
    );
  }
}

Detail.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Detail);
