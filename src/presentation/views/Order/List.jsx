import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import { Paper, withStyles } from "@material-ui/core";
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle";

import Configuration from "../../../variables/configuration";

import KindStore from "../../../stores/KindStore";
import HistoryStore from "../../../stores/HistoryStore";
import UrlStore from "../../../stores/UrlStore";
import Datatable from "../../SmartComponents/Datatable";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import Filter from "../../SmartComponents/Filter";
import CardBody from "../../components/Card/CardBody";
import { SystemContext } from "../../../variables/SystemContext";
import CardFooter from "../../components/Card/CardFooter";

import History from "../../Forms/History";
import AbstractView from "../AbstractView";
import UserStore from "../../../stores/UserStore";
import WorkspaceStore from "../../../stores/WorkspaceStore";

class List extends AbstractView {
  constructor(props, context) {
    super(props, context);
    this.state.kindList = [];
    this.state.historyList = [];
    this.state.enableWorkspaceFilter = false;
    this.state.value = {
      kind: undefined,
      workspace: undefined,
      user: () => false,
      state: row => row.state === "opened" || row.state === "approved"
    };
    this.updateUser(true);
  }

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateUser);
    HistoryStore.addChangeListener(this.updateUser);
    KindStore.addChangeListener(this.updateKind);
    HistoryStore.addChangeListener(this.updateHistory);
    this.updateKind();
    this.updateHistory();
    this.updateUser();
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateUser);
    HistoryStore.removeChangeListener(this.updateUser);
    KindStore.removeChangeListener(this.updateKind);
    HistoryStore.removeChangeListener(this.updateHistory);
  }

  updateUser = () => {
    let validator = () => false;
    let user = UserStore.getUser();
    let display = false;
    if (user.id !== undefined) {
      if (user.roles.filter(role => role.name === "user").length > 0) {
        let workspaces = WorkspaceStore.getAll();
        validator = row => {
          let workspace = row.workspace || {};
          return workspaces.filter(item => workspace.id === item.id && item.users.filter(userId => userId === user.id) > 0).length > 0;
        };
        display = true;
      } else {
        validator = () => true;
      }
      let values = Object.assign({}, this.state.value);
      values.user = validator;
      this.setState({enableWorkspaceFilter: display, value: values});
    }
  };
  updateKind = () => {
    this.setState({ kindList: KindStore.getAll() });
  };
  updateHistory = () => {
    this.setState({ historyList: HistoryStore.getAll() });
  };
  handleUpdateFilter = payload => {
    this.setState({
      value: {
        kind:
          payload.kind === undefined
            ? () => true
            : row => row.kind.id === payload.kind,
        state:
          payload.state !== undefined && payload.state.value !== false
            ? () => true
            : row => row.state === "opened" || row.state === "approved",
        user: this.state.value.user,
        workspace: row => payload.workspace === undefined ? () => true : (row.workspace || {}).id === payload.workspace
      }
    });
  };
  handleNewHistory = response => {
    Configuration.redirect(
      UrlStore.generateUrl("Front", "OrderDetail", { id: response.id })
    );
  };
  handleDisplayOrder = id => {
    Configuration.redirect(
      UrlStore.generateUrl("Front", "OrderDetail", { id }),
      UrlStore.getPath("Front", "OrderList")
    );
  };

  header(language, forFilter = true) {
    let data = [
      {
        id: "kind",
        name: language.text.kind,
        access: row => row.kind.name,
        options: {
          filter: "select2",
          options: this.state.kindList
        }
      },
      {
        id: "producer",
        access: row => row.producer.name,
        name: language.text.producer
      },
      {
        id: "number_order",
        access: row => row.numberOffer,
        name: language.text.orderNumber
      },
      {
        id: "position",
        access: row => {
          let data = [];
          if (row.position.uid !== null) data.push(row.position.uid);
          if (row.position.serialNumber !== null)
            data.push(row.position.serialNumber);
          return data.join("/");
        },
        name: language.text.position
      }
    ];
    if (forFilter) {
      if (this.state.enableWorkspaceFilter) {
        let user = UserStore.getUser();
        data.push({
          id: "workspace",
          access: row => row.workspace.name,
          name: language.text.workspace,
          options: {
            filter: "select",
            options: WorkspaceStore.getAll().filter(workspace => workspace.users.filter(userId => user.id === userId).length > 0)
          }
        });
      }
      data.push({
        id: "state",
        access: row => language.state[row.state],
        name: language.history.state,
        options: {
          filter: "bool-switch",
          options: [language.state.current, language.state.all]
        }
      });
    } else {
      data.push({
        id: "startDate",
        access: row => row.startAt,
        name: language.history.startAt
      });
      data.push({
        id: "state",
        access: row => language.state[row.state],
        name: language.history.state
      });
    }
    return data;
  }
  render() {
    const { classes } = this.props;
    return (
      <>
        {super.render()}
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            return (
              <Card plain>
                <CardHeader color="primary">
                  <Filter
                    filters={this.header(language)}
                    onChange={this.handleUpdateFilter}
                  />
                </CardHeader>
                <CardBody>
                  <Paper className={classes.root}>
                    <Datatable
                      data={Filter.applyFilter(
                        this.state.historyList,
                        this.state.value
                      )}
                      keyRow={row => row.id}
                      header={this.header(language, false)}
                      optLanguage={language.dataTable}
                      optSortable={true}
                      onSelectRow={this.handleDisplayOrder}
                    />
                  </Paper>
                </CardBody>
                <CardFooter>
                  <History
                    onChange={this.updateHistory}
                    onSubmit={this.handleNewHistory}
                  />
                </CardFooter>
              </Card>
            );
          }}
        </SystemContext.Consumer>
      </>
    );
  }
}

List.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(List);
