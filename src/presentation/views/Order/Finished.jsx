import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "../../components/Grid/GridItem";
import GridContainer from "../../components/Grid/GridContainer";
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle";
import CardFooter from "../../components/Card/CardFooter";
import Table from "@material-ui/core/es/Table/Table";
import Paper from "@material-ui/core/es/Paper/Paper";
import TableBody from "@material-ui/core/es/TableBody/TableBody";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import Button from "@material-ui/core/es/Button/Button";
import TableFooter from "@material-ui/core/es/TableFooter/TableFooter";
import Textarea from "@material-ui/core/es/InputBase/Textarea";
import TableHead from "@material-ui/core/es/TableHead/TableHead";

class Finished extends React.Component {
  state = {
    value: 0
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card plain>
              <CardBody>
                <GridContainer
                  justify="center"
                  spacing={16}
                  alignContent="center"
                >
                  <GridItem key="specifications">
                    <Paper className={classes.paper}>
                      <Table className={classes.table}>
                        <TableBody>
                          <TableRow>
                            <TableCell>Druh</TableCell>
                            <TableCell>čerpadlo</TableCell>
                            <TableCell>Číslo zakázky</TableCell>
                            <TableCell>DV16020222</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Výrobce</TableCell>
                            <TableCell>SIGMA GROUP</TableCell>
                            <TableCell>Datum otevření</TableCell>
                            <TableCell>2016-02-02</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Typ</TableCell>
                            <TableCell>CHM</TableCell>
                            <TableCell>Datum ukončení</TableCell>
                            <TableCell>2016-02-28</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Velikost</TableCell>
                            <TableCell>200</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Projektové označení</TableCell>
                            <TableCell>1RY10</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Název technologického systému</TableCell>
                            <TableCell>Odluh</TableCell>
                            <TableCell />
                            <TableCell>
                              <Button>TLP</Button>
                            </TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Výrobní číslo</TableCell>
                            <TableCell>Kusovník</TableCell>
                            <TableCell />
                            <TableCell>
                              <Button>Kusovník</Button>
                            </TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Pracoviště</TableCell>
                            <TableCell>ETE</TableCell>
                            <TableCell />
                            <TableCell>
                              <Button>Výkres</Button>
                            </TableCell>
                          </TableRow>
                        </TableBody>
                        <TableFooter>
                          <TableRow>
                            <TableCell colSpan={4}>
                              <Textarea
                                value="Nekonečná poznámka"
                                style={{
                                  width: 700,
                                  height: 45,
                                  borderStyle: "solid",
                                  borderRadius: 5,
                                  borderColor: "gray"
                                }}
                              />
                            </TableCell>
                          </TableRow>
                        </TableFooter>
                      </Table>
                    </Paper>
                  </GridItem>
                  <GridItem key="image">
                    <img alt="Logo" height="100%" />
                  </GridItem>
                </GridContainer>
              </CardBody>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={12}>
            <Card plain>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Plán kontrol</h4>
              </CardHeader>
              <CardBody>
                <Paper>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>ID</TableCell>
                        <TableCell>Název</TableCell>
                        <TableCell>Vytvořil/Provedl</TableCell>
                        <TableCell>Datum</TableCell>
                        <TableCell />
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell>1</TableCell>
                        <TableCell>kontrola pracoviště</TableCell>
                        <TableCell>Břetislav Novotný</TableCell>
                        <TableCell>2018-06-02</TableCell>
                        <TableCell />
                      </TableRow>
                      <TableRow>
                        <TableCell>2</TableCell>
                        <TableCell>měření teplot</TableCell>
                        <TableCell>Břetislav Novotný</TableCell>
                        <TableCell>2018-06-02</TableCell>
                        <TableCell>
                          <Button>Otevřit protokol</Button>
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                  <CardFooter>
                    <Button>Zobrazit plán</Button>
                  </CardFooter>
                </Paper>
              </CardBody>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

Finished.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Finished);
