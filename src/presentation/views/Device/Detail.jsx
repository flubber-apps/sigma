import React from "react";
import PropTypes from "prop-types";
// core components
import Card from "../../components/Card/Card";
import CardHeader from "../../components/Card/CardHeader";
import CardBody from "../../components/Card/CardBody";

import style from "../../../assets/jss/material-dashboard-react/components/tableStyle";
import TypeStore from "../../../stores/TypeStore";
import SizeStore from "../../../stores/SizeStore";
import AssignSizeStore from "../../../stores/AssignSizeStore";
import PositionStore from "../../../stores/PositionStore";
import AbstractView from "../AbstractView";
import {
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
  withStyles
} from "@material-ui/core";
import FileUpload from "../../SmartComponents/FileUpload";
import { SystemContext } from "../../../variables/SystemContext";
import Datatable from "../../SmartComponents/Datatable";
import DirectoryStore from "../../../stores/DirectoryStore";
import OrderPlan from "../../components/OrderPlanEdit";
import HistoryStore from "../../../stores/HistoryStore";
import ReactApexChart from "react-apexcharts";
import Button from "../../SmartComponents/Button";
import AbstractComponent from "../../SmartComponents/AbstractComponent";

class LineChart extends AbstractComponent {
  static propTypes = {
    options: PropTypes.object.isRequired,
    series: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
    this.state.options = {
      chart: {
        toolbar: {
          show: true
        }
      },
      colors: ["#77B6EA", "#545454", "#1A75FF", "#3333FF"],
      stroke: {
        curve: "straight"
      },
      grid: {
        row: {
          colors: ["#000000", "transparent"],
          opacity: 0.05
        }
      },
      markers: {
        size: 6
      }
    };
  }

  render() {
    const { options, series } = this.props;
    let opt = Object.assign({}, this.state.options, options);
    return (
      <div id="chart">
        <ReactApexChart
          options={opt}
          series={series}
          type="line"
          height="350"
        />
      </div>
    );
  }
}

class Detail extends AbstractView {
  constructor(props) {
    super(props);
    let parts = props.match.params.id.split("_");

    this.state.typeId = parseInt(parts[0]);
    if (!Number.isNaN(parseInt(parts[1])))
      this.state.sizeId = parseInt(parts[1]);
    else this.state.sizeId = undefined;
    if (!Number.isNaN(parseInt(parts[2])))
      this.state.positionId = parseInt(parts[2]);
    else this.state.positionId = undefined;

    this.state.type = {};
    this.state.size = {};
    this.state.position = {};
    this.state.historyList = [];
    this.state.images = [];
    this.state.chartDefault = undefined;
    this.state.charts = {};
  }

  componentDidMount() {
    super.componentDidMount();
    TypeStore.addChangeListener(this.updateType);
    SizeStore.addChangeListener(this.updateSize);
    AssignSizeStore.addChangeListener(this.updateSizeOfType);
    PositionStore.addChangeListener(this.updatePosition);
    HistoryStore.addChangeListener(this.updateHistory);
    PositionStore.addChangeListener(this.updateHistory);
    DirectoryStore.addChangeListener(this.updateType);
    this.updateType();
    this.updateSize();
    this.updatePosition();
    this.updateHistory();
    if (this.state.positionId !== undefined)
      PositionStore.getState(this.state.positionId, this.updateChart);
  }

  componentWillUnmount() {
    super.componentWillUnmount();
    TypeStore.removeChangeListener(this.updateType);
    SizeStore.removeChangeListener(this.updateSize);
    AssignSizeStore.removeChangeListener(this.updateSizeOfType);
    PositionStore.removeChangeListener(this.updatePosition);
    HistoryStore.removeChangeListener(this.updateHistory);
    PositionStore.removeChangeListener(this.updateHistory);
    DirectoryStore.removeChangeListener(this.updateType);
  }

  updateChart = data => {
    if (this.state.chartDefault === undefined) {
      let keys = Object.keys(data).sort();
      let chartDefault = undefined;
      if (keys.length) chartDefault = keys[0];
      this.setState({ chartDefault: chartDefault, charts: data });
    } else this.setState({ chartDefault: undefined, charts: data });
  };
  updateImages = () => {
  };
  updateType = () => {
    let type = TypeStore.getById(this.state.typeId);
    this.setState({ type: type, images:  DirectoryStore.getFilesById(type.directoryId)});
  };
  updateSize = () => {
    if (this.state.sizeId !== undefined)
      this.setState({ size: SizeStore.getById(this.state.sizeId) });
  };
  updateSizeOfType = () => {
    //TODO For extension ...
  };
  updatePosition = () => {
    if (this.state.positionId !== undefined)
      this.setState({ position: PositionStore.getById(this.state.positionId) });
  };
  updateHistory = () => {
    if (this.state.positionId !== undefined)
      this.setState({historyList: HistoryStore.getByPositionId(this.state.positionId)});
  };
  handleFileUpload = () => {
    this.updateImages();
  };
  handleChangeCharts = type => {
    if (type !== this.state.chartDefault) this.setState({ chartDefault: type });
  };

  generateHeaderData = (classes, language) => {
    let type = this.state.type || {};
    let kind = type.kind || {};
    let producer = type.producer || {};
    let size = { name: "-" };
    if (this.state.sizeId !== undefined) size = this.state.size;
    let position = { uid: "-", serialNumber: "-", name: "-" };
    if (this.state.positionId !== undefined) position = this.state.position;
    let workspace = position.workspace || { name: "-" };
    return (
      <Grid item>
        <Card plain>
          <CardBody>
            <Grid container justify="center" spacing={16} alignContent="center">
              <Grid item key="specifications">
                <Paper className={classes.paper}>
                  <Table className={classes.table}>
                    <TableBody>
                      <TableRow>
                        <TableCell>{language.text.kind}</TableCell>
                        <TableCell>{kind.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.text.producer}</TableCell>
                        <TableCell>{producer.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.text.type}</TableCell>
                        <TableCell>{type.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.text.size}</TableCell>
                        <TableCell>{size.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.text.positionUid}</TableCell>
                        <TableCell>{position.uid}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.text.positionName}</TableCell>
                        <TableCell>{position.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>
                          {language.text.positionSerialNumber}
                        </TableCell>
                        <TableCell>{position.serialNumber}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell>{language.text.workspace}</TableCell>
                        <TableCell>{workspace.name}</TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
                  {type.directoryId !== undefined && this.state.images.length === 0 ? (
                    <FileUpload
                      directoryId={type.directoryId}
                      onUpload={this.updateImages}
                    />
                  ) : null}
                </Paper>
              </Grid>
              <Grid item key="image">
                <Grid
                  container
                  justify={"center"}
                  alignItems={"center"}
                  style={{ height: "100%" }}
                >
                  {this.state.images
                    .filter(file => /^image/.test(file.mimeType))
                    .map(image => {
                      return (
                        <Grid item key={image.uid}>
                          <img
                            src={this._config.apiUrl + "/disk/file?uid=" + image.uid}
                            style={{ width: "400px" }}
                            alt="Logo"
                          />
                        </Grid>
                      );
                    })}
                </Grid>
              </Grid>
            </Grid>
          </CardBody>
        </Card>
      </Grid>
    );
  };
  generateHistory = (classes, language) => {
    if (this.state.positionId === undefined) return null;
    return (
      <Grid item>
        <Card plain>
          <CardHeader color="primary">
            {language.deviceDetail.partHistory}
          </CardHeader>
          <CardBody>
            <Paper>
              <Datatable
                keyRow={row => row.id}
                header={[
                  {
                    id: "number",
                    name: language.history.number,
                    access: row => row.number
                  },
                  {
                    id: "subject",
                    name: language.history.subject,
                    access: row => row.subject
                  },
                  {
                    id: "startAt",
                    name: language.history.startAt,
                    access: row => row.startAt
                  },
                  {
                    id: "state",
                    name: language.history.state,
                    access: row => language.state[row.state]
                  }
                ]}
                data={this.state.historyList}
                optSortDir="desc"
                optSortBy="startAt"
                optLanguage={language.dataTable}
              />
            </Paper>
          </CardBody>
        </Card>
      </Grid>
    );
  };
  generateStateOfDevice = (classes, language) => {
    if (this.state.positionId === undefined) return null;
    let chart = this.state.charts[this.state.chartDefault] || {};
    let options = chart.options || {};
    let series = chart.series || [];
    let types = [];
    for (let type in this.state.charts) {
      let temp = this.state.charts[type];
      types.push(
        <Grid item>
          <Button
            onClick={this.handleChangeCharts.bind(this, type)}
            color={type !== this.state.chartDefault ? "default" : "primary"}
          >
            {temp.name}
          </Button>
        </Grid>
      );
    }
    return (
      <Grid item>
        <Card plain>
          <CardHeader color="primary">
            {language.deviceDetail.partState}
          </CardHeader>
          <CardBody>
            <Paper className={classes.paper}>
              <Grid
                container
                justify="center"
                spacing={16}
                alignContent="center"
              >
                <Grid item key="list">
                  <Grid
                    container
                    direction={"column"}
                    justify={"center"}
                    spacing={8}
                    alignContent={"center"}
                  >
                    {types}
                  </Grid>
                </Grid>
                <Grid item>
                  <LineChart options={options} series={series} />
                </Grid>
              </Grid>
            </Paper>
          </CardBody>
        </Card>
      </Grid>
    );
  };

  render() {
    const { classes } = this.props;
    return (
      <>
        {super.render()}
        <SystemContext.Consumer>
          {({ lang, langMapper }) => {
            let language = langMapper.getContent(lang);
            return (
              <Grid container direction={"column"}>
                {this.generateHeaderData(classes, language)}
                {this.generateStateOfDevice(classes, language)}
                <Grid item>
                  <Card plain>
                    <CardHeader color="primary">
                      {language.deviceDetail.partPlan}
                    </CardHeader>
                    <CardBody>
                      <OrderPlan
                        type={this.state.typeId}
                        position={this.state.positionId}
                      />
                    </CardBody>
                  </Card>
                </Grid>
                {this.generateHistory(classes, language)}
              </Grid>
            );
          }}
        </SystemContext.Consumer>
      </>
    );
  }
}

Detail.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(style)(Detail);
