import React from "react";
import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";

import dashboardStyle from "../../../assets/jss/material-dashboard-react/views/dashboardStyle";

import Configuration from "../../../variables/configuration";
import AbstractView from "../AbstractView";
import UrlStore from "../../../stores/UrlStore";
import DeviceSearch from "../../components/DeviceSearch";

class List extends AbstractView {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  handleDisplayDevice = id => {
    Configuration.redirect(
      UrlStore.generateUrl("Front", "DeviceDetail", { id })
    );
  };

  render() {
    return (
      <>
        {super.render()}
        <DeviceSearch onSelect={this.handleDisplayDevice} />
      </>
    );
  }
}

export default withStyles(dashboardStyle)(List);
