import React from "react";
import PropTypes from "prop-types";
import { withStyles} from "@material-ui/core";

const style = {
  root: {
    paddingTop: "20vh",
    textAlign: "center"
  },
  title: {
    fontSize: "80px"
  }
};

class Simple extends React.PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };
  render() {
    const { classes } = this.props;

    return <div className={classes.root}>
      <h1 className={classes.title}>404</h1>
      <p>Sorry, the page you were trying to view does not exist.</p>
    </div>
  }
}

export default withStyles(style)(Simple);
