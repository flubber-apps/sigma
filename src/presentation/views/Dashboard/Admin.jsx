import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import AbstractView from "../AbstractView";

import styles from "../../../assets/jss/material-dashboard-react/views/dashboardStyle";

class Front extends AbstractView {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };
  render() {
    const { classes } = this.props;
    return <div className={classes.root}>ssss{super.render()}</div>;
  }
}

export default withStyles(styles)(Front);
