import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core";
import styles from "../../assets/jss/material-dashboard-react/layouts/dashboardStyle";

import { SystemContext } from "../../variables/SystemContext";
import Configuration from "../../variables/configuration";

class Layout extends PureComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired
  };

  render() {
    const { classes } = this.props;

    return (
      <SystemContext.Provider value={Configuration}>
        <div className={classes.content}>
          <div className={classes.container} ref="mainPanel">
            {this.props.children}
          </div>
        </div>
      </SystemContext.Provider>
    );
  }
}

export default withStyles(styles)(Layout);
