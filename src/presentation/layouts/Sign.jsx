import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router-dom";

import withStyles from "@material-ui/core/styles/withStyles";
import dashboardStyle from "../../assets/jss/material-dashboard-react/layouts/dashboardStyle";

import AbstractLayout from "./AbstractLayout";
import config from "../../variables/configuration";
import { SystemContext } from "../../variables/SystemContext";
import UrlStore from "../../stores/UrlStore";

import SignIn from "../views/Login/SignIn";
import UserStore from "../../stores/UserStore";

class Sign extends AbstractLayout {
  constructor(props, contenxt) {
    super(props, contenxt);
    this.state.user = {};
  }

  componentDidMount() {
    super.componentDidMount();
    UserStore.addChangeListener(this.updateUser);
    this.updateUser();
  }
  componentWillUnmount() {
    super.componentWillUnmount();
    UserStore.removeChangeListener(this.updateUser);
  }

  updateUser = () => {
    let user = UserStore.getUser();
    if (user.id !== undefined)
      config.redirect(UrlStore.getPath("Front", "Dashboard"));
  };

  render() {
    const { classes } = this.props;
    return (
      <SystemContext.Provider value={config}>
        <div className={classes.content}>
          <div className={classes.container}>
            <Switch>
              <Route strict={true} path={UrlStore.getPath("Sign", "SignIn")} component={SignIn} />
            </Switch>
          </div>
        </div>
      </SystemContext.Provider>
    );
  }
}

Sign.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Sign);
