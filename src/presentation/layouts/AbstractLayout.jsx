import React from "react";

export default class AbstractLayout extends React.Component {
  constructor(props) {
    if (new.target === AbstractLayout) {
      throw new TypeError(
        "Cannoct construct Empty class instances directly. Empty is abstract class."
      );
    }

    super(props);
    this.state = {
      lang: "en",
      mobileOpen: false
    };
    this.resizeFunction = this.resizeFunction.bind(this);
  }
  getLanguage = () => {
    return this.laguages.getContent(this.state.lang);
  };
  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  resizeFunction() {
    if (window.innerWidth >= 960) {
      this.setState({ mobileOpen: false });
    }
  }
  componentDidMount() {
    if (navigator.platform.indexOf("Win") > -1) {
      //const ps = new PerfectScrollbar(this.refs.mainPanel);
    }
    window.addEventListener("resize", this.resizeFunction);
  }
  componentDidUpdate(e) {
    /* FIXME if (e.history.location.pathname !== e.location.pathname) {
      this.refs.mainPanel.scrollTop = 0;
      if (this.state.mobileOpen) {
        this.setState({ mobileOpen: false });
      }
    }*/
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeFunction);
  }

  
}
