/* eslint-disable */
import React from "react";
import PropTypes from "prop-types";
import { Redirect, Route, Switch } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import Sidebar from "../components/Sidebar/Sidebar";

import routes from "../../routes/front.js";

import dashboardStyle from "../../assets/jss/material-dashboard-react/layouts/dashboardStyle";

import image from "../../assets/img/background.jpg";
import logo from "../../assets/img/reactlogo.png";
import AbstractLayout from "./AbstractLayout";
import { SystemContext } from "../../variables/SystemContext";
import config from "../../variables/configuration";

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect push from={prop.path} to={prop.to} key={key}/>;
      return <Route exact={prop.exact || false} strict={prop.strict || false} path={prop.path} component={prop.component} key={key}/>;
    })}
  </Switch>
);

class App extends AbstractLayout {
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className={classes.root}>
        <SystemContext.Provider value={config}>
          <Sidebar
            routes={routes}
            logoText={"SIGMA GROUP"}
            logo={logo}
            image={image}
            handleDrawerToggle={this.handleDrawerToggle}
            open={this.state.mobileOpen}
            color="blue"
            {...rest}
          />
          <div className={classes.mainPanel} ref="mainPanel">
            <Header
              routes={routes}
              handleDrawerToggle={this.handleDrawerToggle}
              {...rest}
            />
            <main className={classes.content}>
              {switchRoutes}
            </main>
            <Footer/>
          </div>
        </SystemContext.Provider>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(App);
