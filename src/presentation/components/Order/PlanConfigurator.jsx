import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import FormControl from "@material-ui/core/es/FormControl/FormControl";
import Select from "@material-ui/core/Select";
// core components
import Table from "@material-ui/core/Table";
import FormControlLabel from "@material-ui/core/es/FormControlLabel/FormControlLabel";
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import InputLabel from "@material-ui/core/es/InputLabel/InputLabel";
import TableRow from "@material-ui/core/es/TableRow/TableRow";
import TableCell from "@material-ui/core/es/TableCell/TableCell";
import CheckBox from "@material-ui/core/es/internal/svg-icons/CheckBox";
import TableBody from "@material-ui/core/es/TableBody/TableBody";
import Paper from "@material-ui/core/es/Paper/Paper";
import TableHead from "@material-ui/core/es/TableHead/TableHead";
import Button from "@material-ui/core/es/Button/Button";
import Input from "@material-ui/core/es/Input/Input";
import Toolbar from "@material-ui/core/es/Toolbar/Toolbar";
import Tooltip from "@material-ui/core/es/Tooltip/Tooltip";
const styles = theme => ({});
class PlanConfigurator extends React.Component {
  state = {
    controls: []
  };
  createControl = (id, next, type, vyhl, watch, name) => {
    const classes = this.props.classes;
    return (
      <TableRow>
        <TableCell className={classes.tableCell} align="right">
          {id}
        </TableCell>
        <TableCell className={classes.tableCell} padding="none" align="right">
          {next}
        </TableCell>
        <TableCell className={classes.tableCell} padding="none" align="right">
          {type}
        </TableCell>
        <TableCell className={classes.tableCell} padding="none" align="right">
          {vyhl}
        </TableCell>
        <TableCell className={classes.tableCell} padding="none" align="right">
          {watch === true ? "Ano" : "Ne"}
        </TableCell>
        <TableCell className={classes.tableCell} padding="none" align="right">
          {name}
        </TableCell>
      </TableRow>
    );
  };
  displayRow = () => {
    const { classes } = this.props;
    return (
      <TableRow>
        <TableCell>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="ID">ID</InputLabel>
            <Select
              inputProps={{
                id: "ID",
                name: "ID"
              }}
            >
              <MenuItem key="1">1</MenuItem>
              <MenuItem key="2">20</MenuItem>
              <MenuItem key="3">30</MenuItem>
            </Select>
          </FormControl>
        </TableCell>
        <TableCell padding="none" align="right">
          -
        </TableCell>
        <TableCell padding="none" align="right">
          <FormControl>
            <InputLabel htmlFor="Typ">Typ kontroly</InputLabel>
            <Select
              inputProps={{
                id: "Typ",
                name: "Typ kontroly"
              }}
              style={{ width: 110 }}
            >
              <MenuItem value={1}>Psss</MenuItem>
              <MenuItem value={2}>V</MenuItem>
              <MenuItem value={3}>PK</MenuItem>
            </Select>
          </FormControl>
        </TableCell>
        <TableCell padding="none" align="right">
          <FormControl>
            <InputLabel htmlFor="vyhl">Vyhláška</InputLabel>
            <Select
              inputProps={{
                id: "vyhl",
                name: "Vyhláška"
              }}
              style={{ width: 85 }}
            >
              <MenuItem key="1" selected={true}>
                ABC-97
              </MenuItem>
              <MenuItem key="2">A-1996</MenuItem>
              <MenuItem key="3">30</MenuItem>
            </Select>
          </FormControl>
        </TableCell>
        <TableCell padding="checkbox" align="right">
          <FormControlLabel
            control={<CheckBox checked={false} value="watched" />}
            label=""
          />
        </TableCell>
        <TableCell padding="none" align="right">
          <FormControl>
            <InputLabel htmlFor="name" inputProps={{ name: "Název" }} />
            <Input name="name" />
          </FormControl>
        </TableCell>
        <TableCell>
          <Button>Přidat</Button>
        </TableCell>
      </TableRow>
    );
  };

  render() {
    const { classes, tableHeaderColor } = this.props;
    /* const rows = this.state.controls.map(filter => {
      const options = filter.options.map(option => {
        return <MenuItem key={option.id}>{option.value}</MenuItem>;
      });
    });*/
    return (
      <div className={classes.tableResponsive}>
        <Paper className={classes.root}>
          <form>
            <Table className={classes.table}>
              <TableHead className={classes[tableHeaderColor + "TableHeader"]}>
                <Toolbar>
                  <Tooltip title="Filter list">
                    <Button>Upravit plán</Button>
                  </Tooltip>
                </Toolbar>
                <TableRow>
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                  >
                    ID
                  </TableCell>
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    padding="none"
                    align="right"
                  >
                    Další krok
                  </TableCell>
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    padding="none"
                    align="right"
                  >
                    Typ kontroly
                  </TableCell>
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    padding="none"
                    align="right"
                  >
                    Vyhláška
                  </TableCell>
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    padding="none"
                    align="right"
                  >
                    Sledovat
                  </TableCell>
                  <TableCell
                    className={classes.tableCell + " " + classes.tableHeadCell}
                    padding="none"
                    align="right"
                  >
                    Název
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.createControl(
                  1,
                  2,
                  "V",
                  "",
                  false,
                  "kontrola pracoviště"
                )}
                {this.createControl(
                  2,
                  "-    ",
                  "P",
                  "ABC-97",
                  true,
                  "Měření teplot"
                )}
              </TableBody>
            </Table>
          </form>
        </Paper>
      </div>
    );
  }
}

PlanConfigurator.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(PlanConfigurator);
