import React from "react";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// core components
import tableStyle from "../../../assets/jss/material-dashboard-react/components/tableStyle.jsx";
import AbstractComponent from "../../SmartComponents/AbstractComponent";

class CustomTable extends AbstractComponent {
  generateRow(headerRow, row) {
    return (
      <TableRow key={row.id}>
        {Object.keys(headerRow).map(key => {
          return <TableCell key={key}>{row[key]}</TableCell>;
        })}
      </TableRow>
    );
  }
  render() {
    const {
      classes,
      head: tableHead,
      data: tableData,
    } = this.props;
    let header = null;
    if (tableHead !== undefined && tableHead.length > 0) {
      header = tableHead;
    }
    return (
      <Table className={classes.table}>
        {tableHead !== undefined ? (
          <TableHead>
            <TableRow>
              {Object.keys(tableHead).map(key => {
                return (
                  <TableCell key={key} align="right">
                    {tableHead[key].name}
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
        ) : null}
        <TableBody>
          {tableData.map(row => {
            return header !== null && header !== undefined
              ? this.generateRow(header, row)
              : this.generateRow(row, row);
          })}
        </TableBody>
      </Table>
    );
  }
}

CustomTable.defaultProps = {
  headerColor: "gray",
  data: []
};

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired,
  headerColor: PropTypes.oneOf([
    "warning",
    "primary",
    "danger",
    "success",
    "info",
    "rose",
    "gray"
  ]),
  head: PropTypes.arrayOf(PropTypes.string),
  data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  selectable: PropTypes.bool
};

export default withStyles(tableStyle)(CustomTable);
