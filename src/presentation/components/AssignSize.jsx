import AbstractComponent from "../SmartComponents/AbstractComponent";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Typography } from "@material-ui/core";
import React from "react";
import Close from "@material-ui/icons/Close";
import { SystemContext } from "../../variables/SystemContext";
import DataTable from "../SmartComponents/Datatable";
import CrudCategory from "../../actions/CrudCategory";
import ActionCategory from "../../variables/sigma";
import AssignSizeStore from "../../stores/AssignSizeStore";
import Form from "../Forms/AssignSize";

let closeImg = { cursor: "pointer", float: "right", width: "20px" };

const styles = theme => ({
  formControl: {
    width: "100%"
  }
});

class AssignSize extends AbstractComponent {
  static propTypes = {
    type: PropTypes.object.isRequired,
    onChange: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func
  };
  state = {
    open: false,
    type: null,
    data: []
  };

  constructor(props) {
    super(props);
    this.state.type = props.type;
    this.state.data = AssignSizeStore.getAllByType(this.state.type.id);
    this.onCloseCallback = props.onClose;
    this.onOpenCallback = props.onOpen;
  }

  componentWillMount() {
    AssignSizeStore.addChangeListener(this.handleUpdate);
  }

  componentWillUnmount() {
    AssignSizeStore.removeChangeListener(this.handleUpdate);
  }

  handleUpdate = () => {
    this.setState({ data: AssignSizeStore.getAllByType(this.state.type.id) });
  };
  handleOpen = () => {
    this.setState({ open: true });
    this.handleUpdate();
    if (this.onOpenCallback) this.onOpenCallback();
  };
  handleClose = () => {
    this.setState({ open: false });
    if (this.onCloseCallback) this.onCloseCallback();
  };
  deleteRow = id => {
    CrudCategory.deleteById(ActionCategory.TYPE_ASSIGN_DELETE, id);
  };

  render() {
    let header = language => [
      {
        id: "size",
        name: language.text.size,
        access: row => row.size.name
      },
      {
        id: "actions",
        access: row => (
          <Button onClick={this.deleteRow.bind(this, row.id)}>
            {language.button.remove}
          </Button>
        )
      }
    ];

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <div style={{ display: "inline-block" }}>
              <Button
                color="primary"
                variant="outlined"
                onClick={this.handleOpen}
              >
                {language.text.typeAssignedAdd}
              </Button>
              <Dialog open={this.state.open}>
                <DialogTitle>
                  <Typography variant="title">
                    {language.text.typeAssigned}
                    <Close style={closeImg} onClick={this.handleClose}/>
                  </Typography>
                </DialogTitle>
                <DialogContent>
                  <DataTable
                    data={this.state.data}
                    header={header(language)}
                    optSortable={true}
                    page={1}
                    perPage={10}
                    optLanguage={language.dataTable}
                    keyRow={row => row.id}
                  />
                </DialogContent>
                <DialogActions>
                  <Form onChange={this.handleUpdate} type={this.state.type} />
                  <Button color="secondary" onClick={this.handleClose}>
                    {language.button.close}
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(styles)(AssignSize);
