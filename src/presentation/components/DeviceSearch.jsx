import React from "react";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import { Paper } from "@material-ui/core";
import PropTypes from "prop-types";
import Card from "./Card/Card";
import CardHeader from "./Card/CardHeader";
import Filter from "../SmartComponents/Filter";
import CardBody from "./Card/CardBody";
import Datatable from "../SmartComponents/Datatable";
import KindStore from "../../stores/KindStore";
import ProducerStore from "../../stores/ProducerStore";
import TypeStore from "../../stores/TypeStore";
import PositionStore from "../../stores/PositionStore";
import WorkspaceStore from "../../stores/WorkspaceStore";
import SizeStore from "../../stores/SizeStore";
import { SystemContext } from "../../variables/SystemContext";
import AssignSizeStore from "../../stores/AssignSizeStore";

export default class DeviceSearch extends AbstractComponent {
  static defaultProps = {
    filter: {}
  };
  static propTypes = {
    onSelectDevice: PropTypes.func,
    filter: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      source: [],
      kindList: [],
      producerList: [],
      typeList: [],
      sizeList: [],
      workspaceList: [],
      positionList: [],
      typeDisable: true,
      devicesByType: TypeStore.getAll(),
      devicesBySize: AssignSizeStore.getAll(),
      devicesByPosition: PositionStore.getAll()
    };
    this.state.value = Object.assign({
        fulltext: () => true,
        kind: () => true,
        producer: () => true,
        type: () => true,
        size: () => true,
        workspace: () => true
    }, props.filter);
    this.onSelectDeviceCallback = props.onSelect;
  }

  componentDidMount() {
    KindStore.addChangeListener(this.updateKind);
    ProducerStore.addChangeListener(this.updateProducer);
    TypeStore.addChangeListener(this.updateType);
    SizeStore.addChangeListener(this.updateSize);
    AssignSizeStore.addChangeListener(this.updateSizeOfType);
    WorkspaceStore.addChangeListener(this.updateWorkspace);
    PositionStore.addChangeListener(this.updatePosition);
    this.updateKind();
    this.updateProducer();
    this.updateType();
    this.updateSize();
    this.updateSizeOfType();
    this.updateWorkspace();
    this.updatePosition();
  }

  componentWillUnmount() {
    KindStore.removeChangeListener(this.updateKind);
    ProducerStore.removeChangeListener(this.updateProducer);
    TypeStore.removeChangeListener(this.updateType);
    SizeStore.removeChangeListener(this.updateSize);
    AssignSizeStore.removeChangeListener(this.updateSizeOfType);
    WorkspaceStore.removeChangeListener(this.updateWorkspace);
    PositionStore.removeChangeListener(this.updatePosition);
  }

  updateKind = () => {
    this.setState({ kindList: KindStore.getAll() });
  };
  updateProducer = () => {
    this.setState({ producerList: ProducerStore.getAll() });
  };
  updateType = () => {
    let data = {};
    if (
      this.state.value.kind !== undefined &&
      this.state.value.producer !== undefined
    ) {
      data.typeList = TypeStore.getByKindAndProducer(
        this.state.value.kind,
        this.state.value.producer
      );
      data.devicesByType = TypeStore.getAll();
    } else data.devicesByType = TypeStore.getAll();
    this.setState(data);
  };
  updateSize = () => {
    this.setState({ sizeList: SizeStore.getAll() });
  };
  updateSizeOfType = () => {
    this.setState({ devicesBySize: AssignSizeStore.getAll() });
  };
  updateWorkspace = () => {
    this.setState({ workspaceList: WorkspaceStore.getAll() });
  };
  updatePosition = () => {
    this.setState({ devicesByPosition: PositionStore.getAll() });
  };

  handleFilter = data => {
    let values = Object.assign({}, this.state.value);
    for (let key in values) {
      switch (key) {
        case "kind":
        case "producer":
        case "size":
        case "workspace":
          if (data[key] !== undefined) {
            values[key] = row => row[key].id === data[key];
            break;
          }
        default:
          values[key] = () => true;
          break;
      }
    }
    let newState = {};

    if (data.kind !== undefined && data.producer !== undefined) {
      newState.typeDisable = false;
      newState.typeList = TypeStore.getByKindAndProducer(
        data.kind,
        data.producer
      );
      values.type = row => row.type.id === data.id;
    } else {
      newState.typeList = TypeStore.getAll();
      newState.typeDisable = true;
      values.type = undefined;
    }

    if (data.fulltext) {
      let filters = this.filters({text:{}});
      values.fulltext = row => {
        for (let key in filters) {
          let value = filters[key].access(row);
          if (value !== null && value !== undefined && value.toLowerCase().indexOf(data.fulltext.toLowerCase()) > -1)
            return true;
        }
        return false;
      };
    } else values.fulltext = () => true;
    newState.value = values;
    this.setState(newState);
  };
  handleSelectDevice = id => {
    if (this.onSelectDeviceCallback !== undefined)
      this.onSelectDeviceCallback(id);
  };
  loadData = (type, size, position) => {
    let results = position.map(item => {
      return {
        kind: item.kind,
        producer: item.producer,
        type: item.type,
        size: item.size,
        workspace: item.workspace,
        position: {
          id: item.id,
          name: item.name,
          uid: item.uid,
          serialNumber: item.serialNumber
        }
      };
    });
    size.map(item => {
      item["workspace"] = {};
      item["position"] = {};
      results.push(item);
      return null;
    });
    type.map(item => {
      results.push({
        kind: item.kind,
        producer: item.producer,
        type: {
          id: item.id,
          name: item.name
        },
        size: {},
        workspace: {},
        position: {}
      });
      return null;
    });

    return Filter.applyFilter(results, this.state.value);
  };
  filters = (language, isSearch) => {
    let filters = [
      {
        id: "kind",
        name: language.text.kind,
        access: row => row.kind.name,
        options: {
          filter: "select2",
          options: this.state.kindList,
          value: this.state.value.kind
        }
      },
      {
        id: "producer",
        access: row => row.producer.name,
        name: language.text.producer,
        options: {
          filter: "select2",
          options: this.state.producerList,
          value: this.state.value.producer,
          emptyValue: true
        }
      },
      {
        id: "type",
        access: row => row.type.name,
        name: language.text.type,
        options: {
          disabled: this.state.typeDisable,
          filter: "select2",
          options: this.state.typeList,
          value: this.state.value.type
        }
      },
      {
        id: "size",
        access: row => row.size.name,
        name: language.text.size,
        options: {
          filter: "select2",
          options: this.state.sizeList,
          value: this.state.value.size
        }
      },
      {
        id: "workspace",
        access: row => row.workspace.name,
        name: language.text.workspace,
        options: {
          filter: "select2",
          options: this.state.workspaceList,
          value: this.state.value.workspace
        }
      },
      {
        id: "position",
        access: row => row.position.name,
        name: language.text.positionName
      }
    ];
    if (isSearch === true) {
      filters.push({
        id: "fulltext",
        name: language.text.searchBox,
        options: {
          filter: "search",
          value: this.state.value.fulltext
        }
      });
    } else {
      filters.push({
        id: "serialNumber",
        access: row => row.position.serialNumber,
        name: language.text.positionSerialNumber
      });
      filters.push({
        id: "uid",
        access: row => row.position.uid,
        name: language.text.positionUid
      });
    }
    return filters;
  };

  render() {
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <Card plain>
              <CardHeader color="primary">
                <Filter
                  filters={this.filters(language, true)}
                  onChange={this.handleFilter}
                />
              </CardHeader>
              <CardBody>
                <Paper>
                  <Datatable
                    header={this.filters(language)}
                    data={this.loadData(
                      this.state.devicesByType,
                      this.state.devicesBySize,
                      this.state.devicesByPosition
                    )}
                    optSortBy="type"
                    optLanguage={language.dataTable}
                    perPage={10}
                    onSelectRow={this.handleSelectDevice}
                    keyRow={row =>
                      row.type.id + "_" + row.size.id + "_" + row.position.id
                    }
                  />
                </Paper>
              </CardBody>
            </Card>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
