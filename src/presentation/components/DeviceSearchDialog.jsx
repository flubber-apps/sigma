import PropTypes from "prop-types";
import {
  AppBar,
  Dialog,
  IconButton,
  Toolbar,
  Typography
} from "@material-ui/core";
import React from "react";
import Close from "@material-ui/icons/Close";
import SearchIcon from "@material-ui/icons/Search";
import DeviceSearch from "./DeviceSearch";

class DeviceSearchDialog extends DeviceSearch {
  static propTypes = {
    title: PropTypes.string.isRequired,
    filter: PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    icon: PropTypes.string,
    onChange: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
    onSelectRow: PropTypes.func
  };
  static defaultProps = {
    open: false,
    icon: SearchIcon
  };

  constructor(props) {
    super(props);
    this.state.open = false;
    this.onSelectRowCallback = props.onSelectRow;
    this.onChangeCallback = props.onChange;
    this.onSubmitCallback = props.onSubmit;
    this.onCloseCallback = props.onClose;
    this.onOpenCallback = props.onOpen;
  }

  handleClose = () => {
    this.setState({ open: false });
    if (this.onCloseCallback) this.onCloseCallback();
  };
  handleOpen = () => {
    this.setState({ open: true });
    if (this.onOpenCallback) this.onOpenCallback();
  };
  handleSave = () => {
    if (this.onSubmitCallback !== undefined)
      this.onSubmitCallback(this.state.controls);
  };
  handleSelectDevice = id => {
    if (this.onSelectRowCallback !== undefined) this.onSelectRowCallback(id);
    this.handleClose();
  };

  render() {
    const { title, icon: Icon } = this.props;
    let button = null;
    if (Icon === undefined) {
      button = (
        <div onClick={this.handleOpen}>
          <SearchIcon />
          {title}
        </div>
      );
    } else {
      button = <Icon onClick={this.handleOpen} />;
    }

    return (
      <>
        {button}
        <Dialog open={this.state.open} fullScreen>
          <AppBar style={{ position: "relative" }}>
            <Toolbar>
              <Typography variant="title" style={{ flex: 1 }}>
                {title}
              </Typography>
              <IconButton onClick={this.handleClose}>
                <Close />
              </IconButton>
            </Toolbar>
          </AppBar>
          {super.render()}
        </Dialog>
      </>
    );
  }
}

export default DeviceSearchDialog;
