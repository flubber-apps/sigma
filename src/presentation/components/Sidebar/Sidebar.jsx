import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
// core components
import HeaderLinks from "../Header/HeaderLinks.jsx";

import sidebarStyle from "../../../assets/jss/material-dashboard-react/components/sidebarStyle";
import AbstractComponent from "../../SmartComponents/AbstractComponent";
import UrlStore from "../../../stores/UrlStore";

class Sidebar extends AbstractComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    isAdmin: PropTypes.bool
  };
  static defaultProps = {
    isAdmin: false
  };
  /**
   * Verifies if routeName is the one active (in browser input)
   * @param routeName
   * @return {boolean}
   */
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1;
  }

  /**
   * Generate links
   * @param routes
   * @param classes
   * @param color
   * @return {*}
   */
  links(routes, classes, color) {

    return (
      <List className={classes.list}>
        {routes.map((prop, key) => {
          if (prop.redirect || prop.hidden) return null;
          let listItemClasses = classNames({
            [" " + classes[color]]: this.activeRoute(prop.path)
          });
          const whiteFontClasses = classNames({
            [" " + classes.whiteFont]: this.activeRoute(prop.path)
          });
          return (
            <NavLink
              to={prop.path}
              className={classes.item}
              activeClassName="active"
              key={key}
            >
              <ListItem button className={classes.itemLink + listItemClasses}>
                <ListItemIcon className={classes.itemIcon + whiteFontClasses}>
                  {typeof prop.icon === "string" ? (
                    <Icon>{prop.icon}</Icon>
                  ) : (
                    <prop.icon/>
                  )}
                </ListItemIcon>
                <ListItemText
                  primary={prop.sidebarName}
                  className={classes.itemText + whiteFontClasses}
                  disableTypography={true}
                />
              </ListItem>
            </NavLink>
          );
        })}
      </List>
    );
  }

  render() {
    const { classes, color, logo, image, logoText, routes, isAdmin } = this.props;
    let links = this.links(routes, classes, color);

    return (
      <div>
        <Hidden mdUp implementation="css">
          <Drawer
            variant="temporary"
            anchor="right"
            open={this.props.open}
            classes={{
              paper: classes.drawerPaper
            }}
            onClose={this.props.handleDrawerToggle}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            <div className={classes.logo}>
              <a href={UrlStore.getPath(isAdmin ?"Admin" : "Front", "Dashboard")} className={classes.logoLink}>
                <div className={classes.logoImage}>
                  <img src={logo} alt="logo" className={classes.img}/>
                </div>
                {logoText}
              </a>
            </div>
            <div className={classes.sidebarWrapper}>
              <HeaderLinks/>
              {links}
            </div>
            {image !== undefined ? (
              <div
                className={classes.background}
                style={{ backgroundImage: "url(" + image + ")" }}
              />
            ) : null}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            anchor="left"
            variant="permanent"
            open
            classes={{
              paper: classes.drawerPaper
            }}
          >
            <div className={classes.sidebarWrapper}>{links}</div>
            {image !== undefined ? (
              <div
                className={classes.background}
                style={{ backgroundImage: "url(" + image + ")" }}
              />
            ) : null}
          </Drawer>
        </Hidden>
      </div>
    );
  }
}

export default withStyles(sidebarStyle)(Sidebar);
