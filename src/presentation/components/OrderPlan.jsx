import React from "react";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Toolbar,
  withStyles
} from "@material-ui/core";
import Button from "../SmartComponents/Button";
import PropTypes from "prop-types";
import { SystemContext } from "../../variables/SystemContext";

import tableStyles from "../../assets/jss/material-dashboard-react/components/tableStyle";
import PlanStore from "../../stores/PlanStore";
import Edit from "./OrderPlanEdit";
import FileUpload from "../SmartComponents/FileUpload";
import HistoryStore from "../../stores/HistoryStore";
import UserStore from "../../stores/UserStore";

class OrderPlan extends AbstractComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    history: PropTypes.number.isRequired,
    onChange: PropTypes.func,
    onSave: PropTypes.func,
    onEdit: PropTypes.func,
    editable: PropTypes.bool,
    closed: PropTypes.bool
  };
  static defaultProps = {
    history: undefined,
    editable: true,
    closed: false
  };

  constructor(props) {
    super(props);

    this.onChangeCallback = props.onChange;

    this.state.historyId = parseInt(props.history);
    this.state.history = {};
    this.state.plan = {};
    this.state.rows = [];
    this.state.empty = false;
    this.state.edit = false;
    this.state.itemId = undefined;

    this.onSaveCallback = props.onSave;
    this.onEditCallback = props.onEdit;
    this.refUpload = React.createRef();
    this.downloadCallback = () => {};
  }

  componentDidMount() {
    PlanStore.addChangeListener(this.updatePlan);
    HistoryStore.addChangeListener(this.updateHistory);
    this.updatePlan();
    this.updateHistory();
  }
  componentWillUnmount() {
    PlanStore.removeChangeListener(this.updatePlan);
    HistoryStore.removeChangeListener(this.updateHistory);
  }

  updateProtocol = (itemId, response) => {
    if (response.status === "success") {
      for (let key in response.files) {
        PlanStore.updateFile(
          this.state.plan.id,
          itemId,
          response.files[key],
          response.finishedAt
        );
      }
      this.refUpload.current.handleClose();
    }
  };
  addPlanFile = response => {
    if (response.status === "success") {
      for (let key in response.files) {
        PlanStore.updatePlanFile(
          this.state.plan.id,
          response.files[key]
        );
      }
      this.refUpload.current.handleClose();
    }
  };

  orderRows = rows => {
    let current = true;
    return rows.sort((a, b) => a.order <= b.order).map(row => {
      if (current && !row.finishedAt) {
        row.current = true;
        current = false;
      } else row.current = false;
      return row;
    });
  };
  updatePlan = () => {
    let plan = PlanStore.getByHistoryId(this.state.historyId);
    this.setState({
      plan: plan,
      rows: plan.items || [],
      empty: plan.id === undefined
    });
  };
  updateHistory = () => {
    this.setState({
      history: HistoryStore.getById(this.state.historyId)
    });
  };

  generateRow = (row, classes, language, editable) => {
    let cells = this.generateHeader(language, editable).map(col => (
      <TableCell
        key={col.id + "_" + row.id}
        className={classes.tableCell}
        align={"center"}
        padding={col.padding ? col.padding : "default"}
      >
        {col.access(row)}
      </TableCell>
    ));
    return (
      <TableRow hover key={row.id}>
        {cells}
      </TableRow>
    );
  };
  generateToolbar = (language, editable) => {
    let buttons = [];
    if (!this.state.edit && !this.props.closed && (editable || !this.state.plan.generatedPlan)) {
      buttons.push(
        <Button
          key="edit"
          outlined={true}
          variant="outlined"
          color="primary"
          onClick={this.handleEdit}
        >
          {language.button.edit}
        </Button>
      );
    }
    if (buttons.length > 0) {
      return <Toolbar>{buttons}</Toolbar>;
    } else return null;
  };
  generateFooterToolbar = language => {
    if (this.state.rows.filter(row => row.current === true).length === 0 && this.state.plan.generatedPlan && this.state.plan.state !== "opened") {
      return (
        <Toolbar style={{ width: "100%" }}>
          <div style={{ width: "100%", textAlign: "right" }}>
            {this.state.history.state === "approved" && this.state.plan.generatedPlanFile === null ? (
              <>
                <Button
                  key="edit"
                  outlined={true}
                  onClick={this.handleGeneratePlan}
                  color="primary"
                >
                  {language.planConfigurator.generatePlan}
                </Button>
                <FileUpload
                  key="upload"
                  ref={this.refUpload}
                  apiPath={
                    this._config.apiUrl +
                    "/history/plan/upload?id=" +
                    this.state.historyId
                  }
                  onSuccess={response => {
                    this.updateProtocol(-1, response);
                    this.addPlanFile(response);
                  }}
                  //onError={this.updateProtocol.bind(this, row.id)}
                  directoryId={this.state.plan.directoryId}
                  title="Nahrát plán kontrol"
                  button={data => {
                    const { ...rest } = data;
                    return (
                      <Button
                        color={"primary"}
                        onClick={this.handleGeneratePlan}
                        {...rest}
                      >
                        Nahrát plán
                      </Button>
                    );
                  }}
                />
              </>
            ) : this.state.plan.generatedPlanFile !== null ? (
              <a
                key="download-plan"
                href={
                  this._config.apiUrl +
                  "/disk/file?uid=" +
                  this.state.plan.generatedPlanFile
                }
                color="primary"
              >
                Stáhnout plán
              </a>
            ) : null}
          </div>
        </Toolbar>
      );
    }
    return null;
  };
  generateHeader = (language, editable) => {
    return [
      {
        id: "order",
        name: language.planConfigurator.order,
        access: row => row.order
      },
      {
        id: "controlTypeId",
        name: language.planConfigurator.controlType,
        access: row => row.controlType.name
      },
      {
        id: "name",
        name: language.planConfigurator.name,
        access: row => row.name
      },
      {
        id: "finishedAt",
        name: language.planConfigurator.finishedAt,
        access: row =>
          row.finishedAt ? row.finishedAt : language.state.notDone
      },
      {
        id: "actions",
        name: undefined,
        padding: "none",
        access: row => {
          if (row.current && !this.props.closed) {
            if (!row.finishedAt && !editable) {
              if (row.controlTypeId === "P") {
                let result = [];
                result.push(
                  <Button
                    key="generate"
                    color={"primary"}
                    onClick={this.handleClick.bind(this, row.id, "P")}
                  >
                    {language.button.generate}
                  </Button>
                );
                result.push(
                  <FileUpload
                    key="upload"
                    ref={this.refUpload}
                    apiPath={
                      this._config.apiUrl +
                      "/history/plan/upload?id=" +
                      this.state.historyId +
                      "&itemId=" +
                      row.id +
                      "&userId=" +
                      UserStore.getUser().id
                    }
                    onSuccess={this.updateProtocol.bind(this, row.id)}
                    onError={this.updateProtocol.bind(this, row.id)}
                    directoryId={this.state.plan.directoryId}
                    title="Nahrát"
                    button={data => {
                      const { ...rest } = data;
                      return (
                        <Button
                          color={"primary"}
                          onClick={this.handleClick.bind(this, row.id, "P")}
                          {...rest}
                        >
                          {language.button.upload}
                        </Button>
                      );
                    }}
                  />
                );
                return result;
              } else
                return (
                  <Button
                    color={"primary"}
                    onClick={this.handleClick.bind(this, row.id, "V")}
                  >
                    {language.state.finish}
                  </Button>
                );
            }
          } else {
            if (row.finishedAt && row.controlTypeId === "P")
              return (
                <a
                  href={
                    this._config.apiUrl + "/disk/file?uid=" + row.protocolUid
                  }
                  download
                >
                  {language.button.download}
                </a>
              );
          }
        }
      }
    ];
  };

  handleEdit = () => {
    this.setState({ edit: true });
    this.onEditCallback();
  };
  handleSave = () => {
    this.setState({ edit: false });
    this.onSaveCallback();
  };
  handleGeneratePlan = () => {
    this.setState({
      itemId: 10
    });
    this.downloadCallback = type => {
      //console.log(type);
      PlanStore.generatePlan(this.state.historyId, type);
    };
  };
  handleClick = (id, type) => {
    if (type === "P") {
      this.setState({
        itemId: id
      });
      this.downloadCallback = type => {
        PlanStore.generate(this.state.historyId, this.state.itemId, type);
      };
    } else {
      PlanStore.finish(this.state.historyId, id);
    }
  };
  handleUpload = (id, type) => {
    if (type === "P") {
      PlanStore.generate(this.state.historyId, id);
    }
  };
  handleGenerateProtocol = type => {
    this.downloadCallback(type);
    this.setState({ itemId: undefined });
  };
  handleCloseSelectFormat = () => {
    this.setState({ itemId: undefined });
  };

  render() {
    const { classes, editable } = this.props;
    let selectFormat = language => {
      if (this.state.itemId) {
        return (
          <Dialog open={this.state.itemId !== undefined}>
            <DialogTitle>{language.planConfigurator.selectType}</DialogTitle>
            <DialogContent>
              <Button
                color={"primary"}
                onClick={this.handleGenerateProtocol.bind(this, "DOC")}
              >
                DOC
              </Button>
              <Button
                color={"primary"}
                onClick={this.handleGenerateProtocol.bind(this, "DOCX")}
              >
                DOCX
              </Button>
              <Button
                color={"primary"}
                onClick={this.handleGenerateProtocol.bind(this, "ODT")}
              >
                ODT
              </Button>
            </DialogContent>
            <DialogActions>
              <Button
                color={"transparent"}
                onClick={this.handleCloseSelectFormat}
              >
                {language.button.close}
              </Button>
            </DialogActions>
          </Dialog>
        );
      }
      return null;
    };
    let rows = this.orderRows(this.state.rows);
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          if (this.state.edit)
            return (
              <Edit history={this.state.historyId} onSave={this.handleSave}/>
            );
          else
            return (
              <>
                {selectFormat(language)}
                <Paper>
                  {this.generateToolbar(language, editable)}
                  <form>
                    <Table
                      className={classes.table + " " + classes.tableResponsive}
                    >
                      <TableHead className={classes.primaryTableHeader}>
                        <TableRow>
                          {this.generateHeader(language, editable).map(item => {
                            return (
                              <TableCell
                                className={classes.tableHeadCell}
                                key={item.id}
                                align={"center"}
                              >
                                {item.name}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {rows.map(row =>
                          this.generateRow(row, classes, language, editable)
                        )}
                      </TableBody>
                    </Table>
                  </form>
                  <Toolbar>{this.generateFooterToolbar(language, undefined,  rows)}</Toolbar>
                </Paper>
              </>
            );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(tableStyles)(OrderPlan);
