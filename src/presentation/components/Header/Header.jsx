import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Hidden from "@material-ui/core/Hidden";
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import HeaderLinks from "./HeaderLinks.jsx";

import headerStyle from "assets/jss/material-dashboard-react/components/headerStyle.jsx";
import { Button } from "@material-ui/core";

function Header({ ...props }) {
  function makeBrand() {
    let names = props.routes
      .map((prop, key) => {
        if (prop.path === props.location.pathname) {
          return prop.navbarName;
        }
        return null;
      })
      .filter(name => name !== null);
    if (names.length)
      return (
        <Button href="#" className={classes.title}>
          {names[0]}
        </Button>
      );
    return null;
  }
  const { classes, color, isAdmin } = props;
  const appBarClasses = classNames({
    [" " + classes[color]]: color
  });
  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <div className={classes.flex}>{makeBrand()}</div>
        <Hidden smDown implementation="css">
          <HeaderLinks isAdmin={isAdmin} />
        </Hidden>
        <Hidden mdUp implementation="css">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >
            <Menu />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
  isAdmin: PropTypes.bool
};
Header.defaultProps = {
  isAdmin: false
};

export default withStyles(headerStyle)(Header);
