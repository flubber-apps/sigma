import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";

import {
  MenuList,
  MenuItem,
  Grow,
  Paper,
  ClickAwayListener,
  Popper,
  withStyles
} from "@material-ui/core";
import { Person, Settings, Dashboard } from "@material-ui/icons";

import AbstractComponent from "../../SmartComponents/AbstractComponent";
import Button from "../../SmartComponents/Button";
import UrlStore from "../../../stores/UrlStore";
import UserStore from "../../../stores/UserStore";
import UserActions from "../../../actions/UserActions";

import Configuration from "../../../variables/configuration";

import styles from "../../../assets/jss/material-dashboard-react/components/headerLinksStyle";

class HeaderLinks extends AbstractComponent {
  static propTypes = {
    isAdmin: PropTypes.bool
  };
  static defaultProps = {
    isAdmin: false
  };

  constructor(props) {
    super(props);
    this.state.open = false;
    this.state.isAdmin = props.isAdmin;
    this.state.user = {};
  }
  componentDidMount() {
    UserStore.addChangeListener(this.updateUser);
    this.updateUser();
  }
  componentWillUnmount() {
    UserStore.removeChangeListener(this.updateUser);
  }

  updateUser = () => {
    this.setState({ user: UserStore.getUser() });
  };
  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };
  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
  };
  handleRedirect = path => () => {
    Configuration.redirect(path);
  };
  handleSignIn = () => {
    Configuration.redirect(UrlStore.getPath("Sign", "SignIn"));
  };
  handleSignOut = () => {
    UserActions.signOut(() => {
      Configuration.redirect(UrlStore.getPath("Sign", "SignIn"));
    });
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;
    let switchButton = null;
    if ((this.state.user.roles || []).filter(role => role.name !== "user").length > 0) {
      if (!this.state.isAdmin) {
        if (UserStore.isSigned()) {
          switchButton = (
            <Button
              color={window.innerWidth > 959 ? "transparent" : "white"}
              justIcon={false}
              simple={!(window.innerWidth > 959)}
              aria-label="Settings"
              className={classes.buttonLink}
              onClick={this.handleRedirect(
                UrlStore.getPath("Admin", "Dashboard")
              )}
            >
              <Settings className={classes.icons}/>
              <p className={classes.linkText}> Administrace</p>
            </Button>
          );
        }
      } else {
        switchButton = (
          <Button
            color={window.innerWidth > 959 ? "transparent" : "white"}
            justIcon={false}
            simple={!(window.innerWidth > 959)}
            aria-label="Dashboard"
            className={classes.buttonLink}
            onClick={this.handleRedirect(UrlStore.getPath("Front", "Dashboard"))}
          >
            <Dashboard className={classes.icons}/>
            <p className={classes.linkText}> Aplikace</p>
          </Button>
        );
      }
    }
    let user = null;
    if (UserStore.isSigned()) {
      user = (
        <Button
          buttonRef={node => {
            this.anchorEl = node;
          }}
          color={window.innerWidth > 959 ? "transparent" : "white"}
          justIcon={false}
          simple={!(window.innerWidth > 959)}
          aria-label="Person"
          className={classes.buttonLink}
          onClick={this.handleToggle}
        >
          <Person className={classes.icons} />
          <p className={classes.linkText}>
            {this.state.user.name} {this.state.user.surname}
          </p>
        </Button>
      );
    } else {
      user = (
        <Button
          color={window.innerWidth > 959 ? "transparent" : "white"}
          justIcon={false}
          simple={!(window.innerWidth > 959)}
          aria-label="Person"
          className={classes.buttonLink}
          onClick={this.handleSignIn}
        >
          <Person className={classes.icons} />
          <p className={classes.linkText}>Přihlásit se</p>
        </Button>
      );
    }
    return (
      <>
        {switchButton}
        <div className={classes.manager}>
          <Popper
            open={open}
            anchorEl={this.anchorEl}
            transition
            disablePortal
            className={
              classNames({ [classes.popperClose]: !open }) +
              " " +
              classes.pooperNav
            }
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom"
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <MenuList role="menu">
                      <MenuItem>{this.state.user.email}</MenuItem>
                      <MenuItem
                        onClick={this.handleSignOut}
                        className={classes.dropdownItem}
                      >
                        Odhlásit se
                      </MenuItem>
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </div>
        {user}
      </>
    );
  }
}

export default withStyles(styles)(HeaderLinks);
