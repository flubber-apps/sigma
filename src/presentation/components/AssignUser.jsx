import AbstractComponent from "../SmartComponents/AbstractComponent";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from "@material-ui/core";
import React from "react";
import Close from "@material-ui/icons/Close";
import { SystemContext } from "../../variables/SystemContext";
import DataTable from "../SmartComponents/Datatable";
import CrudCategory from "../../actions/CrudCategory";
import Form from "../Forms/AssignUser";
import WorkspaceStore from "../../stores/WorkspaceStore";
import UserStore from "../../stores/UserStore";

let closeImg = { cursor: "pointer", float: "right", width: "20px" };

const styles = theme => ({
  formControl: {
    width: "100%"
  }
});

class AssignUser extends AbstractComponent {
  static propTypes = {
    workspace: PropTypes.object.isRequired,
    onChange: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func
  };
  state = {
    open: false,
    workspace: null,
    data: []
  };

  constructor(props) {
    super(props);
    this.state.workspace = props.workspace;
    this.state.data = [];
    this.onCloseCallback = props.onClose;
    this.onOpenCallback = props.onOpen;
  }

  componentWillMount() {
    UserStore.addChangeListener(this.handleUpdate);
    WorkspaceStore.addChangeListener(this.handleUpdate);
    this.handleUpdate();
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this.handleUpdate);
    WorkspaceStore.removeChangeListener(this.handleUpdate);
  }

  handleUpdate = () => {
    let workspace = WorkspaceStore.getAll().filter(workspace => this.state.workspace.id === workspace.id)[0];
    this.setState({ data: workspace.users.map(userId => UserStore.getById(userId)) });
  };
  handleOpen = () => {
    this.setState({ open: true });
    this.handleUpdate();
    if (this.onOpenCallback) this.onOpenCallback();
  };
  handleClose = () => {
    this.setState({ open: false });
    if (this.onCloseCallback) this.onCloseCallback();
  };
  deleteRow = id => {
    CrudCategory.removeAssignedUser(this.state.workspace.id, id);
  };

  render() {
    let header = language => [
      {
        id: "name",
        name: language.user.name,
        access: row => row.name
      },{
        id: "surname",
        name: language.user.surname,
        access: row => row.surname
      },{
        id: "email",
        name: language.user.email,
        access: row => row.email
      },
      {
        id: "actions",
        access: row => (
          <Button onClick={this.deleteRow.bind(this, row.id)}>
            {language.button.remove}
          </Button>
        )
      }
    ];

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <div style={{ display: "inline-block" }}>
              <Button
                color="primary"
                variant="outlined"
                onClick={this.handleOpen}
              >
                {language.text.userAssignedAdd}
              </Button>
              <Dialog open={this.state.open} fullScreen={true}>
                <DialogTitle>
                  {language.text.userAssigned}
                  <Close style={closeImg} onClick={this.handleClose}/>
                </DialogTitle>
                <DialogContent>
                  <DataTable
                    data={this.state.data}
                    header={header(language)}
                    optSortable={true}
                    page={1}
                    perPage={10}
                    optLanguage={language.dataTable}
                    keyRow={row => row.email}
                  />
                </DialogContent>
                <DialogActions>
                  <Form onChange={this.handleUpdate} type={this.state.workspace} />
                  <Button color="secondary" onClick={this.handleClose}>
                    {language.button.close}
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(styles)(AssignUser);
