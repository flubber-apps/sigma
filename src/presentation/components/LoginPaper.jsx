import React from "react";
import PropTypes from "prop-types";
import StringUtil from "../../utils/String";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Configuration from "../../variables/configuration";

import {
  Avatar,
  Button,
  FormControl,
  Input,
  InputLabel,
  Paper,
  SnackbarContent,
  Typography,
  withStyles
} from "@material-ui/core";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import UserStore from "../../stores/UserStore";

import { SystemContext } from "../../variables/SystemContext";
import UrlStore from "../../stores/UrlStore";

const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  }
});

class SignIn extends AbstractComponent {
  static propTypes = {
    button: PropTypes.func
  };
  static defaultProps = {
    button: () => null
  };

  constructor(props) {
    super(props);
    this.state.email = "";
    this.state.password = "";
    this.state.displayError = false;
    this.state.redirect = false;
    this.refDialog = React.createRef();
  }

  validateForm = () => {
    return (
      this.state.email.length > 0 &&
      this.state.password.length > 0 &&
      StringUtil.isEmail(this.state.email)
    );
  };
  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  };
  handleSubmit = () => {
    UserStore.signIn(this.state.email, this.state.password, () => {
      Configuration.redirect(UrlStore.getPath("Front", "Dashboard"));
    });
  };
  handleClose = event => {
    this.refDialog.current.handleClose(event);
  };
  _onChange = () => {
    if (UserStore.isSigned()) {
      this.setState({ redirect: true });
    } else {
      this.setState({ displayError: true });
    }
  };

  componentWillMount() {
    UserStore.addChangeListener(this._onChange.bind(this));
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this._onChange.bind(this));
  }

  render() {
    const { classes } = this.props;
    if (this.state.displayError) {
    }
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          let translate = language.signIn;
          return (
            <main className={classes.main}>
              <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                  <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="display1">
                  {translate.title}
                </Typography>
                {this.state.displayError ? (
                  <SnackbarContent
                    message={translate.errorMessage}
                    close
                    color="danger"
                    onClickClose={() => {
                      this.setState({ displayError: false });
                    }}
                  />
                ) : null}
                <form className={classes.form}>
                  <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="email">
                      {translate.labelEmail}
                    </InputLabel>
                    <Input
                      id="email"
                      name="email"
                      autoComplete="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                      autoFocus
                    />
                  </FormControl>
                  <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="password">
                      {translate.labelPassword}
                    </InputLabel>
                    <Input
                      id="password"
                      name="password"
                      type="password"
                      value={this.state.password}
                      onChange={this.handleChange}
                      autoComplete="current-password"
                    />
                  </FormControl>
                  <Button
                    type="button"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={this.handleSubmit}
                  >
                    {translate.submit}
                  </Button>
                </form>
              </Paper>
            </main>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SignIn);
