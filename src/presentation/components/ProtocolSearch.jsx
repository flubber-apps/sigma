import React from "react";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import { Paper } from "@material-ui/core";
import PropTypes from "prop-types";
import Card from "./Card/Card";
import CardBody from "./Card/CardBody";
import Datatable from "../SmartComponents/Datatable";
import { SystemContext } from "../../variables/SystemContext";
import ProtocolStore from "../../stores/ProtocolStore";

export default class ProtocolSearch extends AbstractComponent {
  static defaultProps = {
    filter: {}
  };
  static propTypes = {
    workspaceId: PropTypes.number.isRequired,
    onSelect: PropTypes.func,
    filter: PropTypes.object
  };

  constructor(props) {
    super(props);

    this.state = {
      source: [],
      data: [],
      workspaceId: undefined
    };
    this.state.value = props.filter;
    this.onSelectCallback = props.onSelect;
  }

  componentWillMount() {
    ProtocolStore.addChangeListener(this.updateProtocol);
  }
  componentWillUnmount() {
    ProtocolStore.removeChangeListener(this.updateProtocol);
  }

  updateProtocol = () => {
    this.setState({ source: ProtocolStore.getAll() });
  };

  handleSelect = id => {
    if (this.onSelectCallback !== undefined) this.onSelectCallback(id);
  };
  loadData = workspaceId => {
    return this.state.source;
  };
  header = language => {
    return [
      {
        id: "number",
        name: language.protocol.number,
        access: row => row.number
      },
      {
        id: "name",
        name: language.protocol.name,
        access: row => row.name
      }
    ];
  };

  render() {
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <Card plain>
              <CardBody>
                <Paper>
                  <Datatable
                    header={this.header(language)}
                    data={this.loadData(this.state.workspaceId)}
                    optSortBy="number"
                    optLanguage={language.dataTable}
                    perPage={10}
                    onSelectRow={this.handleSelect}
                    keyRow={row => row.id}
                  />
                </Paper>
              </CardBody>
            </Card>
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
