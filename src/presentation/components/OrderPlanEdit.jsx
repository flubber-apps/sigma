import React from "react";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import {
  Checkbox,
  FormControl,
  FormControlLabel,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableRow,
  TextField,
  Toolbar,
  withStyles
} from "@material-ui/core";
import Button from "../SmartComponents/Button";
import PropTypes from "prop-types";
import { SystemContext } from "../../variables/SystemContext";
import Select from "../SmartComponents/FormControls/Select";
import TypeStore from "../../stores/TypeStore";
import HistoryStore from "../../stores/HistoryStore";
import ControlTypeStore from "../../stores/ControlTypeStore";

import tableStyles from "../../assets/jss/material-dashboard-react/components/tableStyle";
import DecreeStore from "../../stores/DecreeStore";
import PlanStore from "../../stores/PlanStore";
import ProtocolSearchDialog from "./ProtocolSearchDialog";
import ProtocolStore from "../../stores/ProtocolStore";

class OrderPlanEdit extends AbstractComponent {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    rows: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func,
    onSave: PropTypes.func,
    type: PropTypes.number,
    position: PropTypes.number,
    history: PropTypes.number
  };
  static defaultProps = {
    rows: [],
    position: undefined,
    history: undefined
  };

  constructor(props) {
    super(props);
    this.state = Object.assign(this.state, {
      edited: false,
      displayNewRow: false,
      displaySearchProtocol: false,
      decreeList: [],
      typeList: [],
      value: {
        changedWatched: false,
        controlTypeId: "",
        decreeId: null,
        watched: false,
        name: "",
        protocolId: undefined
      },
      typeId: Number.isInteger(parseInt(props.type)) ? parseInt(props.type) : undefined,
      positionId: Number.isInteger(parseInt(props.position)) ? parseInt(props.position) : undefined,
      historyId: Number.isInteger(parseInt(props.history)) ? parseInt(props.history) : undefined,
      type: {},
      position: {},
      history: {}
    });
    this.onChangeCallback = props.onChange;
    this.onSaveCallback = props.onSave;
  }

  componentDidMount() {
    TypeStore.addChangeListener(this.updateType);
    ControlTypeStore.addChangeListener(this.updateControlType);
    DecreeStore.addChangeListener(this.updateDecree);
    PlanStore.addChangeListener(this.updatePlan);
    HistoryStore.addChangeListener(this.updateHistory);
    this.updateHistory();
    this.updateControlType();
    this.updateDecree();
    this.updateType();
  }
  componentWillUnmount() {
    TypeStore.removeChangeListener(this.updateType);
    ControlTypeStore.removeChangeListener(this.updateControlType);
    DecreeStore.removeChangeListener(this.updateDecree);
    PlanStore.removeChangeListener(this.updatePlan);
    HistoryStore.removeChangeListener(this.updateHistory);
  }

  orderRows = rows => rows.sort((a, b) => a.order <= b.order);
  addRow = () => {
    let row = PlanStore.validateData([
      {
        items: [
          {
            order: this.state.rows.length + 1,
            controlTypeId: this.state.value.controlTypeId,
            decreeId: this.state.value.decreeId,
            watched: this.state.value.watched,
            name: this.state.value.name,
            protocolId: this.state.value.protocolId
          }
        ]
      }
    ])[0].items[0];
    let rows = this.state.rows;
    rows.push(row);
    this.setState({
      edited: true,
      rows: rows,
      displayNewRow: false,
      value: {
        changedWatched: false,
        order: this.state.value.order + 1,
        controlTypeId: undefined,
        decreeId: null,
        watched: false,
        name: "",
        protocolId: undefined
      },
      planIsGenerated: true
    });
    if (this.onChangeCallback !== undefined) this.onChangeCallback(row);
  };

  updateType = () => {
    this.setState({ type: TypeStore.getById(this.state.typeId) });
  };
  updateControlType = () => {
    this.setState({ typeList: ControlTypeStore.getAll() });
  };
  updateDecree = () => {
    this.setState({ decreeList: DecreeStore.getAll() });
  };
  updatePlan = () => {
    let plan = PlanStore.getByFullId(
      this.state.typeId,
      this.state.positionId,
      this.state.historyId
    );

    this.setState({
      plan: plan,
      rows: plan.items || [],
      empty: plan.id === undefined
    });
  };
  updateHistory = () => {
    let history = HistoryStore.getById(this.state.historyId);
    let position = history.position || {};
    let plan = PlanStore.getByFullId(
      this.state.typeId,
      this.state.positionId,
      this.state.historyId,
      () => this.setState({ empty: false }),
      () => this.setState({ empty: true })
    );
    this.setState({
      history: history,
      position: position,
      plan: plan,
      rows: plan.items || [],
      empty: plan.id === undefined
    });
  };

  generateRow = (row, classes, language) => {
    let isFirst = true;
    return (
      <TableRow hover key={row.order}>
        {this.generateHeader(language).map((col, id) => {
          let padding = {};
          if (!isFirst) padding = { padding: "none" };
          isFirst = false;
          return (
            <TableCell
              key={id}
              className={classes.tableCell}
              align={"center"}
              {...padding}
            >
              {col.access(row)}
            </TableCell>
          );
        })}
      </TableRow>
    );
  };
  generateNewRow = (classes, language) => {
    let disableCheckBox = true;
    let value = true;
    if (this.state.value.controlTypeId === "V") {
      disableCheckBox = true;
      value = false;
    } else {
      if (this.state.value.protocolId && this.state.value.changedWatched === false) {
        let protocol = ProtocolStore.getById(this.state.value.protocolId);
        disableCheckBox = !protocol.isGenerated;
      } else {
        disableCheckBox = false && this.state.value.protocolId === undefined;
        value = this.state.value.watched && !disableCheckBox;
      }
    }
    if (!this.state.displayNewRow) return null;
    return this.generateRow(
      {
        order: this.state.rows.length + 1,
        controlType: {
          name: (
            <Select
              id="controlTypeId"
              label={language.text.planConfiguratorTypeOfControl}
              onChange={this.handleChange.bind(this, "controlTypeId")}
              options={this.state.typeList}
              value={this.state.value.controlTypeId}
            />
          )
        },
        decree: {
          name: (
            <Select
              id="decreeId"
              label={language.text.planConfiguratorDecree}
              onChange={this.handleChange.bind(this, "decreeId")}
              options={this.state.decreeList}
            />
          )
        },
        watched: (
          <FormControlLabel
            control={
              <Checkbox
                onChange={event =>
                  this.handleChange("watched", event.target.value)
                }
                disabled={disableCheckBox}
                checked={value}
                value="watch"
              />
            }
            label=""
          />
        ),
        name: (
          <FormControl>
            <TextField
              id="name"
              label={language.text.planConfiguratorName}
              onChange={event => this.handleChange("name", event.target.value)}
              value={this.state.value.name}
            />
          </FormControl>
        ),
        actions: (
          <Button color="primary" onClick={this.addRow}>
            {language.button.add}
          </Button>
        )
      },
      classes,
      language
    );
  };
  generateToolbar = language => {
    let result = [];
    if (this.state.historyId)
      result.push(
        <Button
          key="back"
          outlined={true}
          variant="outlined"
          color="primary"
          onClick={this.handleBack}
        >
          {language.button.back}
        </Button>
      );
    if (this.state.empty) {
      if (
        this.state.positionId === undefined &&
        this.state.historyId === undefined
      )
        result.push(
          <Button
            key="create"
            outlined={true}
            variant="outlined"
            color="primary"
            onClick={this.handleCreate}
          >
            {language.planConfigurator.create}
          </Button>
        );
      if (this.state.positionId !== undefined)
        result.push(
          <Button
            key="importType"
            outlined={true}
            variant="outlined"
            color="primary"
            onClick={this.handleImportFromType}
          >
            {language.planConfigurator.importType}
          </Button>
        );
      if (this.state.historyId !== undefined)
        result.push(
          <Button
            key="importPosition"
            outlined={true}
            variant="outlined"
            color="primary"
            onClick={this.handleImportFromPosition}
          >
            {language.planConfigurator.importPosition}
          </Button>
        );
    } else {
      if (this.state.displayNewRow === false)
        result.push(
          <Button
            key="addRow"
            outlined={true}
            variant="outlined"
            color="primary"
            onClick={this.handleShowNewRow}
          >
            {language.planConfigurator.addRow}
          </Button>
        );
      if (this.state.edited)
        result.push(
          <Button
            key="save"
            outlined={true}
            variant="outlined"
            color="primary"
            onClick={this.handleSave}
          >
            {language.button.save}
          </Button>
        );
    }
    let plan = this.state.plan || {};
    result.push(
      <FormControlLabel
        key="by-plan"
        control={
          <Checkbox
            color={"primary"}
            checked={plan.generatedPlan === undefined ? false : plan.generatedPlan}
            disabled={plan.generatedPlan === undefined ? false : !plan.generatedPlan}
            onChange={this.handleChange.bind(this, "generatedPlan")}
          />
        }
        label={"Kontroly podle plánu"}
      />
    );
    return result;
  };
  generateHeader = language => {
    return [
      {
        id: "order",
        name: language.planConfigurator.order,
        access: row => row.order
      },
      {
        id: "controlTypeId",
        name: language.planConfigurator.controlType,
        access: row => row.controlType.name
      },
      {
        id: "decreeId",
        name: language.planConfigurator.decree,
        access: row => row.decree.name
      },
      {
        id: "watch",
        name: language.planConfigurator.watch,
        access: row => {
          if (row.decreeId !== undefined)
            return row.watched === true
              ? language.button.agree
              : language.button.disagree;
          return row.watched;
        }
      },
      {
        id: "name",
        name: language.planConfigurator.name,
        access: row => row.name
      },
      {
        id: "actions",
        name: undefined,
        access: row => row.actions
      }
    ];
  };

  handleChange = (id, value) => {
    let newPlan = Object.assign({}, this.state.plan);
    newPlan.generatedPlan = !newPlan.generatedPlan;
    if (id === "generatedPlan")
      this.setState({
        edited: true,
        plan: newPlan
      });
    else if (id === "controlTypeId")
      this.setState({
        value: Object.assign({}, this.state.value, {
          changedWatched: false,
          controlTypeId: value
        }),
        displaySearchProtocol: value === "P"
      });
    else if (id === "watched")
      this.setState({
        value: Object.assign({}, this.state.value, {
          changedWatched: true,
          watched: !this.state.value.watched
        })
      });
    else
      this.setState({
        changedWatched: false,
        value: Object.assign({}, this.state.value, { [id]: value })
      });
  };
  handleAssignProtocol = id => {
    let protocol = ProtocolStore.getById(id);
    this.setState({
      displaySearchProtocol: false,
      value: Object.assign({}, this.state.value, {
        protocolId: id,
        name: protocol.name
      })
    });
  };
  handleCloseProtocol = () => {
    /*this.setState({
      value: Object.assign({}, this.state.value, { controlTypeId: undefined })
    });*/
  };
  handleShowNewRow = () => {
    this.setState({ displayNewRow: true });
  };
  handleSave = () => {
    if (this.state.plan.id !== undefined) {
      PlanStore.update(this.state.plan.id, this.state.plan.generatedPlan, this.state.rows, () =>
        this.setState({ edited: false })
      );
    } else {
      PlanStore.add(
        this.state.typeId,
        this.state.positionId,
        this.state.historyId,
        this.state.rows,
        () => this.setState({ edited: false })
      );
    }
    this.handleBack();
  };
  handleBack = () => {
    if (this.onSaveCallback !== undefined) this.onSaveCallback();
  };
  handleCreate = () => {
    this.setState({ empty: false });
  };
  handleImportFromType = () => {
    if (this.state.historyId === undefined)
      PlanStore.toPosition(this.state.typeId, this.state.positionId, () => {
        this.setState({ empty: false });
      });
    else
      PlanStore.toHistoryFromType(
        this.state.typeId,
        this.state.historyId,
        () => {
          this.setState({ empty: false });
        }
      );
  };
  handleImportFromPosition = () => {
    PlanStore.toHistoryFromPosition(
      this.state.positionId,
      this.state.historyId,
      () => {
        this.setState({ empty: false });
      }
    );
  };

  render() {
    const { classes } = this.props;
    let searchProtocol = (display, language) => {
      if (display)
        return (
          <ProtocolSearchDialog
            title={language.protocol.titleSearch}
            open={true}
            onSelect={this.handleAssignProtocol}
            onClose={this.handleCloseProtocol}
          />
        );
      return null;
    };

    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          if (this.state.empty)
            return (
              <Paper>
                <Toolbar>{this.generateToolbar(language)}</Toolbar>
              </Paper>
            );
          else
            return (
              <Paper>
                {searchProtocol(this.state.displaySearchProtocol, language)}
                <Toolbar>{this.generateToolbar(language)}</Toolbar>
                <form>
                  <Table
                    className={classes.table + " " + classes.tableResponsive}
                  >
                    <TableHead className={classes.primaryTableHeader}>
                      <TableRow>
                        {this.generateHeader(language).map(item => {
                          return (
                            <TableCell
                              className={classes.tableHeadCell}
                              key={item.id}
                              align={"right"}
                            >
                              {item.name}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {this.orderRows(this.state.rows || []).map(row =>
                        this.generateRow(row, classes, language)
                      )}
                    </TableBody>
                    <TableFooter>
                      {this.generateNewRow(classes, language)}
                    </TableFooter>
                  </Table>
                </form>
              </Paper>
            );
        }}
      </SystemContext.Consumer>
    );
  }
}

export default withStyles(tableStyles)(OrderPlanEdit);
