import React from "react";
import PropTypes from "prop-types";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";
import Button from "@material-ui/core/es/Button/Button";
import Dialog from "@material-ui/core/es/Dialog/Dialog";
import DialogTitle from "@material-ui/core/es/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/es/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/es/DialogActions/DialogActions";
import FormControl from "@material-ui/core/es/FormControl/FormControl";
import TextField from "@material-ui/core/es/TextField/TextField";

const renderTextField = (input, label, ...custom) => {
  return (
    <TextField
      hintText={label}
      floatingLableText={label}
      {...input}
      {...custom}
    />
  );
};

class OrderForm extends React.Component {
  state = {
    open: false
  };
  show = () => {
    this.setState({ open: true });
  };
  hide = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, name } = this.props;
    return (
      <div>
        <Dialog
          open={this.state.open}
          aria-labelledby="form-dialog-title"
          fullWidth={true}
          maxWidth="xs"
        >
          <DialogTitle>{this.props.title}</DialogTitle>
          <DialogContent md={12} xs={12}>
            <form>
              <div>
                <FormControl
                  fullWidth={true}
                  name="kind"
                  component={renderTextField}
                  label="Pozice"
                />
              </div>
              <div>
                <FormControl
                  fullWidth={true}
                  name="kind"
                  component={renderTextField}
                  label="Datum založení"
                />
              </div>
              <div>
                <FormControl
                  fullWidth={true}
                  name="kind"
                  component={renderTextField}
                  label="Číslo zakázky"
                />
              </div>
              <div>
                <FormControl
                  fullWidth={true}
                  name="kind"
                  component={renderTextField}
                  label="Velikost"
                />
              </div>
              <div>
                <FormControl
                  fullWidth={true}
                  name="kind"
                  component={renderTextField}
                  label="Pracoviště"
                />
              </div>
            </form>
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={this.hide}>
              Uložit
            </Button>
            <Button color="primary" onClick={this.hide}>
              Zrušit
            </Button>
          </DialogActions>
        </Dialog>
        <Button color="primary" variant="outlined" onClick={this.show}>
          {name}
        </Button>
      </div>
    );
  }
}

OrderForm.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(OrderForm);
