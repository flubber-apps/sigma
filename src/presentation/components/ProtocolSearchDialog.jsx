import React from "react";
import {
  AppBar,
  Dialog,
  IconButton,
  Toolbar,
  Typography
} from "@material-ui/core";
import PropTypes from "prop-types";
import ProtocolStore from "../../stores/ProtocolStore";
import ProtocolSearch from "./ProtocolSearch";
import SearchIcon from "@material-ui/icons/Search";
import Close from "@material-ui/icons/Close";

export default class ProtocolSearchDialog extends ProtocolSearch {
  static defaultProps = {
    open: false
  };
  static propTypes = {
    title: PropTypes.string.isRequired,
    onSelect: PropTypes.func,
    onClose: PropTypes.func,
    filter: PropTypes.object,
    icon: PropTypes.string,
    open: PropTypes.bool
  };

  constructor(props) {
    super(props);

    this.state.source = ProtocolStore.getAll();
    this.state.open = props.open;
    this.onCloseCallback = props.onClose;
  }

  handleClose = event => {
    this.setState({ open: false });
    if (this.onCloseCallback) this.onCloseCallback();
  };
  handleOpen = event => {
    this.setState({ open: true });
    if (this.onOpenCallback) this.onOpenCallback();
  };
  handleSelect = id => {
    if (this.onSelectCallback !== undefined) this.onSelectCallback(id);
    this.setState({ open: false });
  };
  render() {
    const { title, icon: Icon } = this.props;
    let button = null;
    if (Icon === undefined) {
      button = (
        <div onClick={this.handleOpen}>
          <SearchIcon />
          {title}
        </div>
      );
    } else {
      button = <Icon onClick={this.handleOpen} />;
    }

    return (
      <div>
        {button}
        <Dialog open={this.state.open} fullScreen>
          <AppBar style={{ position: "relative" }}>
            <Toolbar>
              <Typography variant="title" style={{ flex: 1 }}>
                {title}
              </Typography>
              <IconButton onClick={this.handleClose}>
                <Close />
              </IconButton>
            </Toolbar>
          </AppBar>
          {super.render()}
        </Dialog>
      </div>
    );
  }
}
