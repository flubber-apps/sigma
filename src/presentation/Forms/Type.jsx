import React from "react";

import PropTypes from "prop-types";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import FormGenerator from "../SmartComponents/FormDialog";
import { SystemContext } from "../../variables/SystemContext";
import KindStore from "../../stores/KindStore";
import ProducerStore from "../../stores/ProducerStore";
import CrudCategory from "../../actions/CrudCategory";
import Dialog from "../SmartComponents/Dialog";
import OrderPlan from "../components/OrderPlanEdit";
import { Button } from "@material-ui/core";

export default class Type extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    entity: PropTypes.object
  };
  static defaultProps = {
    entity: null
  };
  state = {
    enable: true,
    error: false,
    valid: false,
    kindList: [],
    producerList: []
  };
  entity = null;

  constructor(props) {
    super(props);
    for (let key in props) {
      if (key === "onChange") continue;
      if (key === "entity") continue;
      this.state[key] = props[key];
    }
    this.entity = props.entity;
    this.formRef = React.createRef();
  }

  componentWillMount() {
    KindStore.addChangeListener(this.updateKind);
    ProducerStore.addChangeListener(this.updateProducer);
  }

  componentWillUnmount() {
    KindStore.removeChangeListener(this.updateKind);
    ProducerStore.removeChangeListener(this.updateProducer);
  }

  updateKind = () => {
    this.setState({ kindList: KindStore.getAll() });
  };
  updateProducer = () => {
    this.setState({ producerList: ProducerStore.getAll() });
  };
  handleSubmit = data => {
    if (this.entity !== null) {
      //TODO update entity
    } else {
      CrudCategory.addType(
        data.kind,
        data.producer,
        data.type,
        () => {
          this.formRef.current.handleClose();
        },
        () => {
          this.setState({ error: true });
        }
      );
    }
  };
  handleOrderPlan = (a, data) => {
    //console.log(a, data);
  };
  handleValidation = data => {
    if (
      data.kind !== undefined &&
      data.producer !== undefined &&
      data.type !== undefined &&
      data.type !== ""
    )
      this.setState({ valid: true });
    else this.setState({ valid: false });
  };
  handleClose = () => {
    this.setState({ error: false });
  };

  render() {
    const { entity } = this.props;
    this.entity = entity;
    let controls = (translate, entity) => {
      return [
        {
          id: "kind",
          type: "select",
          options: this.state.kindList,
          label: translate.text.kind,
          required: true
        },
        {
          id: "producer",
          type: "select",
          options: this.state.producerList,
          label: translate.text.producer,
          required: true
        },
        {
          id: "type",
          label: translate.text.type,
          required: true,
          error: this.state.error,
          errorMessage: translate.text.typeErrorDuplicity
        },
        {
          id: "plan",
          type: "custom",
          label: translate.text.planConfiguratorTitle,
          render: (
            <Dialog
              title={translate.text.planConfiguratorTitle}
              button={props => {
                const { ...rest } = props;
                return (
                  <Button color="primary" {...rest}>
                    {translate.text.planConfiguratorTitle}
                  </Button>
                );
              }}
            >
              <OrderPlan onChange={this.handleOrderPlan} />
            </Dialog>
          )
        }
      ];
    };
    let buttons = {
      close: true,
      save: this.state.valid
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              ref={this.formRef}
              title={language.text.typeAdd}
              controls={controls(language, entity)}
              buttons={buttons}
              onOpen={() => {
                this.updateKind();
                this.updateProducer();
              }}
              onSubmit={this.handleSubmit}
              onClose={this.handleClose}
              onChange={this.handleValidation}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
