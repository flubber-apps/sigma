import React from "react";

import PropTypes from "prop-types";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import FormGenerator from "../SmartComponents/FormDialog";
import { SystemContext } from "../../variables/SystemContext";
import CrudCategory from "../../actions/CrudCategory";
import WorkspaceStore from "../../stores/WorkspaceStore";
import AssignSizeStore from "../../stores/AssignSizeStore";
import TypeStore from "../../stores/TypeStore";

export default class Position extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    entity: PropTypes.object
  };
  static defaultProps = {
    entity: null
  };
  state = {
    enable: true,
    error: false,
    valid: false,
    typeList: [],
    workspaceList: [],
    sizeOfTypeList: []
  };
  typeId = undefined;
  entity = null;
  onChangeCallback = undefined;

  constructor(props) {
    super(props);
    for (let key in props) {
      if (key === "onChange") continue;
      if (key === "entity") continue;
      this.state[key] = props[key];
    }
    this.onChangeCallback = props.onChange;
    this.entity = props.entity;
    this.formRef = React.createRef();
  }

  componentWillMount() {
    TypeStore.addChangeListener(this.updateType);
    WorkspaceStore.addChangeListener(this.updateWorkspace);
    AssignSizeStore.addChangeListener(this.updateSizeOfType);
  }

  componentWillUnmount() {
    TypeStore.removeChangeListener(this.updateType);
    WorkspaceStore.removeChangeListener(this.updateWorkspace);
    AssignSizeStore.removeChangeListener(this.updateSizeOfType);
  }

  updateType = () => {
    this.setState({ typeList: TypeStore.getAll() });
  };

  updateWorkspace = () => {
    this.setState({ workspaceList: WorkspaceStore.getAll() });
  };
  updateSizeOfType = () => {
    this.setState({
      sizeOfTypeList: AssignSizeStore.getAllByType(this.typeId).map(item => {
        return {
          id: item.id,
          name: item.size.name
        };
      })
    });
  };
  handleValidation = data => {
    if (
      data.name !== undefined &&
      data.name !== "" &&
      data.sizeOfType !== undefined &&
      data.workspace !== undefined &&
      ((data.uid !== undefined && data.uid !== "") ||
        (data.serialNumber !== undefined && data.serialNumber !== ""))
    )
      this.setState({ valid: true });
    else this.setState({ valid: false });
  };
  handleChange = data => {
    if (data.type !== undefined) {
      this.typeId = data.type;
      this.updateSizeOfType();
    }
  };
  handleSubmit = data => {
    if (this.entity !== null) {
      //TODO update entity
    } else {
      CrudCategory.addPosition(
        data.sizeOfType,
        data.workspace,
        data.name,
        data.uid,
        data.serialNumber,
        data.comment,
        () => {
          this.formRef.current.handleClose();
        },
        () => {
          this.setState({ error: true });
        }
      );
    }
  };
  handleClose = () => {
    this.setState({ error: false });
  };

  render() {
    const { entity } = this.props;
    this.entity = entity;
    let controls = translate => {
      return [
        {
          id: "type",
          type: "select",
          options: this.state.typeList,
          label: translate.text.type,
          required: true
        },
        {
          id: "sizeOfType",
          type: "select",
          options: this.state.sizeOfTypeList,
          label: translate.text.size,
          required: true
        },
        {
          id: "workspace",
          type: "select",
          options: this.state.workspaceList,
          label: translate.text.workspace,
          required: true
        },
        {
          id: "name",
          label: translate.text.positionName,
          required: true
        },
        {
          id: "uid",
          label: translate.text.positionUid,
          error: this.state.error,
          errorMessage: translate.text.positionErrorDuplicity
        },
        {
          id: "serialNumber",
          label: translate.text.positionSerialNumber,
          error: this.state.error,
          errorMessage: translate.text.positionErrorDuplicity
        },
        {
          id: "comment",
          label: translate.text.positionComment
        }
      ];
    };
    let buttons = {
      close: true,
      save: this.state.valid
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              ref={this.formRef}
              title={language.text.positionAdd}
              controls={controls(language, entity)}
              buttons={buttons}
              onSubmit={this.handleSubmit}
              onChange={data => {
                this.handleChange(data);
                this.handleValidation(data);
              }}
              onOpen={() => {
                this.updateType();
                this.updateWorkspace();
              }}
              onClose={this.handleClose}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
