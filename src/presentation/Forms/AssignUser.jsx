import React from "react";

import PropTypes from "prop-types";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import FormGenerator from "../SmartComponents/FormDialog";
import { SystemContext } from "../../variables/SystemContext";
import WorkspaceStore from "../../stores/WorkspaceStore";
import UserStore from "../../stores/UserStore";

export default class AssignUser extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    type: PropTypes.object.isRequired
  };
  state = {
    data: [],
    type: null,
    enable: true,
    error: false
  };

  constructor(props) {
    super(props);
    for (let key in props) {
      if (key === "onChange") continue;
      this.state[key] = props[key];
    }

    this.state.data = [];
    this.formRef = React.createRef();
  }

  componentDidMount() {
    UserStore.addChangeListener(this.handleUpdate);
    WorkspaceStore.addChangeListener(this.handleUpdate);
    this.handleUpdate();
  }

  componentWillUnmount() {
    UserStore.removeChangeListener(this.handleUpdate);
    WorkspaceStore.removeChangeListener(this.handleUpdate);
  }

  handleSubmit = data => {
    WorkspaceStore.assignUser(this.state.type.id, data.user, () => {
      this.formRef.current.handleClose();
    });
  };

  handleUpdate = () => {
    let users = UserStore.getAll().filter(user => this.state.type.users.filter(userId => userId === user.id).length === 0);
    this.setState({
      data: users.map(item => ({id: item.id, name: item.email}))
    });
  };

  render() {
    let controls = translate => {
      return [
        {
          id: "user",
          type: "select",
          label: translate.text.user,
          required: true,
          options: this.state.data
        }
      ];
    };
    let buttons = {
      close: true,
      save: true
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              ref={this.formRef}
              title={language.text.userAssignedAdd}
              controls={controls(language)}
              buttons={buttons}
              onSubmit={this.handleSubmit}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
