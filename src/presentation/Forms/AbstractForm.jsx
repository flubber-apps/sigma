import AbstractComponent from "../SmartComponents/AbstractComponent";

export default class AbstractForm extends AbstractComponent {
  constructor(props) {
    super(props);
    if (new.target === AbstractForm) {
      throw new TypeError(
        "Cannot construct " +
          AbstractForm.name +
          " class instances directly. Class is abstract class."
      );
    }
  }
}
