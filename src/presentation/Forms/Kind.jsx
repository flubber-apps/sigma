import React from "react";

import PropTypes from "prop-types";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import FormGenerator from "../SmartComponents/FormDialog";
import { SystemContext } from "../../variables/SystemContext";
import KindStore from "../../stores/KindStore";

export default class Kind extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    entity: PropTypes.object
  };
  static defaultProps = {
    entity: null
  };
  state = {
    enable: true,
    error: false,
    valid: false
  };
  entity = null;

  constructor(props) {
    super(props);
    for (let key in props) {
      if (key === "onChange") continue;
      if (key === "entity") continue;
      this.state[key] = props[key];
    }
    this.formRef = React.createRef();
    this.entity = props.entity;
  }

  handleSubmit = data => {
    if (this.entity !== null) {
      //TODO update entity
    } else {
      KindStore.add(
        data.kind,
        () => {
          this.formRef.current.handleClose();
        },
        () => {
          this.setState({ error: true });
        }
      );
    }
  };
  handleValidation = data => {
    if (data.kind !== undefined && data.kind !== "")
      this.setState({ valid: true });
    else this.setState({ valid: false });
  };
  handleClose = () => {
    this.setState({ error: false });
  };

  render() {
    const { entity } = this.props;
    this.entity = entity;
    let controls = (translate, entity) => {
      if (entity !== null) {
        return [
          {
            id: "kind-id",
            hidden: true,
            value: entity.id
          },
          {
            id: "kind",
            label: translate.text.kindName,
            required: true,
            value: entity.name,
            error: this.state.error,
            errorMessage: translate.text.kindErrorDuplicity
          }
        ];
      } else {
        return [
          {
            id: "kind",
            label: translate.text.kindName,
            required: true,
            error: this.state.error,
            errorMessage: translate.text.kindErrorDuplicity
          }
        ];
      }
    };
    let buttons = {
      close: true,
      save: this.state.valid
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              ref={this.formRef}
              title={language.text.kindAdd}
              controls={controls(language, entity)}
              buttons={buttons}
              onSubmit={this.handleSubmit}
              onClose={this.handleClose}
              onChange={this.handleValidation}
              open={this.state.open}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
