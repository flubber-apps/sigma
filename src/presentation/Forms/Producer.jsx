import React from "react";

import PropTypes from "prop-types";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import FormGenerator from "../SmartComponents/FormDialog";
import { SystemContext } from "../../variables/SystemContext";
import ProducerStore from "../../stores/ProducerStore";

export default class Producer extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    entity: PropTypes.object
  };
  static defaultProps = {
    entity: null
  };
  state = {
    enable: true,
    error: false,
    valid: false
  };
  entity = null;

  constructor(props) {
    super(props);
    for (let key in props) {
      if (key === "onChange") continue;
      if (key === "entity") continue;
      this.state[key] = props[key];
    }
    this.entity = props.entity;
    this.formRef = React.createRef();
  }

  handleSubmit = data => {
    if (this.entity !== null) {
      //TODO update entity
    } else {
      ProducerStore.add(
        data.producer,
        () => {
          this.formRef.current.handleClose();
        },
        () => {
          this.setState({ error: true });
        }
      );
    }
  };
  handleValidation = data => {
    if (data.producer !== undefined && data.producer !== "")
      this.setState({ valid: true });
    else this.setState({ valid: false });
  };
  handleClose = () => {
    this.setState({ error: false });
  };

  render() {
    const { entity } = this.props;
    this.entity = entity;
    let controls = (translate, entity) => {
      if (entity !== null) {
        return [
          {
            id: "producer-id",
            hidden: true,
            value: entity.id
          },
          {
            id: "producer",
            label: translate.text.producerName,
            required: true,
            value: entity.name,
            error: this.state.error,
            errorMessage: translate.text.producerErrorDuplicity
          }
        ];
      } else {
        return [
          {
            id: "producer",
            label: translate.text.producerName,
            required: true,
            error: this.state.error,
            errorMessage: translate.text.producerErrorDuplicity
          }
        ];
      }
    };
    let buttons = {
      close: true,
      save: this.state.valid
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              ref={this.formRef}
              title={language.text.producerAdd}
              controls={controls(language, entity)}
              buttons={buttons}
              onSubmit={this.handleSubmit}
              onClose={this.handleClose}
              onChange={this.handleValidation}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
