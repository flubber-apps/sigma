import React from "react";

import PropTypes from "prop-types";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import FormGenerator from "../SmartComponents/FormDialog";
import { SystemContext } from "../../variables/SystemContext";
import AssignSizeStore from "../../stores/AssignSizeStore";
import CrudCategory from "../../actions/CrudCategory";

export default class AssignSize extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    type: PropTypes.object.isRequired
  };
  state = {
    data: [],
    type: null,
    enable: true,
    error: false
  };

  constructor(props) {
    super(props);
    for (let key in props) {
      if (key === "onChange") continue;
      this.state[key] = props[key];
    }

    this.state.data = AssignSizeStore.getAllUnassignByType(this.state.type.id);
    this.formRef = React.createRef();
  }

  componentWillMount() {
    AssignSizeStore.addChangeListener(this.handleUpdate);
  }

  componentWillUnmount() {
    AssignSizeStore.removeChangeListener(this.handleUpdate);
  }

  handleSubmit = data => {
    CrudCategory.assignSize(this.state.type.id, data.size, () => {
      this.formRef.current.handleClose();
    });
  };

  handleUpdate = () => {
    this.setState({
      data: AssignSizeStore.getAllUnassignByType(this.state.type.id)
    });
  };

  render() {
    let controls = translate => {
      return [
        {
          id: "size",
          type: "select",
          label: translate.text.size,
          required: true,
          options: this.state.data
        }
      ];
    };
    let buttons = {
      close: true,
      save: true
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              ref={this.formRef}
              title={language.text.typeAssignedAdd}
              controls={controls(language)}
              buttons={buttons}
              onSubmit={this.handleSubmit}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
