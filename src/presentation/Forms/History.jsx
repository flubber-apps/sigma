import React from "react";

import PropTypes from "prop-types";
import AbstractComponent from "../SmartComponents/AbstractComponent";
import FormGenerator from "../SmartComponents/FormGenerator/Dialog";
import { SystemContext } from "../../variables/SystemContext";
import DeviceSearch from "../components/DeviceSearch";
import PositionStore from "../../stores/PositionStore";
import HistoryStore from "../../stores/HistoryStore";
import Dialog from "../SmartComponents/Dialog";
import { DialogContent } from "@material-ui/core";
import Button from "../SmartComponents/Button";

export default class History extends AbstractComponent {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    onSubmit: PropTypes.func,
    entity: PropTypes.object
  };
  static defaultProps = {
    onSubmit: () => {},
    entity: null
  };
  state = {
    enable: true,
    error: false,
    valid: false,
    typeList: [],
    workspaceList: [],
    sizeOfTypeList: [],
    position: undefined,
    positionId: undefined
  };
  typeId = undefined;
  entity = null;
  onChangeCallback = undefined;

  constructor(props) {
    super(props);
    for (let key in props) {
      if (key === "onChange") continue;
      if (key === "entity") continue;
      this.state[key] = props[key];
    }
    this.onChangeCallback = props.onChange;
    this.entity = props.entity;
    this.formRef = React.createRef();
    this.searchDeviceRef = React.createRef();
  }
  handleChange = data => {
    const {position} = data;
    if (position !== undefined) {
      let parts = position.split("_");
      if (parts.length === 3 && Number.isInteger(parseInt(parts[2]))) {
        let _position = PositionStore.getById(parseInt(parts[2]));
        let data = [];
        if ("id" in _position) {
          if (_position.uid) data.push(_position.uid);
          if (_position.serialNumber) data.push(_position.serialNumber);
        }
        this.setState({
          positionId: parseInt(parts[2]),
          position: data.join("/")
        });
      }
    }
  };
  handleSubmit = data => {
    if (this.entity !== null) {
      //TODO update entity
    } else {
      HistoryStore.add(
        this.state.positionId,
        data.subject,
        data.number,
        data.numberOffer,
        data.startAt,
        data.endAt,
        data.content,
        response => {
          this.formRef.handleClose();
          this.props.onSubmit(response);
        },
        () => {
          this.setState({ error: true });
        }
      );
    }
  };
  handleClose = () => {
    this.setState({
      position: undefined,
      positionId: undefined,
      error: false
    });
  };
  handleSearchDevice = () => {
  };

  handleSelectDevice = id => {
    let parts = id.split("_");
    let position = PositionStore.getById(parseInt(parts[2]));
    this.setState({
      positionId: parseInt(parts[2]),
      position:
        "id" in position
          ? position.uid + "/" + position.serialNumber
          : undefined
    });
  };

  render() {
    const { entity } = this.props;
    this.entity = entity;
    let controls = translate => {
      return [
        {
          id: "position",
          type: "string-search",
          searchDialog: (props, onSelect) => (
            <Dialog {...props} simpleTitle>
              <DialogContent>
                <DeviceSearch onSelect={onSelect} filter={{defaultPosition: row => row.position.id !== undefined}}/>
              </DialogContent>
            </Dialog>
          ),
          label: translate.text.position,
          value: this.state.position,
          required: true
        },
        {
          id: "subject",
          label: translate.text.orderSubject,
          required: true
        },
        {
          id: "number",
          label: translate.text.orderNumber
        },
        {
          id: "numberOffer",
          label: translate.text.orderNumberOffer,
          required: true
        },
        {
          id: "startAt",
          type: "date",
          label: translate.text.orderStartAt,
          required: true
        },
        {
          id: "endAt",
          type: "date",
          label: translate.text.orderEndAt
        },
        {
          id: "content",
          label: translate.text.orderContent
        }
      ];
    };
    let buttons = {
      submit: "Založit"
    };
    return (
      <SystemContext.Consumer>
        {({ lang, langMapper }) => {
          let language = langMapper.getContent(lang);
          return (
            <FormGenerator
              innerRef={node => (this.formRef = node)}
              title={language.text.orderNew}
              controls={controls(language, entity)}
              buttons={buttons}
              onSubmit={this.handleSubmit}
              validate={this.handleValidation}
              onChange={this.handleChange}
              onClose={this.handleClose}
              buttonValue={this.state.entity === undefined ? "Založit zakázku" : "Upravit"}
              openButton={value => props => <Button color={"primary"} {...props}>{value}</Button>}
            />
          );
        }}
      </SystemContext.Consumer>
    );
  }
}
