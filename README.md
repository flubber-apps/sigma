# TDMS - kontrola kvality

## Instalace:
   - instalace závislostí třetích stran
```bash
$ composer install --no-dev
$ npm install --only=prod
```
   - nastavení připojek k databázi
     1. vytvoření souboru `./config/config.local.json`
     2. vložíme do něj: 
     ```json
     {
       "doctrine": {
         "host": "mysql",
         "user": "JMENO_UZIVATELE",
         "password": "HESLO",
         "dbname": "JMENO_DATABAZE"
       }
     }
     ```
        
   - vytvoření schéma databáze a provedení migrací
```bash
$ php api/index.php orm:schema-tool:update -f
$ php bin/phinx migrate
```
   - vegenrování zdrojových souboru pro klientskou část
```
$ npm run build
```

## Přihlašovací údaje

| Email | Heslo | Role 
| ----- | ------ | ----- |
 superadmin@web.com | He32slo | Super Administrator
 admin@web.com | He32slo | Administrator
 user@web.com | He32slo | Uživatel
 
## Spuštění testů
```
./bin/phpunit ./tests
```

