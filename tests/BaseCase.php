<?php

namespace Flubber\Tests;

use Nette\Application\IPresenterFactory;
use Nette\Application\IRouter;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Http\UrlScript;

/**
 * Class BaseCase
 *
 * @property-read Container $container
 */
abstract class BaseCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Container
     */
    private $container;

    protected function setUp()
    {
        parent::setUp();
        $this->container = require __DIR__ . "/bootstrap.php";
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    public function compareUrlWithPresenter($urlPath, $presenter, $action, $data = []) {
        $router = $this->container->getByType(IRouter::class);
        $url = new UrlScript($urlPath);
        // TODO
        $httpRequest = new Request($url);
        $appRequest = $router->match($httpRequest);
        var_dump($httpRequest);
        if ($appRequest === null)
            $this->assertTrue(false, "BadUrl API");

    }

    /**
     * @param string $name
     * @return \Nette\Application\IPresenter
     */
    public function getPresenter($name) {
        return $this->container->getByType(IPresenterFactory::class)->createPresenter($name);
    }
}