<?php

$loader = require __DIR__ . '/../vendor/autoload.php';

//Tester\Environment::setup();

$configurator = new \Flubber\Component\Application\Configurator($loader);

$configurator->setTempDirectory(__DIR__ . '/../../api/var/temp');
//$configurator->setDebugMode(false);
$configurator->enableTracy(__DIR__ . '/../../api/var/log');

$configurator->setTimeZone('Europe/Prague');

$configurator->addExtensions([
    "flubber.extension" => \Flubber\Extension\DI\Extension::class,
    "flubber.diskBrowser" => \Flubber\Component\DiskBrowser\DI\Extension::class
]);

$configurator->addConfig(__DIR__ . '/../../api/config/config.json');
$configurator->addConfig(__DIR__ . '/../../api/config/config.local.json');

$container = $configurator->createContainer();

return $container;