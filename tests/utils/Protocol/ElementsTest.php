<?php

namespace Flubber\Tests\utils\Protocol;

use Flubber\Extension\ProtocolUtils\AbstractElement;
use PHPUnit_Framework_TestCase as PHPUnit_Framework_TestCaseAlias;
use Tracy\Debugger;

class ElementsTest extends PHPUnit_Framework_TestCaseAlias
{

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object     Instantiated object that we will run method on.
     * @param string  $methodName Method name to call
     * @param array   $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = []) {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testIssetVariable() {
        /** @var AbstractElement $obj */
        $obj = $this->getMockForAbstractClass(AbstractElement::class);

        $obj->setValues(["test" => "asd", "test.2" => "???"]);

        $this->assertTrue($obj->issetVariable("%test%"));
        $this->assertTrue($obj->issetVariable("%test.2%"));
        $this->assertFalse($obj->issetVariable("%test.3"));
    }

    public function testGetVariableName() {
        /** @var AbstractElement $obj */
        $obj = $this->getMockForAbstractClass(AbstractElement::class);

        $this->assertTrue("TEST" === $obj->getVariableName("Hello %TEST%World!!!"));
        $this->assertTrue("TEST" === $obj->getVariableName("Hello %TEST%World%TEST.2%!!!"));
    }

    public function testParseValue() {
        /** @var AbstractElement $obj */
        $obj = $this->getMockForAbstractClass(AbstractElement::class);

        $obj->setValues(["TEST" => ""]);

        $this->invokeMethod($obj, "parseValue", [
            "", ""
        ]);
        $this->assertTrue("TEST" === $obj->getVariableName("Hello %TEST%World!!!"));
        $this->assertTrue("TEST" === $obj->getVariableName("Hello %TEST%World%TEST.2%!!!"));
    }
}