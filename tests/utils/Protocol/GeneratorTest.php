<?php

namespace Flubber\Tests\utils\Protocol;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Extension\ProtocolUtils\ProtocolGenerator;
use Nette\Application\Responses\FileResponse;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PHPUnit_Framework_TestCase;

class GeneratorTest extends PHPUnit_Framework_TestCase
{

    public $root = "./tests/utils/Protocol";

    public function testText() {
        $structure = [
            "body" => [
                [
                    "text" => [
                        "data" => "Ahoj"
                    ]
                ],
                [
                    "text" => [
                        "data" => "Ahoj",
                        "style" => [
                            "bold" => true
                        ]
                    ]
                ],
                [
                    "text" => [
                        "data" => "Ahoj",
                        "style" => [
                            "bold" => true,
                            "size" => 22
                        ],
                        "alignment" => [
                            "alignment" => "center"
                        ]
                    ]
                ],
                [
                    "title" => "Title 01"
                ],
                [
                    "title2" => "Title 02"
                ],
            ]
        ];
        $this->templateTest("test_text", $structure);
    }


    public function testTable() {
        $structure = [
            "body" => [
                [
                    "table" => [
                        "data" => [
                            [
                                "height" => 800,
                                "data" => [
                                    [
                                        "content" => [
                                            "text" => "A"
                                        ]
                                    ],
                                    [
                                        "content" => [
                                            "text" => "B"
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "C"
                                        ]
                                    ],
                                    [
                                        "width" => 4000,
                                        "content" => [
                                            "text" => "D"
                                        ],
                                        "style" => [
                                            "valign" => "center"
                                        ]
                                    ],
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "E"
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "G"
                                        ]
                                    ],
                                    [
                                        "width" => null,
                                    ],
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "H"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "text" => ""
                ],
                [

                    "table" => [
                        "style" => "tableBorder",
                        "data" => [
                            [
                                "height" => 800,
                                "data" => [
                                    [
                                        "content" => [
                                            "text" => "A"
                                        ]
                                    ],
                                    [
                                        "content" => [
                                            "text" => "B"
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "C"
                                        ]
                                    ],
                                    [
                                        "width" => 4000,
                                        "content" => [
                                            "text" => "D"
                                        ],
                                        "style" => [
                                            "valign" => "center"
                                        ]
                                    ],
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "E"
                                        ]
                                    ]
                                ]
                            ],
                            [
                                "data" => [
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "G"
                                        ]
                                    ],
                                    [
                                        "width" => null,
                                    ],
                                    [
                                        "width" => 2625,
                                        "content" => [
                                            "text" => "H"
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "styles" => [
                "tableDefault" => [
                    "borderSize" => 6,
                    "borderColor" => "000000"
                ],
                "tableFirstRowDefault" => null,
                "tableBorder" => [
                    "borderSize" => 12,
                    "borderColor" => "000000"
                ],
                "tableFirstRowBorder" => null
            ]
        ];
        $this->templateTest("test_table", $structure);
    }

    public function testDocumentParts() {
        $structure = [
            "body" => [
                "text" => "BODY"
            ],
            "header" => [
                "text" => "HEADER"
            ],
            "footer" => [
                "text" => "FOOTER"
            ],
            "orientation" => "landscape"
        ];
        $this->templateTest("test_document_parts", $structure);
    }

    private function templateTest($sourceName, $structure) {
        $phpWord = new PhpWord();
        $generator = new ProtocolGenerator($phpWord, $structure, []);
        $generator->setEntityManager($this->getMockForAbstractClass(EntityManagerInterface::class));
        $generator->run();

        $objWriter = IOFactory::createWriter($phpWord);
        $objWriter->save("{$this->root}/{$sourceName}.docx");
        exec("export HOME=/home && soffice --headless --convert-to pdf --outdir {$this->root} {$this->root}/{$sourceName}.docx");
        $assertedImagick = new \Imagick("{$this->root}/{$sourceName}.pdf[0]");
        $assertedImagick->setImageFormat("png");
        // $assertedImagick->writeImage("{$this->root}/{$sourceName}_result.png");
        $assertedImagick->resetIterator();
        $assertedImagick = $assertedImagick->appendImages(true);
        $testImagick = new \Imagick("{$this->root}/{$sourceName}_result.png");
        $testImagick->resetIterator();
        $testImagick = $testImagick->appendImages(true);

        $diff = $assertedImagick->compareImages($testImagick, 1);
        $this->assertSame(0.0, $diff[1]);
    }
}