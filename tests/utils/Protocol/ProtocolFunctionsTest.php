<?php

namespace Flubber\Tests\utils\Protocol;

use Flubber\Extension\ProtocolUtils\ProtocolFunctions;
use PHPUnit_Framework_TestCase;

class ProtocolFunctionsTest extends PHPUnit_Framework_TestCase
{
    public function testAvg() {
        $this->assertTrue(5 == ProtocolFunctions::avg(10, 0));
        $this->assertTrue(-5 == ProtocolFunctions::avg(-10, 0));
        $this->assertTrue(2.5 == ProtocolFunctions::avg(7, -2));
    }

    public function testValidateNumber() {
        $this->assertTrue(10.0 === ProtocolFunctions::validateNumber(10.0));
        $this->assertTrue(-10 == ProtocolFunctions::validateNumber(-10.0));
        $this->assertTrue(-15.2 === ProtocolFunctions::validateNumber("-15,2"));
        $this->assertTrue(0.2 === ProtocolFunctions::validateNumber("+0,2"));
        $this->assertTrue(0.2 === ProtocolFunctions::validateNumber("+0.2"));
        $this->assertTrue(-0.2 === ProtocolFunctions::validateNumber("-0.2"));
        $this->assertTrue(-2 === ProtocolFunctions::validateNumber("-2"));
    }

    public function testParseNumber() {
        $this->assertTrue(-15.2 === ProtocolFunctions::parseNumber("-15,2"));
        $this->assertTrue(0.2 === ProtocolFunctions::parseNumber("+0,2"));
        $this->assertTrue(0.2 === ProtocolFunctions::parseNumber("-+0.2"));
        $this->assertTrue(-0.2 === ProtocolFunctions::parseNumber("-0.2"));
        $this->assertTrue(-0.2 === ProtocolFunctions::parseNumber("-0.2EWA"));
        $this->assertTrue(-2 === ProtocolFunctions::parseNumber("+-2"));
    }

    public function testParallelism() {
        $this->assertTrue(11.0 === ProtocolFunctions::parallelism(10.5, "-0,5"));
        $this->assertTrue(9.0 === ProtocolFunctions::parallelism("10.5", "+19,5"));
    }

    public function testFlangeOffset() {
        $this->assertTrue(5.5 === ProtocolFunctions::flangeOffset(10.5, "-0,5"));
        $this->assertTrue(4.5 === ProtocolFunctions::flangeOffset("10.5", "+19,5"));
    }

}