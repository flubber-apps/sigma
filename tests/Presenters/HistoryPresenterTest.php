<?php

namespace Flubber\Tests\Presenters;

use Doctrine\ORM\EntityManagerInterface;
use Flubber\Extension\Presenters\HistoryPresenter;
use Flubber\Tests\BaseCase;
use Nette\Application\Request;

class HistoryPresenterTsest //extends BaseCase
{
    public function testsApiUrl() {
        $this->compareUrlWithPresenter("/history", HistoryPresenter::class, "actionRead");
    }

    public function tesstRead() {
/** @var HistoryPresenter $presenter */
        $presenter = $this->getPresenter("Extension:History");
        $presenter->em = $this->createMock($presenter);
        $request = new Request("Extension:History", "GET", ["action" => "read"]);

        var_dump($presenter->run($request));
        //$this->container->getByType();
        $this->assertTrue(true);
    }
}